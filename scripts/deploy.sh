#!/bin/bash

# Script to deploy the securityscanner backend
# 1  Saves any existing deployment in a time-stamped subdir
# 2  Currently ubuntu is assumed by default as the platform

OSVARIANT=ubuntu

APPLICATION=securityscanner
VERSION=v1

SOURCEROOT=..
APPSRCDIR=$SOURCEROOT/htdocs
CONFSRCDIR=$SOURCEROOT/conf

if [ "$OSVARIANT" = "ubuntu" ]
then
    WEBROOT=/var/www/$APPLICATION
    if [ -d $WEBROOT ]
    then
        echo ">> Saving existing website deployment.."
        SAVEDIR=$WEBROOT/.saved/`date +%Y-%m-%d-%H-%M-%S`
        sudo mkdir -p $SAVEDIR
        sudo mv $WEBROOT/* $SAVEDIR/
    else
        echo ">> Creating webroot for initial deployment.."
        sudo mkdir -p $WEBROOT
        mkdir -p $APPSRCDIR/csv
    fi
    echo ">> Copying source to webroot.."
    sudo cp -r $APPSRCDIR/* $WEBROOT/
    sudo chmod 777 $WEBROOT/csv/
    echo ">> Enabling $APPLICATION virtual host.."
    sudo cp $CONFSRCDIR/apache/$OSVARIANT/*$APPLICATION*.conf /etc/apache2/sites-available/
    sudo a2ensite *$APPLICATION*
    sudo a2enmod rewrite
    sudo service apache2 restart
fi
