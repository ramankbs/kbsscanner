#!/bin/bash

# Sets up the securityscanner database and privileges

OSVARIANT=ubuntu

APPLICATION=securityscanner
VERSION=v1

SOURCEROOT=..
DESIGNDIR=$SOURCEROOT/doc/design/
CONFDIR=$SOURCEROOT/htdocs/$VERSION/conf

if [ "$OSVARIANT" = "ubuntu" ]
then
    echo "Setting up MySQL $APPLICATION database.."
    echo -n ">> Enter $APPLICATION database username : "
    read SSUSER
    echo -n ">> Enter $APPLICATION database user password : "
    read -s SSPASSWD
    echo " "
    echo -n ">> (Re)creating the $APPLICATION database.."
    cat $DESIGNDIR/mysql-schema.sql | sed "s/{SSAPP}/$APPLICATION/g" | sed "s/{SSUSER}/$SSUSER/g" | sed "s/{SSPASSWD}/$SSPASSWD/g" | mysql
    echo "   Done"
    echo ">> Creating the user/password configuration for PHP code to use.."
    mkdir -p $CONFDIR
    echo -n ">> Enter $APPLICATION database hostname : "
    read SSHOST
    echo "$SSHOST" > $CONFDIR/mysql.conf
    echo "$APPLICATION" >> $CONFDIR/mysql.conf
    echo "$SSUSER" >> $CONFDIR/mysql.conf
    echo "$SSPASSWD" >> $CONFDIR/mysql.conf
    echo "   Done"
fi
