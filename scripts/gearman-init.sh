#!/bin/bash

# Sets up the securityscanner database and privileges

OSVARIANT=ubuntu

APPLICATION=securityscanner
VERSION=v1

SOURCEROOT=..
CONFDIR=$SOURCEROOT/htdocs/$VERSION/conf

if [ ! -d $CONFDIR ]
then
    mkdir -p $CONFDIR
fi

if [ "$OSVARIANT" = "ubuntu" ]
then
    echo "Setting up gearman job queue config ..."
    echo -n ">> Enter gearman hostname : "
    read GHOST
    echo -n ">> Enter gearman port : "
    read GPORT
    echo " "
    echo -n ">> (Re)creating the gearman conf ..."
    echo "$GHOST" > $CONFDIR/gearman.conf
    echo "$GPORT" >> $CONFDIR/gearman.conf
    echo "   Done"
fi
