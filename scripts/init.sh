#!/bin/bash

# Script to init the dev setup
# 1  Sets up mysql config to be able to run mysql commands on bash command line

OSVARIANT=ubuntu

SOURCEROOT=..
CONFSRCDIR=$SOURCEROOT/conf

if [ "$OSVARIANT" = "ubuntu" ]
then
    echo ">> Setting up MySQL dev env.."
    echo -n ">> Enter MySQL root user for this host : "
    read USER
    echo -n ">> Enter MySQL root user password for this host : "
    read -s PASSWD
    echo " "
    cat $CONFSRCDIR/mysql/$OSVARIANT/.my.cnf | sed "s/{USER}/$USER/g" | sed "s/{PASSWD}/$PASSWD/g" > ~/.my.cnf
    echo ">> Setup MySQL ~/.my.cnf for helping run SQL schema scripts automatically"
fi
