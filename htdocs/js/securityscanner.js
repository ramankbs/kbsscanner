$(document).ready(function($){
    //initFixedHeader();
    initFormValidations();
    initUserId();
    initNotice();
    initFooter();

    var rememberme = $.cookie('rememberme');
    
    if (rememberme == 'true') 
    {
        var email = $.cookie('email');
        var password = $.cookie('password');
        // autofill the fields
        $('#email').val(email);
        $('#password').val(password);
        $('#rememberme').prop('checked', true);
    }

    $("#user-login").submit(function() {
        if ($('#rememberme').is(':checked')) {
            var email = $('#email').val();
            var password = $('#password').val();

            // set cookies to expire in 14 days
            $.cookie('email', email, { expires: 14 });
            $.cookie('password', password, { expires: 14 });
            $.cookie('rememberme', true, { expires: 14 });                
        }
        else
        {
            // reset cookies
            $.cookie('email', null);
            $.cookie('password', null);
            $.cookie('rememberme', null);
        }
  });
    
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });    
});

$(document).resize(function(){
    initUserId();
    initFooter();
});

function initNotice() {
    if($(".notice").length) {
        $(".notice").click(function(){$(this).fadeOut(400, function(){$(this).remove();});});
        $(".notice").fadeIn(400);
    }
}

function initFooter() {
    var h = $("div.footer:first").height();
    var t = $("div.footer:first").offset().top;
    if(h + t < $(window).height()) {
        $("div.footer:first").css('width', '100%');
        $("div.footer:first").css('position', 'absolute');
        $("div.footer:first").css('bottom', 0);
    }
}

var user_id_bottom = 91;

function initUserId() {
    if($(".user-id").length) {
        user_id_bottom = $(".user-id:first").offset().top + $(".user-id:first").outerHeight() + 10;
        $(".user-id").click(function(){
            var user = $(this).attr('user');
            var h = '<div style="margin-bottom: 20px;cursor: pointer !important;" onclick="window.location=\'/v1/users/scans/:' + user + '\';">My Scans</div><div style="margin-bottom: 20px;cursor: pointer !important;" onclick="window.location=\'/v1/users/emails/:' + user + '\';">My Emails</div><div style="margin-bottom: 20px;cursor: pointer !important;" onclick="window.location=\'/v1/users/domains/:' + user + '\';">My Domains</div><div style="cursor: pointer !important;" onclick="logout();">Logout</div>';
            addPopup($(".user-id:first"), h, 0, user_id_bottom);
            return(false);
        });
    }
}

function initFormValidations() {
    if($("form[name='users-signup'] input[name='confirm']:first").length) {
        var passwd_elem = $("form[name='users-signup'] input[name='password']:first").eq(0);
        var confirm_elem = $("form[name='users-signup'] input[name='confirm']:first").eq(0);
        $(confirm_elem).change(function(){
            validateConfirmField(confirm_elem);
            if($(passwd_elem).val() != $(confirm_elem).val()) {
                addFlyout(confirm_elem, "Password do not match");
            }
        });
        $(passwd_elem).change(function(){
            if($(confirm_elem).val() != '') {
                validateConfirmField(confirm_elem);
                if($(passwd_elem).val() != $(confirm_elem).val()) {
                    addFlyout(confirm_elem, "Password do not match");
                }
            }
        });
    }
}

function validateScanForm() {
    var url_elem = $("form[name='scans-create'] input[name='url']:first").eq(0);
    if(false == validateUrlField(url_elem)) return(false);
    //var captcha_elem = $("form[name='scans-create'] input[name='captcha']:first").eq(0);
    //if(false == validateTextField(captcha_elem)) return(false);
        var googleResponse = $('#g-recaptcha-response').val();
    if (!googleResponse) {
        alert("Please Fill Captcha");
        return false;
    }
    if($("form[name='scans-create'] input[type=checkbox][name=terms]:first").length) {
        var terms_elem = $("form[name='scans-create'] input[type=checkbox][name=terms]:first").eq(0);
        if($(terms_elem).prop('checked') == false) {
            addFlyout($(terms_elem).parent(), "Please confirm that you have read the Terms & Conditions");
            return(false);
        }
    }
    return(true);
}

function validateSigninForm() {
    var email_elem = $("form[name='users-login'] input[name='email']:first").eq(0);
    if(false == validateEmailField(email_elem)) return(false);
    var passwd_elem = $("form[name='users-login'] input[name='password']:first").eq(0);
    if(false == validatePasswdField(passwd_elem)) return(false);
    return(true);
}

function validateSignupForm() {
    var first_name_elem = $("form[name='users-signup'] input[name='first-name']:first").eq(0);
    if(false == validateTextField(first_name_elem)) return(false);
    var last_name_elem = $("form[name='users-signup'] input[name='last-name']:last").eq(0);
    if(false == validateTextField(last_name_elem)) return(false);
    var email_elem = $("form[name='users-signup'] input[name='email']:first").eq(0);
    if(false == validateEmailField(email_elem)) return(false);
    var passwd_elem = $("form[name='users-signup'] input[name='password']:first").eq(0);
    if(false == validatePasswdField(passwd_elem)) return(false);
    var confirm_elem = $("form[name='users-signup'] input[name='confirm']:first").eq(0);
    if(false == validateConfirmField(confirm_elem)) return(false);
    if($(passwd_elem).val() != $(confirm_elem).val()) {
        addFlyout(confirm_elem, "Password do not match");
        return(false);
    }
    var company_elem = $("form[name='users-signup'] input[name='company']:last").eq(0);
    if(false == validateTextField(company_elem)) return(false);
    var position_elem = $("form[name='users-signup'] input[name='position']:last").eq(0);
    if(false == validateTextField(position_elem)) return(false);
    //var phone_elem = $("form[name='users-signup'] input[name='phonenumber']:last").eq(0);
    //if(false == validateTextField(phone_elem)) return(false);
    //var address_elem = $("form[name='users-signup'] input[name='address']:last").eq(0);
    //if(false == validateTextField(address_elem)) return(false);
    //var captcha_elem = $("form[name='users-signup'] input[name='captcha']:first").eq(0);
    //if(false == validateTextField(captcha_elem)) return(false);
    var googleResponse = $('#g-recaptcha-response').val();
    if (!googleResponse) {
        alert("Please Fill Captcha");
        return false;
    }
    var terms_elem = $("form[name='users-signup'] input[type=checkbox][name=terms]:first").eq(0);
    if($(terms_elem).prop('checked') == false) {
        addFlyout($(terms_elem).parent(), "Please confirm that you have read the Terms & Conditions");
        return(false);
    }
    return(true);
}

function validateForgotPasswordForm() {
    var email_elem = $("form[name='forgot-password'] input[name='email']:first").eq(0);
    if(false == validateEmailField(email_elem)) return(false);
    if($("form[name='forgot-password'] input[name='password']:first").length) {
        var passwd_elem = $("form[name='forgot-password'] input[name='password']:first").eq(0);
        if(false == validatePasswdField(passwd_elem)) return(false);
        var confirm_elem = $("form[name='forgot-password'] input[name='confirm']:first").eq(0);
        if(false == validateConfirmField(confirm_elem)) return(false);
        if($(passwd_elem).val() != $(confirm_elem).val()) {
            addFlyout(confirm_elem, "Password do not match");
            return(false);
        }
    }
    return(true);
}

function validateEmailField(elem) {
    if($(elem).val() == $(elem).attr('inittext') ||
       false == validateEmail($(elem).val()))
    {
        addFlyout(elem, 'Please enter a valid email address');
        return(false);
    }
    return(true);
}

function validateUrlField(elem) {
    if($(elem).val() == $(elem).attr('inittext') ||
       false == validateUrl($(elem).val()))
    {
        addFlyout(elem, 'Please enter a valid url of the form http://www.xyz.com/ or https://www.xyz.com/a/b/c.html');
        return(false);
    }
    return(true);
}

function validateTextField(elem) {
    if($(elem).val() == $(elem).attr('inittext') || $(elem).val() == '') {
        addFlyout(elem, 'Please enter a valid value');
        return(false);
    }
    return(true);
}

function validatePasswdField(elem) {
    if('' == $(elem).val()) {
        addFlyout(elem, 'Please enter your password');
        return(false);
    }
    return(true);
}

function validateConfirmField(elem) {
    if('' == $(elem).val()) {
        addFlyout(elem, 'Please enter password confirmation');
        return(false);
    }
    return(true);
}

function validateEmail(x) {
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        return false;
    }
    return(true);
}

function validateUrl(x) {
    if(!x.match(/^http:\/\//) && !x.match(/^https:\/\//)) return(false);
    var strip = x.replace(/^http[s]*:\/\//, "");
    strip = strip.replace(/\/.*/, "");
    var slashpos = strip.length;
    var dotpos = strip.lastIndexOf(".");
    if (dotpos < 1 || dotpos >= (slashpos - 1)) return(false);
    return(true);
}

function initFixedHeader() {
    $(document).scroll(function(e) {
        var scrollTop = $(this).scrollTop();
        var headerTop = 0;
        if(scrollTop > headerTop) {
            $("div.navbar:first").addClass('sticky-header');
        }
        else {
            $("div.navbar:first").removeClass('sticky-header');
        }
    });
}

function logout() {
    $('body').append('<form id="logout" action="/v1/users/logout" method="POST"></form>');
    $('form#logout').submit();
}



function validateSignupFormadmin() {
    var first_name_elem = $("form[name='admin-add-user'] input[name='first-name']:first").eq(0);
    if(false == validateTextField(first_name_elem)) return(false);
    var last_name_elem = $("form[name='admin-add-user'] input[name='last-name']:last").eq(0);
    if(false == validateTextField(last_name_elem)) return(false);
    var email_elem = $("form[name='admin-add-user'] input[name='email']:first").eq(0);
    if(false == validateEmailField(email_elem)) return(false);
    var passwd_elem = $("form[name='admin-add-user'] input[name='password']:first").eq(0);
    if(false == validatePasswdField(passwd_elem)) return(false);
    var company_elem = $("form[name='admin-add-user'] input[name='company']:last").eq(0);
    if(false == validateTextField(company_elem)) return(false);
    var position_elem = $("form[name='admin-add-user'] input[name='position']:last").eq(0);
    if(false == validateTextField(position_elem)) return(false);

    return(true);
}