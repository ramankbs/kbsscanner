$(document).ready(function(){
    if($(".carousel").length) {
        setCarouselPosition(0);
        setupCarousel();
    }
    initInputs();
    initSelects();
    initCheckboxes();
});

/* CAROUSELS */

var carouselHandle = null;

function setupCarousel() {
    carouselHandle = setInterval(carousel, 5000);
}

var counter = 0;
var numSlides = $(".carousel:first .slide").length;

function carousel() {
    clearInterval(carouselHandle);
    var current = $(".carousel:first .slide").eq(counter);
    counter = (counter + 1) % numSlides;
    var next = $(".carousel:first .slide").eq(counter);
    $(current).stop(true, true).fadeOut(600,function(){
        setCarouselPosition(counter);
        $(next).stop(true, true).fadeIn(600);
        carouselHandle = setInterval(carousel, 5000);
    });
}

function setCarouselPosition(counter) {
    $(".carousel .carousel-position > div").removeClass("active");
    $(".carousel .carousel-position > div").eq(counter).addClass("active");
}

/* INPUTS */

function initInputs() {
    $("input[inittext]").focus(function(){
        if($(this).val() == $(this).attr('inittext')) {
            $(this).val('');
        } else {
            //$(this).select();
            if (this.setSelectionRange) {
                var len = $(this).val().length * 2;
                this.setSelectionRange(len, len);
            } else {
                $(this).val($(this).val());
            }
        }
    });
    $("input[type='password']").focus(function(){
        $(this).select();
    });
    $("input[inittext]").each(function(){
        if('' == $(this).val())
            $(this).val($(this).attr('inittext'));
    });
    $("input[inittext]").blur(function(){
        if($(this).val() == '') {
            $(this).val($(this).attr('inittext'));
        }
    });
}

/* SELECT WIDGET */

function initSelects() {
    $(".select .control .control-container").unbind('click').click(function(){
        $(".finre-popup").css('display', 'none');
        $(this).parent().parent().find("div.data").eq(0).css('display', 'initial');
        $('html').unbind('click').click(function(){
            $(".finre-popup").css('display', 'none');
            $('html').unbind('click');
        });
        $(document).unbind('keyup').keyup(function(e){
            if(27 == e.which) {
                e.preventDefault();
                $(".finre-popup").css('display', 'none');
            }
            $(document).unbind('keyup');
        });
        return(false);
    });
    $(".select div.selected").unbind('click').click(function(){
        if($(this).parent().find('div.search').length) {
            var selected = this;
            var search = $(this).parent().find('div.search').eq(0);
            $(this).css('display', 'none');
            var input = $(search).find('input[type=text]').eq(0);
            $(input).val('');
            $(search).css('display', '');
            $(input).focus();
        } else {
            $(this).parent().find('.control .control-container').eq(0).click();
            return(false);
        }
    });
    $(".select div.search input[type=text]").unbind('keyup').keyup(function(e){
        var select = $(this).parent().parent();
        var selected = $(select).find('div.selected').eq(0);
        var search = $(this).parent();
        var input = $(this);
        var data = $(select).find('div.data').eq(0);
        if(27 == e.which) {
            e.preventDefault();
            $(select).find('li').css('display', '');
            $(".finre-popup").css('display', 'none');
            return;
        }
        if($(input).val() == '') {
            $(select).find('li').css('display', '');
            $(".finre-popup").css('display', 'none');
            return;
        }
        var val = $(input).val().toLowerCase();
        $(select).find('li').each(function(){
            var h = $(this).html();
            if(-1 != h.toLowerCase().search(val)) {
                $(this).css('display', '');
            } else {
                $(this).css('display', 'none');
            }
        });
        if($(data).css('display', 'none')) {
            $(data).css('display', '');
        }
    });
    $(".select div.search input[type=text]").unbind('blur').blur(function(){
        var selected = $(this).parent().parent().find('div.selected').eq(0);
        var search = $(this).parent();
        var input = $(this);
        $(search).css('display', 'none');
        $(selected).css('display', '');
        $(this).parent().parent().find('div.data').css('display', 'none');
        $(this).parent().parent().find('li').css('display', '');
    });
    $(".select .data li").unbind('click').click(function(){
         var h = $(this).html();
         changeSelect($(this).parent().parent().parent(), h);
    });
    $(".select").each(function() {
        var h = '';
        if($(this).find("li.selected").length) {
            h = $(this).find("li.selected:first").html();
        } else {
            h = $(this).find("li:first").html();
        }
        changeSelect(this, h);
    });
}

function changeSelect(select, h) {
    $(select).find('div.selected').eq(0).html(h);
    $(select).find('li').removeClass('selected');
    $(select).find('li[data="'+h+'"]').addClass('selected');
    var pad_hz_left = parseInt($(select).css('padding-left'));
    var pad_hz_right = parseInt($(select).css('padding-right'));
    if($(select).css('max-width') != $(select).css('min-width')) {
        var w = $(select).find('div.selected').eq(0).outerWidth();
        var total_w = w + pad_hz_left + pad_hz_right;
        $(select).find("div.data:first").css('width', total_w);
        $(select).width(total_w);
        $(select).find('div.search').eq(0).width(w);
        $(select).find('div.search input[type=text]').eq(0).width(w);
    } else {
        var max_w = parseInt($(select).css('min-width'));
        $(select).find("div.data:first").css('width', max_w);
        var w = max_w - pad_hz_left - pad_hz_right;
        $(select).find('div.selected').eq(0).width(w);
        $(select).find('div.selected').css('overflow-x', 'hidden');
        $(select).find('div.search').eq(0).width(w);
        $(select).find('div.search input[type=text]').eq(0).width(w);
    }
    var name = $(select).find('input[type=hidden]').attr('name');
    if($("div.select[dep*='"+name+"']").length) {
        $("div.select[dep*='"+name+"']").each(function(){
            $(this).find("li").css('display', 'none');
            $(this).find("li[dep*='"+h+"']").css('display', '');
            $(this).find("li[indep]").css('display', '');
            if($(this).find("li.selected").css('display') == 'none') {
                $(this).find("li").removeClass('selected');
                var first = $(this).find("li").filter(function(){return($(this).css('display') != 'none');}).eq(0);
                changeSelect(this, $(first).html());
                $(first).addClass('selected');
            }
        });
    }
    if($(select).attr('dep') && $(select).hasClass('reverse-fill')) {
        var dep = $(select).attr('dep');
        if($(select).find('div.data ul li[dep].selected').length > 0) {
            var parts = $(select).find('div.data ul li.selected:first').attr('dep').split(',');
            $('input[type=hidden][name="'+dep+'"]').each(function(){
                var dep_select = $(this).parent();
                for(var i = 0; i < parts.length; ++i) {
                    if($(dep_select).find('div.data li[data="'+parts[i]+'"]').length) {
                        changeSelect(dep_select, parts[i]);
                        break;
                    }
                }
            });
        } else { // this is typically the case of li's with header value in the select
            $('input[type=hidden][name="'+dep+'"]').each(function(){
                var dep_select = $(this).parent();
                var first = $(dep_select).find('div.data li:first').eq(0).html();
                changeSelect(dep_select, first);
            });
        }
    }
    $(select).find("input[type='hidden']").val(h).trigger('change');
}

/* CHECKBOX */

function initCheckboxes() {
    $("label.checkbox > input[type='checkbox']").each(function(){
        handleCheckboxChange(this);
    });
    $("label.checkbox > input[type='checkbox']").change(function(){
        handleCheckboxChange(this);
    });
}

function handleCheckboxChange(elem) {
    if(true == $(elem).prop('checked')) $(elem).parent().addClass('checked');
    else $(elem).parent().removeClass('checked');
    if($(elem).attr('disabled') == 'disabled') $(elem).parent().addClass('disabled');
    else $(elem).parent().removeClass('disabled');
}

/* FLYOUT */

function addPopup(elem, popupMessage, minWidth, fixed_bottom) {
    $(".flyout").remove();
    var style = '';
    if(minWidth > 0) style = 'style="min-width: ' + minWidth + 'px !important;"';
    var fixed_style = '';
    if(fixed_bottom != 0) fixed_style = ' style="position:fixed;top:' + fixed_bottom + 'px;"';
    // add flyout
    var h = 
      '<div class="flyout finre-popup elem-popup"' + fixed_style + '>' +
        '<div ' + style + '>' +
          '<div class="arrow"></div>' +
          '<div class="flyout-content">' + popupMessage + '</div>' +
        '</div>' +
      '</div>';

    if(fixed_bottom)
        setupFlyout(elem, h, true);
    else
        setupFlyout(elem, h, false);
}

function addFlyout(elem, message) {
    $(".flyout").remove();
    // add flyout
    var h = 
      '<div class="flyout finre-popup">' +
        '<div style="width:300px;">' +
          '<div class="arrow"></div>' +
          '<div class="close">x</div>' +
          '<div>' + message + '</div>' +
        '</div>' +
      '</div>';
    setupFlyout(elem, h, false);
}

function setupFlyout(elem, h, is_fixed) {
    $('body').append(h);
    // position it below the element to attach with
    var max_right = $(document).width() - 40;
    var min_left = 40;
    var offset = $(elem).offset();
    var w = $(".flyout:first").outerWidth();
    var fl_left = ($(elem).width()/2) + offset.left - (w/2);
    if(fl_left + w > max_right) fl_left = (max_right - w);
    if(fl_left < min_left) fl_left = min_left;
    $(".flyout:first").css('left', fl_left);
    if(!is_fixed) {
        var fl_top = offset.top + $(elem).height() + 30;
        $(".flyout:first").css('top', fl_top);
    }
    var arrow_left = offset.left - fl_left + ($(elem).width()/2) - ($(".flyout:first .arrow").width()/2);
    $(".flyout:first .arrow").css('left', arrow_left);
    // scroll to element and show the flyout
    if(!is_fixed) {
        $('body,html').stop(true,true).animate({scrollTop:offset.top-180},200,function(){
            initFlyout();
        });
    } else {
        initFlyout();
    }
}

function initFlyout() {
    $('html').unbind('click').click(function(){
        $(".finre-popup").css('display', 'none');
        $('.flyout').remove();
        $('html').unbind('click');
    });
    $(document).unbind('keyup').keyup(function(e){
        if(27 == e.which) {
            e.preventDefault();
            $(".finre-popup").css('display', 'none');
            $('.flyout').remove();
            $(document).unbind('keyup');
        }
    });
    $(".flyout:first").css('display', 'initial');
    $(".flyout:first .close").unbind('click').click(function(){
        $(this).parent().parent().remove();
    });
}
