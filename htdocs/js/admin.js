$(document).ready(function(){
    if($("#file-cover").length) {
        $("#file-cover").click(function(){$("#file-upload").click();});
        $("#file-upload").change(function(){
            $("#file-submit").css('display', '');
            $("#file-name").html('Selected file: ' + $("#file-upload").val());
        });
    }
});

function validateEmailField(elem) {
    if($(elem).val() == $(elem).attr('inittext') ||
       false == validateEmail($(elem).val()))
    {
        addFlyout(elem, 'Please enter a valid email address');
        return(false);
    }
    return(true);
}

function validateTextField(elem) {
    if($(elem).val() == $(elem).attr('inittext') || $(elem).val() == '') {
        addFlyout(elem, 'Please enter a valid value');
        return(false);
    }
    return(true);
}

function validatePasswdField(elem) {
    if('' == $(elem).val()) {
        addFlyout(elem, 'Please enter your password');
        return(false);
    }
    return(true);
}

function validateEmail(x) {
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        return false;
    }
    return(true);
}

function validateSignupFormadmin() {
    var first_name_elem = $("form[name='admin-add-user'] input[name='first-name']:first").eq(0);
    if(false == validateTextField(first_name_elem)) return(false);
    var last_name_elem = $("form[name='admin-add-user'] input[name='last-name']:last").eq(0);
    if(false == validateTextField(last_name_elem)) return(false);
    var email_elem = $("form[name='admin-add-user'] input[name='email']:first").eq(0);
    if(false == validateEmailField(email_elem)) return(false);
    var passwd_elem = $("form[name='admin-add-user'] input[name='password']:first").eq(0);
    if(false == validatePasswdField(passwd_elem)) return(false);
    var company_elem = $("form[name='admin-add-user'] input[name='company']:last").eq(0);
    if(false == validateTextField(company_elem)) return(false);
    var position_elem = $("form[name='admin-add-user'] input[name='position']:last").eq(0);
    if(false == validateTextField(position_elem)) return(false);

    return(true);
}