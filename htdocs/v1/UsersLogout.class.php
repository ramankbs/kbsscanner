<?php

class UsersLogout extends SessionPagesBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        $this->_beginHandle();
        if(false == ($user_id = $this->_checkSession())) {
            $this->_endHandle();
            return;
        }
        $referer = $_SERVER['HTTP_REFERER'];
        $session = CookieManager::get(AppGlobals::$USER_COOKIE_NAME);
        DbHandler::deleteQuery(Array('table' => 'sessions',
                                        'where' => Array('session_id' => $session)));
        $site = AdminUtils::getSiteName();
        CookieManager::setDomainCookie(AppGlobals::$USER_COOKIE_NAME, $session, -(365*24*60*60), $site);
	header ("Location: /");
        $this->_endHandle();
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'POST' );

    protected $allowNoJson = true;
}

?>
