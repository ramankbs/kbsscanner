<?php

class PagesHelper
{
    /* Private constructor */
    private function __construct() {
    }

    static function htmlBegin($hasParallax=false) {
        
        if(!isset($_COOKIE['rememberme']) && !isset($_COOKIE['PHPSESSID'])){
            
            $session = CookieManager::get(AppGlobals::$USER_COOKIE_NAME);
            DbHandler::deleteQuery(Array('table' => 'sessions',
                                            'where' => Array('session_id' => $session)));
            $site = AdminUtils::getSiteName();
            CookieManager::setDomainCookie(AppGlobals::$USER_COOKIE_NAME, $session, -(365*24*60*60), $site);
            ?>
            
            <script type="text/javascript">
                window.location.href = "";
            </script>
            
            <?php            
        }
        
        if($hasParallax) $class = "class=\"has-parallax\"";
        else $class = "";
        $str = <<<EOH
<!DOCTYPE html>
<html $class>
<head>
   <meta charset="utf-8">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <title>Security Scanner</title>
   <meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0,maximum-scale=1.0">
   <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
   <link rel="icon" href="/faviconn.png" type="image/x-icon">
   <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,500,600,700,800' rel='stylesheet' type='text/css'>
   <link rel="stylesheet" type="text/css" href="/css/normalize.css">
   <link rel="stylesheet" type="text/css" href="/css/finre.css">
   <link rel="stylesheet" type="text/css" href="/css/securityscanner.css">
<!--[if lt IE 9]>
   <script src="/js/respond.min.js"></script>
<![endif]-->
   <script src="/js/modernizr.custom.min.js"></script>
</head>
<body $class>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-57125101-2', 'auto');
    ga('send', 'pageview');
  </script>
EOH;
        return($str);
    }

    static function htmlEnd() {
        $str = <<<EOH
<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/js/finre.js"></script>
<script type="text/javascript" src="/js/securityscanner.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>
EOH;
        return($str);
    }

    static function headerNotLoggedIn($class='') {
        $str = <<<EOH
   <div class="header-back"></div>

   <div class="not-logged-in navbar$class">
<div class="main-container">
      <div class="site-title left new_logo" onclick="window.location='/';">
         <div class="logos"><img align=left src="/img/logo.png"></div>
      </div>
      <div class="nav-container navContainer">
         <ul class="nav right">
            <li><a href="/scan">Run a Scan</a></li>
            <!--li><a href="/scanner-details">Scanner Details</a></li-->
            <li><a href="/sign-in">Sign In</a></li>
            <li><a href="/sign-up">Sign Up</a></li>
         </ul>
      </div>
   </div>
 </div>
EOH;
        return($str);
    }

    static function headerLoggedIn($class='') {
        $session = CookieManager::get(AppGlobals::$USER_COOKIE_NAME);
        $first_name = null; $last_name = null;
        $stmt = DbHandler::select(Array('table'   => 'sessions',
                                        'columns' => Array('user_id'),
                                        'where'   => Array('session_id' => $session)));
        if(null != ($row = DbHandler::getRow($stmt))) {
            $user_id = $row['user_id'];
            $stmt = DbHandler::select(Array('table'   => 'users',
                                            'columns' => Array('first_name', 'last_name'),
                                            'where'   => Array('id' => $user_id)));
            if(null != ($row = DbHandler::getRow($stmt))) {
                $first_name = $row['first_name'];
                $last_name = $row['first_name'];
            }
        }
        if(null != $first_name) {
            $user_str = '<li class="user-id with-text" user="' . $user_id . '">' .
                         substr(ucfirst($first_name), 0, 1) . substr(ucfirst($last_name), 0, 1) .
                         '</li>';
        } else {
            $user_str = '<li class="user-id with-text">U</li>';
        }
        $str = <<<EOH
   <div class="header-back"></div>
   <div class="logged-in navbar$class">
<div class="main-container">
      <div class="site-title left new_logo" onclick="window.location='/';">
         <div class="logos"><img align=left src="/img/logo.png"></div>
      </div>
      <div class="nav-container navContainer">
         <ul class="nav right">
            <li><a href="/scan">Run a Scan</a></li>
            <!--li><a href="/scanner-details">Scanner Details</a></li-->
            $user_str
         </ul>
      </div>
   </div>
</div>
EOH;
        return($str);
    }

    static function footer() {
        $str = <<<EOH
   <div class="footer">
	<div class="footer_inner">
         <div class="logoFooter1"><a href="http://www.zapts.com" target="_blank"><img align="left" src="/img/logo1.png"></a></div>
<div class="copyright">
      <div class="infocopy">© 2015 Zap Technology Solutions, LLC. All Rights Reserved.</div>
      <div class="italic" style="text-align:center;">Any use of this site is subject to the <a href="/terms-of-service" style="display:inline-block;text-decoration:underline;">Terms of Use</a>.</div></div>
<div class="logoFooter2"><a href="https://solutionpartners.adobe.com/content/apex/en/home/partnerFinder/partnerdetail.a1X14000005Nv1fEAC.html" target="_blank"><img align="left" src="/img/Solution_Partner_Community_badge.jpg"></a></div>
   </div></div>
EOH;
        return($str);
    }

    static function buildSelect($name, $selected, $values, $dep_on = null, $dep_maps = null) {
        $default = $values[0];
        if(null != $dep_on) { $dep_on = " dep=\"$dep_on\""; }
        $str = <<<EOH
         <div class="select"$dep_on>
            <div class="selected"></div>
            <div class="control"><div class="control-container"></div></div>
            <div class="data finre-popup">
               <ul>
EOH;
        for($i = 0; $i < sizeof($values); ++$i) {
             $val = $values[$i];
             if(null != $selected and $selected == $val) $class = 'class="selected"';
             else $class = '';
             if(null == $dep_maps || !isset($dep_maps[$val])) {
                 $str .= <<<EOH
                  <li indep $class>$val</li>
EOH;
             } else {
                 $dep = $dep_maps[$val];
                 $str .= <<<EOH
                  <li dep="$dep" $class>$val</li>
EOH;
             }
        }
        $str .= <<<EOH
               </ul>
            </div>
            <input type="hidden" name="$name" value="$default"/>
         </div>
EOH;
        return($str);
    }
}

?>
