<?php

class ScrapeJobSender {

    static function send($user_id, $url, $email, $ip_address) {
        $gmclient= new GearmanClient();
        //$gmclient->addServer('127.0.0.1',8888);
        $gmclient->addServer('127.0.0.1',4730);
        $job_id = uniqid();
        $workload = Array('url' => $url, 'email' => $email, 'jobid' => $job_id);
        $job_handle = $gmclient->doBackground("scrape", json_encode($workload));

        if ($gmclient->returnCode() != GEARMAN_SUCCESS) {
            return "ERROR|" . $gmclient->returnCode();
        } else {
            $now = date("Y-m-d H:i:s");
            if(preg_match("/^http:/", $url)) {
                $domain = preg_replace("/^http:\/\//", "", $url);
            } else if(preg_match("/^https:/", $url)) {
                $domain = preg_replace("/^https:\/\//", "", $url);
            }
            $domain_parts = explode("/", $domain);
            $domain = array_shift($domain_parts);
            DbHandler::insert(Array('table' => 'jobs', 'columns' => Array('user_id' => $user_id, 'handle' => $job_handle, 'job_id' => $job_id, 'url' => $url, 'domain' => $domain, 'email' => $email, 'created_on' => $now, 'ip_address' => $ip_address)));
            return "OK|$job_id";
        }
    }
}

?>
