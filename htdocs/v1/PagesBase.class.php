<?php

abstract class PagesBase extends RestHandler
{

    /*
     * PUBLIC METHODS
     */

    public function __construct($_model, $_object, $_args) {
        parent::__construct($_model, $_object, $_args);
        $this->allowNoJson = true;
    }

    protected function headerFunctionName($page = 'regular') {
        if(null != CookieManager::get(AppGlobals::$USER_COOKIE_NAME)) {
            $funcName = 'headerLoggedIn';
        } else {
            $funcName = 'headerNotLoggedIn';
        }
        return($funcName);
    }

    protected function handleGetRedirect($object) {
    }
}

?>
