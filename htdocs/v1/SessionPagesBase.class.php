<?php

abstract class SessionPagesBase extends PagesBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _checkSession() {
        $session = CookieManager::get(AppGlobals::$USER_COOKIE_NAME);
        // Find the user id from session
        $stmt = DbHandler::select(Array('table'   => 'sessions',
                                        'columns' => Array('user_id', 'id'),
                                        'where'   => Array('session_id' => $session)));
        if(null == ($row = DbHandler::getRow($stmt))) {
            // No session found - redirect to home - safest place
            header('Location: /');
            return(false);
        }
        // Update the session last seen time
        $now = date("Y-m-d H:i:s");
        DbHandler::update(Array('table'  => 'sessions',
                                'update' => Array('modified_on' => $now),
                                'where'  => Array('id' => $row['id'])));
        return($row['user_id']);
    }

    protected function _getAnonSession() {
        $anon = CookieManager::get(AppGlobals::$ANON_COOKIE_NAME);
        if(null == $anon) return(false);
        $stmt = DbHandler::select(Array('table' => 'users',
                                        'columns' => Array('id'),
                                        'where' => Array('login_id' => 'anon', 'auth' => $anon)));
        if(null != ($row = DbHandler::getRow($stmt))) {
            return($row['id']);
        }
        return(false);
    }
}

?>
