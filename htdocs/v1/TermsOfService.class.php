<?php

class TermsOfService extends PagesBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        $this->handleGet();
        $this->_endHandle();
    }

    private function handleGet() {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->content()
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function content() {
        $str = <<<EOH
<div class="content body-margin-top">
  <div>
    <h1>SERVICE AGREEMENT</h1>
  </div>
  <div>
<p>BY TICKING THE ‘I ACCEPT THE TERMS OF USE’ CHECKBOX IN THE REGISTRATION PAGES OF THE SERVICE, THE ENTITY YOU REPRESENT (THE ‘CUSTOMER’) IS HEREBY AGREEING WITH ZAP TECHNOLOGY SOLUTIONS, LLC (‘ZAPTS’) TO THE FOLLOWING TERMS OF SERVICE AND ANY TERMS INCORPORATED HEREIN BY SPECIFIC REFERENCE (COLLECTIVELY, REFERRED TO AS THE ‘TERMS’).</p>
<ol>
   <li>
      <b>Definitions</b>
      <p>‘You’, ‘Customer’, ‘Subscriber’ or ‘End-user’ means an individual or a single entity, corporate or other statutory body with legal personality that is being granted an Account by ZAPTS to make use of the Service.</p>
      <p>‘Terms of Service’ or ‘Terms’ mean the terms as per this Service Agreement.</p>
      <p>‘Service’ means the Service offered by ZAPTS and branded by ZAPTS as ‘Zap Technology Solutions AEM Security Scanner’ that has been utilized by the Customer and is provided for the purposes of conducting Network and Web Security Audits.</p>
      <p>‘Service Infrastructure’ means all the ZAPTS systems that facilitate, provide or describe the ‘Service’.</p>
      <p>‘Network Security Audits’ are audits conducted to ascertain the compliance of network Devices with certain published security standards and to disclose security vulnerabilities and may include but are not limited to port scanning and port connections, as well as evaluating services by checking versions and responses to certain requests.</p>
      <p>‘Web Security Audits’ means the crawling of a website to perform testing of forms, application responses, or to confirm the existence of certain files.</p>
      <p>‘Device’ or &#39;Devices&#39; means computer hardware, network, storage, input/output or electronic control devices, or software installed on such devices.</p>
      <p>‘Hostnames’ means the name used to identify each individual Device. ‘URLs’ is the address of a web site.</p>
      <p>‘IPs’ or &#39;IP Addresses&#39; refers to the address of a Device.</p>
      <p>‘ZAPTS Parties’ means Zap Technology Solutions, LLC and its parents, subsidiaries, shareholders, directors, officers, employees, licensors, suppliers and agents.</p>
      <p>‘Support’ means the furnishing of technical assistance and remedies, provided on a best effort basis, via any telematic means chosen by ZAPTS.</p>
      <p>‘Confidential Information’ means any information disclosed by one party to another which is defined as confidential and proprietary as per the Terms of this Service Agreement.</p>
      <p>‘Intellectual Property Rights’ means all intellectual property rights including patents, trademarks, design rights, copyrights, database rights, trade secrets and all rights of an equivalent nature anywhere in the world.</p>
   </li>
   <li>
      <b>Ineligible Parties</b>
      <p>To the extent permissible by law, You are ineligible to subscribe to the Service if (a) You or Your employees have been convicted for any computer or Internet related crimes; or (b) if You are more than</p>
      <p>sixty (60) days overdue on any monies or amounts owed to ZAPTS; or (c) if You are a competitor of ZAPTS; or (d) if You are located in a region that is prohibited from using the Service by law; or (e) if You have already previously been refused the Service by ZAPTS in the past.</p>
      <p>Provided that in any case, ZAPTS reserves the right to refuse access to any potential subscriber to the Service should ZAPTS, in its absolute discretion, deem such refusal necessary.</p>
   </li>
   <li>
      <b>Your Identity and Authority</b>
      <p>You agree to provide current, accurate information in all electronic or hardcopy registration forms submitted in connection with the Service. You agree not to impersonate or in any way misrepresent Your affiliation or authority to act on behalf of any person, company or other entity. By subscribing to the Service or accepting these Terms, all Your personnel using the Service or accepting these Terms, certify that they are authorized to act on Your behalf and are authorized by You as a representative of an individual, business or other legal entity having contractual usage rights granted by an ISP or Web Host, owning or licensed to use any and all IPs and the associated Devices to which You direct the Service to be performed. You agree to cooperate with ZAPTS within reasonable measures to verify the identity and authority of persons using the Service.</p>
   </li>
   <li>
      <b>Prohibited Uses</b>
      <ol style="list-style-type: upper-alpha;">
         <li>
            <p style="display: inline;">Scanning of Third Party Devices</p>
            <p>You must never use or direct the Service to interact with IPs or Devices for which You are not expressly authorized to do so. You must not use the Service in such a way as to create unreasonable load on IPs or Devices to which You have directed the Service to interact. You may not use any Service Infrastructure, directly or indirectly to initiate, propagate, participate, direct or attempt any attack, hack, or send bandwidth saturation, malicious or potentially damaging network messages to any Device, whether owned by ZAPTS or not.</p>
         </li>
         <li>
            <p style="display: inline;">Reasonable Usage of ZAPTS Service</p>
            <p>You must not, through the use of the Service or by any other means, create unreasonable load on the Service Infrastructure.</p>
         </li>
         <li>
            <p style="display: inline;">Unlawful Activities</p>
            <p>You must not use the Service to perform any unlawful activity including but not limited to computer crime, transmission or storage of illegal content, or content or software in violation of intellectual property and copyright laws.</p>
         </li>
         <li>
            <p style="display: inline;">Unauthorized Access</p>
            <ol style="list-style-type: lower-roman;">
               <li>
                  <p style="display: inline;">You must not access information on the Service Infrastructure for which You are not authorized, or which is not made available intentionally, publicly and in accordance with ZAPTS Privacy Policy available on http://www.zapts.com/privacy.html. If You gain access to any information for which You are not authorized, by any means or method, or for any reason, You must report such access to ZAPTS immediately and destroy all electronic or hard copies of such information. You must report incidents by email with return receipt requested to support@zapts.com</p>
               </li>
               <li>
                  <p style="display: inline;">Furthermore, You agree not to provide access to the Service by:- a) allowing others to use Your account; b) creating an account for someone who is not authorized to perform the role or view the information for which You have granted access; c) creating an account for an ineligible party as defined in clause 2 above; or d) failing to revoke access for those persons who are no longer authorized to access the Service for any reason. You will immediately notify ZAPTS of any unauthorized access from Your account or the accounts of others for which You have administrative authority, including the use of accounts, passwords, or any other breach of security. You will not solicit another party’s password for any reason. You will not access someone else’s account, nor disrupt, interfere, or limit the functioning of the Services or other’s enjoyment of the Service.</p>
                  <p>Any breach of the above covenants will result in immediate termination of the Service and, if appropriate, referral to law enforcement authorities.</p>
               </li>
            </ol>
         </li>
      </ol>
   </li>
   <li>
      <b>Support</b>
      <p>Support shall be conveyed by telematic means to You in order to help You with Your ongoing use of the Service. Support is available during normal business hours (CET) at ZAPTS. ZAPTS may at its own discretion and without engaging in any statements of service levels with customers, extend its Support availability to other time zones, where feasible.</p>
   </li>
   <li>
      <b>Disclaimer of Warranties</b>
      <p>THE SERVICE IS PROVIDED &#39;&#39;AS IS.&#39;&#39; ZAP TECHNOLOGY SOLUTIONS, LLC DOES NOT WARRANT THAT THE FUNCTIONS CONTAINED IN THE SERVICE WILL MEET YOUR REQUIREMENTS, OR THAT THE OPERATION OF THE SERVICE WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT DEFECTS IN THE SERVICE WILL BE CORRECTED. FURTHERMORE, ZAP TECHNOLOGY SOLUTIONS, LLC DOES NOT WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OR THE RESULTS OF THE USE OF THE SERVICE OR ANY DOCUMENTATION PROVIDED THEREWITH IN TERMS OF THEIR CORRECTNESS, ACCURACY, RELIABILITY, OR OTHERWISE. NO ORAL OR WRITTEN INFORMATION OR ADVICE GIVEN BY ZAPTS PARTIES SHALL CREATE A WARRANTY OR IN ANY WAY INCREASE THE SCOPE OF THIS WARRANTY. ZAPTS PARTIES DO NOT MAKE ANY, AND HEREBY SPECIFICALLY DISCLAIM ANY OTHER REPRESENTATIONS, ENDORSEMENTS, GUARANTIES, OR WARRANTIES, EXPRESSED, IMPLIED OR STATUTORY, RELATED TO THE SERVICE INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTY OF MERCHANTABILITY, TITLE, FITNESS FOR A PARTICULAR PURPOSE, ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OF WORKMANLIKE EFFORT, OF LACK OF VIRUSES, AND OF LACK OF NEGLIGENCE, AND THE PROVISION OF OR FAILURE TO PROVIDE SUPPORT. THERE IS NO WARRANTY OR CONDITION OF TITLE, QUIET ENJOYMENT, QUIET POSSESSION, CORRESPONDENCE TO DESCRIPTION OR NON-INFRINGEMENT (OF INTELLECTUAL PROPERTY RIGHTS OR OTHERWISE) WITH REGARD TO THE SERVICE.</p>
      <p>TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL ZAPTS PARTIES BE LIABLE FOR ANY SPECIAL, INCIDENTAL, INDIRECT, OR CONSEQUENTIAL DAMAGES WHATSOEVER (INCLUDING, BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS OR CONFIDENTIAL OR OTHER INFORMATION, FOR BUSINESS INTERRUPTION, FOR PERSONAL INJURY, FOR LOSS OF PRIVACY, FOR FAILURE TO MEET ANY DUTY INCLUDING OF GOOD FAITH OR OF REASONABLE CARE, FOR NEGLIGENCE, AND FOR ANY OTHER PECUNIARY OR OTHER LOSS WHATSOEVER) ARISING OUT OF OR IN ANY WAY RELATED TO THE USE OF OR INABILITY TO USE THE SERVICE, THE PROVISION OF OR FAILURE TO PROVIDE SUPPORT, OR OTHERWISE UNDER OR IN CONNECTION WITH ANY PROVISION OF THIS SERVICE AGREEMENT, EVEN IN THE EVENT OF THE FAULT, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY, BREACH OF CONTRACT OR BREACH OF WARRANTY OF ZAPTS PARTIES, AND EVEN IF ZAPTS PARTIES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.</p>
   </li>
   <li>
      <b>Copyright and Intellectual Property</b>
      <p>The Service is protected by copyright and all other applicable intellectual property laws and treaties of the United States and other nations and by any international treaties, unless specifically excluded herein.</p>
      <p>ZAPTS Parties own the title, copyright, and other Intellectual Property Rights in the Service.</p>
      <p>This Service Agreement does not convey to the Subscriber an interest in or to the Service, but only a limited right of use revocable in accordance with the Terms of this Service Agreement. ZAPTS Parties own all right, title and interest in and to the Service. No license or other right in or to the Service is being granted to the Subscriber except for the rights specifically set forth in this Service Agreement. The Subscriber hereby agrees to abide by all applicable laws and international treaties, and undertakes to inform Zap Technology Solutions, LLC of any suspected breach of Intellectual Property Rights belonging to ZAPTS Parties.</p>
   </li>
   <li>
      <b>Privacy</b>
      <p>You agree that ZAPTS and its affiliates may collect and use technical information which You provide or which is acquired by ZAPTS as part of Your use of the Service. ZAPTS agrees not to use this information in a form that personally identifies You. Each party shall comply with its respective obligations under applicable data protection laws (“DPL”).</p>
      <p>You agree that ZAPTS may refer to the name of Your corporation as one of its customers, both internally and in externally published media, unless You expressly, and in writing, restrict ZAPTS from mentioning You. Any additional disclosure by ZAPTS with respect to You or Your company shall be subject to Your prior written consent.</p>
   </li>
   <li>
      <b>Confidentiality</b>
      <p>You acknowledge that the Service and certain other materials are confidential as provided herein. ZAPTS Parties’ confidential information includes any and all information related to the Service and/or business of ZAPTS Parties that is treated as confidential or secret by ZAPTS Parties (that is, it is the subject of efforts by ZAPTS Parties, as applicable, that are reasonable under the circumstances to maintain its secrecy), including, without limitation:</p>
      <ol style="list-style-type: lower-alpha;">
         <li>
            <p style="display: inline;">The Service;</p>
         </li>
         <li>
            <p style="display: inline;">Any and all other information which is disclosed by ZAPTS to You orally, electronically, visually, or in a document or other tangible form which is either identified as or should be reasonably understood to be confidential and/or proprietary; and,</p>
         </li>
         <li>
            <p style="display: inline;">Any notes, extracts, analysis, or materials prepared by You which are copies of or derivative works of ZAPTS Parties’ confidential information from which the substance of said information can be inferred or otherwise understood (the “Confidential Information”).</p>
         </li>
      </ol>
      <p>During the course of delivery of Support it will be necessary for confidential information to be exchanged between You and ZAPTS. The Recipient may use such confidential information only for the purposes for which it was provided, and may disclose it only to employees, or contractors or partners, obligated to the Recipient under similar confidentiality restrictions and only for the purposes it was provided.</p>
      <p>Confidential information shall not include information which the Recipient can clearly establish by written evidence:</p>
      <ol style="list-style-type: lower-alpha;">
         <li>
            <p style="display: inline;">Is already lawfully known to or independently developed by the Recipient without access to the confidential information;</p>
         </li>
         <li>
            <p style="display: inline;">Is disclosed in non-confidential published materials;</p>
         </li>
         <li>
            <p style="display: inline;">Is generally known to the public; or</p>
         </li>
         <li>
            <p style="display: inline;">Is rightfully obtained from any third party without any obligation of confidentiality.</p>
         </li>
      </ol>
      <p>The Recipient agrees not to disclose confidential information to any third party and will protect and treat all confidential information with the highest degree of care. Except as otherwise expressly provided in this Service Agreement, the Recipient will not use or make any copies of confidential information, in whole or in part, without the prior written authorization of the other party. The Recipient may disclose confidential information if required by statute, regulation, or order of a court of competent jurisdiction, provided that the Recipient provides the other party with prior notice, discloses only the minimum confidential information required to be disclosed, and cooperates with the other party in taking appropriate protective measures. These obligations shall continue to survive indefinitely following the termination of this Service Agreement with respect to confidential information.</p>
      <p>ZAPTS Parties will not be obliged to respect Your confidential information in the case of termination due to Your breach of the Service Agreement conditions, insofar as such information is required for ZAPTS Parties to safeguard their own rights and interests.</p>
   </li>
   <li>
      <b>Changes in Service</b>
      <p>You acknowledge and agree that ZAPTS may, in its sole and absolute discretion, modify or remove the Service as necessary. Scans, verification and authentications performed by the Service may also be modified, removed or updated by ZAPTS at any time without notice.</p>
   </li>
   <li>
      <b>Web and Network Security Audits</b>
      <p>You hereby authorize ZAPTS to perform Web and/or Network Security Audits on any Devices, IPs, Hostnames and URLs specified by You. Web and/or Network Security Audits are performed with the assistance of ZAPTS employees or its appointed contractors, and may from time to time include additional probing and validation beyond the scope of the ZAPTS Online Vulnerability Scanner automated scanning system. In certain cases, the exploitation of a vulnerability and/or minimal extraction of data from the target server or web application may be conducted to support ZAPTS’s security audit findings or to illustrate a vulnerability to the Customer.</p>
   </li>
   <li>
      <b>Suspension of Accounts or Audits</b>
      <p>ZAPTS reserves the right to suspend the Service being given to You, at any stage, should it in its sole discretion, deem such suspension necessary.</p>
      <p>ZAPTS reserves the right not to commence or to suspend an audit at any stage, should it deem it necessary, in its sole discretion, to do so.</p>
   </li>
   <li>
      <b>Storage of Scan Data</b>
      <p>ZAPTS is only bound to retain all stored data originating from audits for a limited period of one (1) calendar year.</p>
   </li>
   <li>
      <b>Beta Testing</b>
      <p>Should You receive the Service for a limited time-period in Beta, such Service will be provided to You, at no cost, at Your own risk, without warranty of any kind, “AS IS”, without any guarantees on the storage period of the audit data and subject to confidentiality as outlined in this Service Agreement, as well as under the clear understanding that You are obliged to provide truthful, accurate and complete feedback on the Beta version of the Service, with no expectation of remuneration, and You agree to waive any claims for royalties or any other forms of remuneration, with ZAPTS on any use made by ZAPTS of the feedback provided, in whatever form.</p>
   </li>
   <li>
      <b>Indemnity</b>
      <p>You agree to indemnify, defend, and hold ZAPTS Parties harmless from any claim, loss, demand, or damage, including reasonable attorneys’ fees, asserted by any third party due to or arising out of Your breach of any provision of this Service Agreement, Your negligent or wrongful acts, and/or Your violation of any applicable laws.</p>
   </li>
   <li>
      <b>Limitation of Liability</b>
      <p>Notwithstanding any damages that You might incur for any reason whatsoever (including, without limitation, all damages referred to above and all direct or general damages), the entire liability of ZAPTS Parties under any provision of this Service Agreement and Your exclusive remedy for all of the foregoing, shall be limited to the greater of the amount actually paid by You for the Service, during that calendar year, or U.S.$5.00. The foregoing limitations, exclusions and disclaimers shall apply to the maximum extent permitted by applicable law, even if any remedy fails its essential purpose.</p>
   </li>
   <li>
      <b>Term and Termination</b>
      <p>Without prejudice to any other rights, ZAPTS may revoke this Service Agreement if You do not abide by the terms and conditions of this Service Agreement.</p>
      <p>An Account that has reached its expiry date is to be considered as terminated or revoked. When Your Account is terminated, You must immediately cease and desist from making further use of the Service.</p>
   </li>
   <li>
      <b>Entire Agreement</b>
      <p>This Service Agreement is the entire agreement between You and ZAPTS relating to the Service and Support (if any) and it supersedes all prior or contemporaneous oral or written communications, proposals and representations with respect to the Service or any other subject matter covered by this Service Agreement. To the extent that the terms of any ZAPTS terms and conditions, policies or programs for Support conflict with the terms of this Service Agreement, the terms of this Service Agreement shall prevail.</p>
      <p>In addition, the terms set out in this Service Agreement shall prevail and control over any and all additional or conflicting terms or provisions contained in any document of Yours, whether set out in a purchase order or alternative agreement, and any and all such additional or conflicting terms shall be void and shall have no effect.</p>
      <p>If this Service Agreement is translated into a language other than English and there are conflicts between the translations, the English version shall prevail and control.</p>
      <p>This Service Agreement:</p>
      <ol style="list-style-type: lower-roman;">
         <li>
            <p style="display: inline;">May not be assigned by You. Any purported assignment will be null and void;</p>
         </li>
         <li>
            <p style="display: inline;">May not be amended by You, but ZAPTS may amend the Service Agreement from time to time and shall incorporate any amended Service Agreement in the latest current version of the Service available at http://scan.zapts.com and may be provided upon request;</p>
         </li>
         <li>
            <p style="display: inline;">Constitutes the entire understanding between the parties with respect to the subject matter of this Service Agreement and supersedes all written and oral prior agreements, negotiations and discussions between the parties relating to it; and;</p>
         </li>
         <li>
            <p style="display: inline;">Is for the sole benefit of ZAPTS and You and nothing herein, express or implied, is intended to or shall confer upon any other person or entity any legal or equitable right, benefit or remedy of any nature whatsoever under or by reason of this Service Agreement.</p>
         </li>
      </ol>
   </li>
   <li>
      <b>Severability</b>
      <p>If any part of any provision of this Service Agreement is found to be illegal, invalid or unenforceable, that provision shall apply with the minimum modification necessary to make it legal, valid and enforceable, and all other terms shall remain in force. Paragraph headings are for convenience and shall have no effect or interpretation.</p>
   </li>
   <li>
      <b>US Government End Users</b>
      <p>If the Subscriber is obtaining the Service on behalf of any part of the US Government, the Service and any documentation pertaining thereto shall be deemed a ‘commercial item’, as that term is defined in 48</p>
      <p>C.F.R. 2.101 (Oct. 1995), consisting of ‘commercial computer software’ and ‘commercial computer software documentation’, as such terms are used in 48 C.F.R. 12.212 (Sept. 1995). Consistent with 48</p>
      <p>C.F.R. 12.212 and 48 C.F.R. 227.7202-1 through 227.7202-4 (June 1995), all U.S. Government End Users acquire the Service with only those rights set forth herein. Any use, modification, revision, release,</p>
      <p>performance, display or disclosure of the Service shall be governed solely by the Terms of this Service Agreement.</p>
   </li>
   <li>
      <b>Governing Law</b>
      <p>This Service Agreement shall be governed by and construed in accordance with the laws of the state of Nevada USA, without regard to conflict of law provisions thereto. You submit to the jurisdiction of any court sitting in Nevada, in any action or proceeding arising out of or relating to this Service Agreement and agree that all claims in respect of the action or proceeding may be heard and determined in any such court. There shall be no class action, arbitration or litigation pursuant to this Service Agreement. ZAPTS may seek injunctive relief in any venue of its choosing. You hereby submit to personal jurisdiction in such courts. The parties hereto specifically exclude the United Nations Convention on Contracts for the International Sale of Goods and the Uniform Computer Information Transactions Act from this Service Agreement and any transaction between them that may be implemented in connection with this Service Agreement. The original of this Service Agreement has been written in English. The parties hereto waive any statute, law, or regulation that might provide an alternative law or forum or to have this Service Agreement written in any language other than English.</p>
   </li>
   <li>
      <b>Equitable Relief</b>
      <p>It is agreed that because of the proprietary nature of the Service, ZAPTS Parties’ remedies at law for a breach by You of its obligations under this Service Agreement will be inadequate and that ZAPTS Parties shall, in the event of such breach, be entitled to, in addition to any other remedy available to it, equitable relief, including injunctive relief, without the posting of any bond and in addition to all other remedies provided under this Service Agreement or available at law.</p>
   </li>
   <li>
      <b>No Waiver or Delay</b>
      <p>The delay or failure of ZAPTS to exercise any right provided in this agreement shall not be deemed a waiver of such right. Any express waiver, delay or failure by ZAPTS to exercise promptly any right under this agreement due to it will not create a continuing waiver or any expectation of non-enforcement.</p>
   </li>
   <li><b>Force Majeure</b></li>
</ol>
<p>A party is not liable under this Service Agreement for non-performance caused by events or conditions beyond that party’s control if that party makes reasonable efforts to perform. This provision does not relieve You of Your obligation to make all payments due.</p>
  </div>
</div>
<div class="body-margin-bottom"></div>
EOH;
        return($str);
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET' );
}

?>
