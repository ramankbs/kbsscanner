<?php

class ResponseHandler
{
    private function __construct() {
    }

    /* Send the http response */
    static function response($data, $status = 200, $echo = true) {
        header("HTTP/1.1 " . $status . " " . self::requestStatus($status));
        if('json' == self::$OUTPUT) {
            if($echo) {
                echo json_encode($data);
            } else {
                return(json_encode($data));
            }
        } else if('html' == self::$OUTPUT) {
            if($echo) {
                if(null != $data) {
                    echo $data . "\n";
                }
            } else {
                return($data);
            }
        }
    }

    /* Request status helper */
    static function requestStatus($code) {
        $status = array(  
            200 => 'OK',
            201 => 'Created',
            400 => 'Bad Request',
            404 => 'Not Found',   
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        ); 
        return ($status[$code])?$status[$code]:$status[500]; 
    }

    /* send an error response */
    static function errorResponse($errorCode, $status, $echo = true, $a1 = null, $a2 = null, $a3 = null) {
        $message = self::$ERROR_MESSAGES[$errorCode];
        if(null != $a1) {
            $message = preg_replace("/\{1\}/", $a1, $message);
        }
        if(null != $a2) {
            $message = preg_replace("/\{2\}/", $a2, $message);
        }
        if(null != $a3) {
            $message = preg_replace("/\{3\}/", $a3, $message);
        }
        if(self::$OUTPUT == 'json') {
            if($echo) {
                self::response(Array("status" => 1,
                                   "error_code" => self::$ERROR_CODES[$errorCode],
                                   "error_message" => $message), $status);
            } else {
                $response = self::response(Array("status" => 1,
                                   "error_code" => self::$ERROR_CODES[$errorCode],
                                   "error_message" => $message), $status, false);
                return($response);
            }
        } else
        if(self::$OUTPUT == 'html') {
            if($echo) {
                self::response($message, $status);
            }
            else {
                return(self::response($message, $status, false));
            }
        }
    }

    /*
     * ATTRIBUTES
     */

    static $ERROR_CODES = Array(
        'EINVAL' => 5001,     /* generic errors */
        'EINSUF' => 5002,
        'EPARSE' => 5003,
        'ENOTFO' => 5004,
        'ENOSUP' => 5005,
        'EERROR' => 5100,
        'EJOBQUEUE' => 10001,
        'EBOTCHECK' => 10002,
        'EUSEREXISTS'=> 10501,
        'EUSERNOTVER'=> 10502,
        'ESOCLOGFAIL'=> 10503,
        'EUSERNOTFO' => 10504,
        'EVERIFINVAL'=> 10505,
        'EVERIFCRED' => 10506,
        'EVERIFALRDY'=> 10507,
        'EPASSWDWRO' => 10508,
        'ESCANLIMIT' => 10509,
        'EDOMFORBID' => 10510,
        'EEMAILEXIST' => 10511,
        'EDOMEXIST' => 10512,
        'EDOMVERIFY' => 10513,
        'ECNF01' => 15001,    /* configuration errors */
        'ECNF02' => 15002,
        'ECNF03' => 15003,
        'ESYS01' => 20001,    /* system errors */
        'ESYS02' => 20002,
        'ESYS03' => 20003,
        'ESYS04' => 20004,
        'ESYS05' => 20005,
        'ESYS06' => 20006,
        'EPRG01' => 25001,    /* programming errors */
    );

    static $ERROR_MESSAGES = Array(
        'EINVAL' => 'Malformed Input Parameter: {1}, Expected {2}. Received {3}',
        'EINSUF' => 'Insufficient Input Parameters. Need {1}',
        'EPARSE' => 'Failed to parse input due to malformed JSON format.',
        'ENOTFO' => 'No such {1} - {2}',
        'ENOSUP' => 'The {1} {2} is not supported by {3}',
        'EERROR' => 'General error - {1}',
        'EJOBQUEUE' => 'There was an error starting your scan : {1}',
        'EBOTCHECK' => 'Failed to verify that user is human',
        'EUSEREXISTS'=> 'A user with id {1} exists',
        'EUSERNOTVER'=> 'The user with id {1} is not verified yet',
        'ESOCLOGFAIL'=> 'Social media login did not succeed - {1}',
        'EUSERNOTFO' => 'No user signed-up with email {1} found',
        'EVERIFINVAL'=> 'Invalid verification for email {1}',
        'EVERIFCRED' => 'Invalid credentials for verifying user with email {1} provided',
        'EVERIFALRDY'=> 'The email {1} is already verified.',
        'EPASSWDWRO' => 'Password mismatch for user with email {1}',
        'ESCANLIMIT' => 'Please wait for your <a href="/v1/scans/details/:{1}" style="display:inline-block;" class="error">scan# {1}</a> to finish before starting another scan',
        'EDOMFORBID' => 'Use "My Domains" or "My Emails" from user options to verify ownership of {1} via domain email verification or page upload.',
        'EEMAILEXIST' => 'The email {1} has been addded already',
        'EDOMEXIST' => 'The domain {1} has been addded and verified already',
        'EDOMVERIFY' => 'Failed to verify ownership of domain {1}',
        'ECNF01' => 'Missing Security Scanner DB configuration',
        'ECNF02' => 'Missing hostname, user or password for Security Scanner DB',
        'ECNF03' => 'Missing Security Scanner Admins configuration',
        'ESYS01' => 'Failed to connect to Security Scanner DB: {1}',
        'ESYS02' => 'Failed to start/commit transactions in Security Scanner DB: {1}',
        'ESYS03' => 'Failed to prepare/execute query: {1}',
        'ESYS04' => 'Failed to connect to Security Scanner cache server: {1}',
        'ESYS05' => 'Failed to set {1} = {2} in Security Scanner cache server: {3}',
        'ESYS06' => 'Failed to get value of {1} in Security Scanner cache server: {2}',
        'EPRG01' => 'Program Defect: Needed {1} param in the input to build {2} query'
    );

    static $OUTPUT = 'json';
}

?>
