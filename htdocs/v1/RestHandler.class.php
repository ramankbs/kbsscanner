<?php

abstract class RestHandler
{
    /*
     * PUBLIC METHODS
     */

    /* Construct: Allow for CORS, assemble and pre-process the data */
    public function __construct($_model, $_object, $_args) {

        $this->model = $_model;
        $this->object = $_object;
        $this->args = $_args;
        if (array_key_exists(0, $this->args)) {
            $this->subject = array_shift($this->args);
        }

        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else {
                throw new Exception(
                    ResponseHandler::errorResponse('ENOSUP', 405, false, 'method',
                                          $_SERVER['HTTP_X_HTTP_METHOD'], 'server'));
            }
        }
    }

    /* process method: impl here includes the basic checks on the request */
    public function process() {
        if(false == $this->_checkRequest()) {
            return(false);
        }
        if(false == $this->_checkMethod() or false == $this->_checkInputJSON()) {
            return(false);
        }
        $this->_handle();
    }

    /*
     * PRIVATE METHODS
     */

    /* This function is called by 'process' method as the first thing */
    private function _checkRequest() {
        switch($this->method) {
            case 'DELETE':
            case 'POST':
                $this->request = $this->_cleanInputs($_POST);
                break;
            case 'GET':
                $this->request = $this->_cleanInputs($_GET);
                break;
            case 'PUT':
                $this->request = $this->_cleanInputs($_GET);
                break;
            default:
                ResponseHandler::errorResponse('ENOSUP', 405, true, 'method', $this->method, 'server');
                return(false);
        }
        return(true);
    }

    /* Clean-up/process the input json */
    private function _cleanInputs($data) {
        $clean_input = Array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->_cleanInputs($v);
            }
        } else {
            $clean_input = trim(strip_tags($data));
        }
        return $clean_input;
    }

    /* Check method */
    protected function _checkMethod() {
        $result = false;
        for($i = 0; $i < sizeof($this->methodsExpected); ++$i) {
            if($this->methodsExpected[$i] == $this->method) {
                $result = true;
                break;
            }
        }
        if(false == $result) {
            ResponseHandler::errorResponse('ENOSUP', 405, true, 'method', $this->method, $this->model . '/' . $this->object);
            return($result);
        }
        return($result);
    }

    /* Check input data */
    protected function _checkInputJSON() {
        if(null == ($this->file = file_get_contents("php://input")) and false == $this->allowNoJson) {
            ResponseHandler::errorResponse('EPARSE', 400, true);
            return(false);
        }
        $this->input = json_decode($this->file, true);
        if(null == $this->input or 0 == sizeof($this->input)) {
            if(false == $this->allowNoJson) {
                ResponseHandler::errorResponse('EPARSE', 400, true);
                return(false);
            } else {
                return(true);
            }
        }
        $attrName = $this->method . "_DataSpec";
        if(null != $attrName) {
            foreach ($this->$attrName as $name => $type) {
                if(!isset($this->input[$name])) {
                    ResponseHandler::errorResponse('EINSUF', 400, true, $name);
                    return(false);
                }
                else {
                    $foundType = 'UNEXPECTED';
                    if(is_string($this->input[$name])) $foundType = 'STRING';
                    else if(is_int($this->input[$name])) $foundType = 'INTEGER';
                    else if(is_bool($this->input[$name])) $foundType = 'BOOL';
                    else if(is_float($this->input[$name])) $foundType = 'FLOAT';
                    if($type != $foundType) {
                        ResponseHandler::errorResponse('EINVAL', 400, true, $name, $type, $foundType);
                        return(false);
                    }
                }
            }
        }
        return(true);
    }

    /* Beginning of the request handling */
    protected function _beginHandle() {
        DbHandler::init();
        // CacheHandler::init();
        // TODO more such inits to come here
    }

    /* End of the request handling */
    protected function _endHandle() {
        // TODO more such closes to come here
        // CacheHandler::close();
        DbHandler::close();
    }

    /*
     * ATTRIBUTES
     */

    protected $method = null;    // http method 
    protected $model = null;     // model base e.g. /users
    protected $object = null;    // data sub-model or action e.g. /users/matches or /users/create
    protected $subject = null;   // model record instance id e.g. /users/matches/:user_id
    protected $args = null;      // arguments beyond the model, object and subject
    protected $file = null;      // PUT request data
    protected $input = null;     // input json parsed as array
    protected $allowNoJson = false;
}

?>
