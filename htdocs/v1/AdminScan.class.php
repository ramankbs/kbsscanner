<?php

class AdminScan extends AdminBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if('GET' == $this->method) {
            if(!isset($this->subject) or '' == $this->subject) {
                $this->handlePrintForm();
            } else {
                $this->handleError();
            }
        } else if('POST' == $this->method) {
            $this->handleDemo();
        }
        $this->_endHandle();
    }

    private function handlePrintForm() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            . AdminPanelHelper::navBar(Array('selected' => 'scan'))
            . $this->printForm()
            . AdminPanelHelper::htmlEnd());
    }

    private function handleError() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            . AdminPanelHelper::navBar(Array('selected' => 'scan'))
            . $this->error()
            . $this->printForm()
            . AdminPanelHelper::htmlEnd());
    }

    private function handleDemo() {
        $url = $_POST['url'];
        $email = $_POST['email'];
        $ip_address = $_POST['user_ip_address'];
        $ret = ScrapeJobSender::send(0, $url, $email, $ip_address);
        
        $ret_parts = explode("|", $ret);
        if($ret_parts[0] == 'ERROR') {
            header('Location: /v1/admin/scan/EJOBQUEUE/' . $ret_parts[1]);
        } else {
            header('Location: /v1/admin/jobs/' . $ret_parts[1]);
        }
    }

    private function error() {
        $errCode = $this->subject;
        $arg = array_shift($this->args);
        ResponseHandler::$OUTPUT = 'html';
        $errStr = ResponseHandler::errorResponse($errCode, 500, false, $arg);
        $str = <<<EOH
   <div class="container centered-content row">
     <h2 class="error">$errStr</h2>
   </div>
EOH;
        return $str;
    }

    private function printForm() {
        $user_remote_address = $_SERVER['REMOTE_ADDR'];
        $str = <<<EOH
   <form action="/v1/admin/scan" method="POST">
   <div class="container">
         <div class="row centered-content">
            <h3>Enter URL to scan</h3>
            <input type="text" name="url" class="url"/>
         </div>
         <div class="row centered-content">
            <h3>Enter email</h3>
            <input type="text" name="email" class="url"/>
         </div>
         <div class="row centered-content">
            <input type="hidden" name="user_ip_address" value="$user_remote_address">
            <input type="submit" value="Submit"/>
         </div>
   </div>
   </form>
EOH;
        return($str);
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET', 'POST' );
}

?>
