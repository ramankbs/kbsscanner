<?php

class DbHandler
{
    /* Private constructor */
    private function __construct() {
    }

    /* Initialize */
    static function init() {
        // read db host, user, passwd config
        if(false == ($handle = fopen("conf/mysql.conf", "r"))) {
            throw new Exception(ResponseHandler::errorResponse('ECNF01', 500, false));
        }
        $host = chop(fgets($handle));
        $dbname = chop(fgets($handle));
        $user = chop(fgets($handle));
        $passwd = chop(fgets($handle));
        fclose($handle);
        if($host == "" or $dbname == "" or $user == "" or $passwd == "") {
            throw new Exception(ResponseHandler::errorResponse('ECNF02', 500, false));
        }
        // try opening a connection to db
        try {
            self::$DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $passwd);
            self::$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$DBH->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        }
        catch(PDOException $e) {
            throw new Exception(ResponseHandler::errorResponse('ESYS01', 500, false, $e->getMessage()));
        }
        // start a transcation
        try {
            self::$DBH->exec("START TRANSACTION;");
        }
        catch(PDOException $e) {
            throw new Exception(ResponseHandler::errorResponse('ESYS02', 500, false, $e->getMessage()));
        }
    }

    /* Wrap up */
    static function close() {
        // commit the transaction started
        try {
            self::$DBH->exec("COMMIT;");
        }
        catch(PDOException $e) {
            throw new Exception(ResponseHandler::errorResponse('ESYS02', 500, false, $e->getMessage()));
        }
    }

    /* Select query */
    static function select($input) {
        $query = "SELECT ";
        if(isset($input['columns']) and (null != $input['columns'])) {
            $query .= self::a2l($input['columns']);
        } else {
            $query .= "* ";
        }
        if(!isset($input['table'])) {
            throw new Exception(ResponseHandler::errorResponse('EPRG01', 500, false, 'table', 'SELECT'));
        }
        $query .= "FROM " . $input['table'] . " ";
        if(isset($input['where']) and (null != $input['where']) and (0 != sizeof($input['where']))) {
            $query .= "WHERE ";
            $query .= self::h2kvl($input['where'], true);
        }
        if(isset($input['order']) and (null != $input['order']) and (0 != sizeof($input['order']))) {
            $query .= "ORDER BY ";
            $query .= self::a2l($input['order']);
        }
        if(isset($input['limit']) and (null != $input['limit']) and
          isset($input['limit']['offset']) and isset($input['limit']['number']))
        {
            $query .= "LIMIT " . $input['limit']['offset'] . ", " . $input['limit']['number'] . " ";
        }
        $query .= ";";
        // echo "Executing: '$query'";
        try {
            $stmt = self::$DBH->prepare($query); // prepare a statement
            if(isset($input['where']) and (null != $input['where']) and (0 != sizeof($input['where']))) {
                $stmt->execute($input['where']); // use the values for binding the injection-free prepared stmt
            } else {
                $stmt->execute();
            }
        }
        catch (PDOException $e) {
            throw new Exception(ResponseHandler::errorResponse('ESYS03', 500, false, $e->getMessage()));
        }

        return($stmt); // array of rows of result
    }

    /* Select query */
    static function selectUsingQuery($query) {
        //echo "Executing: '$query'";
        try {
            $stmt = self::$DBH->query($query); // prepare a statement
        }
        catch (PDOException $e) {
            throw new Exception(ResponseHandler::errorResponse('ESYS03', 500, false, $e->getMessage()));
        }

        return($stmt); // array of rows of result
    }

    /* Insert query */
    static function insert($input) {
        $query = "INSERT INTO ";
        if(!isset($input['table'])) {
            throw new Exception(ResponseHandler::errorResponse('EPRG01', 500, false, 'table', 'INSERT'));
        }
        $query .= $input['table'] . " ";
        if(!isset($input['columns']) or (null == $input['columns']) or (0 == sizeof($input['columns']))) {
            throw new Exception(ResponseHandler::errorResponse('EPRG01', 500, false, 'fields', 'INSERT'));
        }
        $query .= "( " . self::h2kl($input['columns']) . ") VALUES ( " . self::h2vl($input['columns']) . ") ;";
        // echo "Executing: '$query'";
        try {
            // data adaptation
            foreach($input['columns'] as $key => $val) {
                if(is_string($val)) {
                    $input['columns'][$key] = preg_replace("/'/", "&#039;", $val);
                }
            }
            $stmt = self::$DBH->prepare($query); // prepare a statement
            $stmt->execute($input['columns']); // use the values for binding the injection-free prepared stmt
        }
        catch (PDOException $e) {
            throw new Exception(ResponseHandler::errorResponse('ESYS03', 500, false, $e->getMessage()));
        }
    }

    /* Update query */
    static function update($input) {
        $query = "UPDATE ";
        if(!isset($input['table'])) {
            throw new Exception(ResponseHandler::errorResponse('EPRG01', 500, false, 'table', 'UPDATE'));
        }
        $query .= $input['table'] . " SET ";
        if(!isset($input['update']) or (null == $input['update']) or (0 == sizeof($input['update']))) {
            throw new Exception(ResponseHandler::errorResponse('EPRG01', 500, false, 'settables', 'UPDATE'));
        }
        $query .= self::h2kvl($input['update']);
        if(isset($input['where']) and (null != $input['where']) and (0 != sizeof($input['where']))) {
            $query .= "WHERE " . self::h2kvl($input['where'], true);
        }
        $query .= ";";
        //echo "Executing: '$query'";
        $bindings = $input['update'] + $input['where'];
        try {
            // data adaptation
            foreach($input['update'] as $key => $val) {
                if(is_string($val)) {
                    $input['update'][$key] = preg_replace("/'/", "&#039;", $val);
                }
            }
            $stmt = self::$DBH->prepare($query); // prepare a statement
            $stmt->execute($bindings); // use the values for binding the injection-free prepared stmt
        }
        catch (PDOException $e) {
            throw new Exception(ResponseHandler::errorResponse('ESYS03', 500, false, $e->getMessage()));
        }
    }

    /* Delete query */
    static function deleteQuery($input) {
        $query = "DELETE FROM ";
        if(!isset($input['table'])) {
            throw new Exception(ResponseHandler::errorResponse('EPRG01', 500, false, 'table', 'DELETE'));
        }
        $query .= $input['table'] . " ";
        if(isset($input['where']) and (null != $input['where']) and (0 != sizeof($input['where']))) {
            $query .= "WHERE " . self::h2kvl($input['where'], true);
        }
        // echo "Executing: '$query'";
        try {
            $stmt = self::$DBH->prepare($query); // prepare a statement
            if(isset($input['where']) and (null != $input['where']) and (0 != sizeof($input['where']))) {
                $stmt->execute($input['where']); // use the values for binding the injection-free prepared stmt
            } else {
                $stmt->execute();
            }
        }
        catch (PDOException $e) {
            throw new Exception(ResponseHandler::errorResponse('ESYS03', 500, false, $e->getMessage()));
        }
    }

    static function getRow($stmt) {
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if(null != $row) {
            foreach($row as $key => $val) {
                if(is_string($val)) {
                    $row[$key] = preg_replace("/&#039;/", "'", $val);
                }
            }
        }
        return($row);
    }

    /* array to comma separated list */
    static function a2l($array) {
        $sz = sizeof($array);
        $var = '';
        for($i = 0; $i < $sz-1; ++$i) {
            $var .= $array[$i] . ", ";
        }
        $var .= $array[$sz-1] . " ";
        return($var);
    }

    /* assoc array to comma separated list of key = value clauses */
    static function h2kvl(&$hash, $and = false) {
        $var = '';
        if(true == $and) {
            $rangeHash = Array();
            foreach ($hash as $col => $val) {
                if(!is_array($val)) {
                    $var .= "$col = :$col AND "; // PDO injection prevention
                }
                else {
                    // range provided from backend - use them directly
                    $rangeHash["min_".$col] = $val[0];
                    $rangeHash["max_".$col] = $val[1];
                    $var .= "$col >= :min_$col AND $col <= :max_$col AND ";
                    unset($hash[$col]);
                }
            }
            $hash = $hash + $rangeHash;
            $var = preg_replace("/AND $/", " ", $var); // remove the trailing comma, keep the space separator
        }
        else {
            foreach ($hash as $col => $val) {
                $var .= "$col = :$col, "; // PDO injection prevention
            }
            $var = preg_replace("/, $/", " ", $var); // remove the trailing comma, keep the space separator
        }
        return($var);
    }

    /* assoc array to comma separated list of keys only */
    static function h2kl($hash) {
        $var = '';
        foreach ($hash as $col => $val) {
            $var .= $col . ", ";
        }
        $var = preg_replace("/, $/", " ", $var); // remove the trailing comma, keep the space separator
        return($var);
    }

    /* assoc array to comma separated list of value only */
    static function h2vl($hash) {
        $var = '';
        foreach ($hash as $col => $val) {
            $var .= ":" . $col . ", "; // PDO injection prevention
        }
        $var = preg_replace("/, $/", " ", $var); // remove the trailing comma, keep the space separator
        return($var);
    }
    

    /* ATTRIBUTES */
    static $DBH = null;
}

?>
