<?php

class AdminUtils
{
    private function __construct() {
    }

    static function getReferer() {
        $referer = $_SERVER['HTTP_REFERER'];
       
        $referer = preg_replace("/^.*\.com\//", "", $referer);
        $referer = preg_replace("/\/.*$/", "", $referer);
        return($referer);
    }

    static function getSiteName() {
        $site = $_SERVER['HTTP_HOST'];
        $site = preg_replace('/www\./', '', $site);
        $site = preg_replace('/\.com/', '', $site);
        return($site);
    }

    static function getIpAddr() {
        $ip = '';
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return($ip);
    }

    static function rands($v1, $v2) {
        $r1 = mt_rand();
        $r2 = mt_rand();
        $r3 = mt_rand();
        $input = uniqid($v1 . $r1 . time() . $r2 . $v2 . $r3);
        $r = md5($input);
        return($r);
    }

    static function getPrefix() {
        $r1 = mt_rand();
        return($r1);
    }

    static function getKey($v1, $v2) {
        $r = self::rands($v1, $v2);
        $enc = CryptHandler::encrypt($r, AppGlobals::$SS_KEY);
        return(array($r, $enc));
    }

}

?>
