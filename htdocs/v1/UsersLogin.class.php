<?php

class UsersLogin extends PagesBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        $this->_beginHandle();
        $this->handleLogin();
        $this->_endHandle();
    }

    private function handleLogin() {
        $referer = AdminUtils::getReferer();
        $_POST['password'] = md5($_POST['password']);
        $createSession = false;
        $stmt = DbHandler::select(Array('table' => 'users',
                                        'columns' => Array('id', 'is_active', 'auth'),
                                        'where' => Array('login_id' => $_POST['email'])));
        $row = DbHandler::getRow($stmt);
        if(null != $row) {
            if(true == $row['is_active']) {
                if($_POST['password'] != $row['auth']) {
                    header ("Location: /$referer/EPASSWDWRO/" . $_POST['email']);
                } else {
                    $site = AdminUtils::getSiteName();
                    $createSession = true;
                    if(null == ($route = CookieManager::get(AppGlobals::$ROUTE_COOKIE_NAME))) {
	                header ("Location: /");
                    } else {
	                header ("Location: /$route");
                        CookieManager::setDomainCookie(AppGlobals::$ROUTE_COOKIE_NAME, '', -365*24*60*60, $site);
                    }
                }
            } else {
                header ("Location: /$referer/EUSERNOTVER/" . $_POST['email']);
            }
        } else {
            header ("Location: /$referer/EUSERNOTFO/" . $_POST['email']);
        }
        if($createSession) {
            $now = date("Y-m-d H:i:s");
            $session = md5($now . uniqid());
            // Create a session
            DbHandler::insert(Array('table' => 'sessions',
                                    'columns' => Array('user_id' => $row['id'], 'session_id' => $session,
                                                       'created_on' => $now, 'modified_on' => $now)));
            $site = AdminUtils::getSiteName();
            CookieManager::setDomainCookie(AppGlobals::$USER_COOKIE_NAME, $session, 15 * 60, $site);
        }
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'POST' );

    protected $allowNoJson = true;
}

?>
