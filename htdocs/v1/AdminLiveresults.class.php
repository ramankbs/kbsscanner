<?php



class AdminLiveresults extends AdminBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if(!isset($this->args) or sizeof($this->args) == 0) {
            $this->printError();
        } else {
            $this->handleScraping();
        }
        $this->_endHandle();
    }

    private function handleScraping() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            . AdminPanelHelper::navBar(Array('selected' => 'none')));

        $this->scrape();
        echo AdminPanelHelper::htmlEnd();
    }

    private function scrape() {
        $url = join("/", $this->args);
        $str = <<<EOH
   <div class="container centered-content">
     <h3>Scraping: $url</h3>
   </div>
EOH;
        echo $str;

        $sh = new ScrapeHandler($url);
        $sh->scrapeAllModules();
    }

    private function printError() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            . AdminPanelHelper::navBar(Array('selected' => 'none'))
            . $this->error()
            . AdminPanelHelper::htmlEnd());
    }

    private function error() {
        $str = <<<EOH
   <div class="container centered-content row">
     <h2 class="error">Please specify a URL to scan</h2>
   </div>
EOH;
        return $str;
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET' );
}

?>
