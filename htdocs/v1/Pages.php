<?php

spl_autoload_register(function ($class) {
    include "$class.class.php";
});

header("Content-Type: text/html; charset=utf-8");

try {
    $handler = RestHandlerFactory::getHandler($_REQUEST['request'], true);
    if(null != $handler) {
        $handler->process();
    } else {
        ResponseHandler::$OUTPUT = 'html';
        ResponseHandler::errorResponse('ENOTFO', 404, true, 'endpoint', $_REQUEST['request']);
    }
}
catch(Exception $e) {
    echo $e->getMessage();
}

?>
