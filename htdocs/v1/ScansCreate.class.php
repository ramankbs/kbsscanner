<?php

class ScansCreate extends SessionPagesBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        $this->handleScan();
        $this->_endHandle();
    }

    private function handleScan() {
        $referer = AdminUtils::getReferer();
        //$captcha = $_POST['captcha'];
        //if($_SESSION['phrase'] != $captcha) {
            //header("Location: /$referer/EBOTCHECK/");
            //return;
        //}
        $url = $_POST['url'];
        $url = $this->domain_verification($url);
        $ip_address = $_POST['user_ip_address'];
        $is_anon = false;
        if(null != CookieManager::get(AppGlobals::$USER_COOKIE_NAME)) {
            if(false == ($user_id = $this->_checkSession())) return;
        } else
        if(null != ($anon = CookieManager::get(AppGlobals::$ANON_COOKIE_NAME))) {
            if(false == ($user_id = $this->_getAnonSession())) {
                header('Location: /');
                return;
            }
            $is_anon = true;
        } else {
            // create anon user and set a cookie
            $now = date('Y-m-d H:i:s');
            $anon = md5($now . uniqid());
            DbHandler::insert(Array('table' => 'users',
                                     'columns' => Array('login_id' => 'anon',
                                                       'first_name' => 'anon',
                                                       'last_name' => 'anon',
                                                       'auth' => $anon,
                                                       'company' => 'anon',
                                                       'position' => 'anon',
                                                       'ip_address' => $ip_address,
                                                       'created_on' => $now, 'modified_on' => $now)));
            $stmt = DbHandler::select(Array('table' => 'users', 'columns' => Array('id'), 'where' => Array('login_id' => 'anon', 'auth' => $anon)));
            $row = DbHandler::getRow($stmt);
            $user_id = $row['id'];
            $site = AdminUtils::getSiteName();
            CookieManager::setDomainCookie(AppGlobals::$ANON_COOKIE_NAME, $anon, 30 * 24 * 60 * 60, $site);
            $is_anon = true;
        }
        // Check if a scan is already running for this user
        $stmt = DbHandler::select(Array('table' => 'jobs', 'columns' => Array('job_id'), 'where' => Array('user_id' => $user_id, 'is_completed' => false)));
        if(null != ($row = DbHandler::getRow($stmt))) {
            $job_id = $row['job_id'];
            header("Location: /$referer/ESCANLIMIT/$job_id");
            return;
        }
        // Check if the domain matches
        if(!$is_anon) {
            if(preg_match("/^http:/", $url)) {
                $domain = preg_replace("/^http:\/\//", "", $url);
            } else if(preg_match("/^https:/", $url)) {
                $domain = preg_replace("/^https:\/\//", "", $url);
            }
            $domain_parts = explode("/", $domain);
            $domain = array_shift($domain_parts);
            $stmt = DbHandler::select(Array('table' => 'allowed_domains', 'columns' => Array('domain'), 'where' => Array('user_id' => $user_id, 'is_verified' => true)));
            $is_allowed = false;
            while(null != ($row = DbHandler::getRow($stmt))) {
                $allowed = $row['domain'];
                if(preg_match("/$allowed/", $domain) or preg_match("/$domain/", $allowed)) {
                    $is_allowed = true;
                    break;
                }
            }
            if(false == $is_allowed) {
                header("Location: /$referer/EDOMFORBID/$domain");
                return;
            }
        }
        $stmt = DbHandler::select(Array('table' => 'users', 'columns' => Array('login_id'), 'where' => Array('id' => $user_id)));
        $row = DbHandler::getRow($stmt);
        $email = $row['login_id'];
        $ret = ScrapeJobSender::send($user_id, $url, $email, $ip_address);
        $ret_parts = explode("|", $ret);
        if($ret_parts[0] == 'ERROR') {
            header("Location: /$referer/EJOBQUEUE/" . $ret_parts[1]);
        } else {
            header("Location: /v1/scans/details/:" . $ret_parts[1]);
        }
    }

    private function domain_verification($url) {
        
        $ch = curl_init();
        $url_arr = array();
        
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
        curl_setopt($ch, CURLOPT_URL, $url);
        $out = curl_exec($ch);
    
        // line endings is the wonkiest piece of this whole thing
        $out = str_replace("\r", "", $out);
    
        // only look at the headers
        $headers_end = strpos($out, "\n\n");
        if( $headers_end !== false ) { 
            $out = substr($out, 0, $headers_end);
        }
    
        $headers = explode("\n", $out);
        foreach($headers as $header) {
            if( substr($header, 0, 10) == "Location: " ) {
                $target = substr($header, 10);
                $url_arr[] = $target;
                //echo "[$url] redirects to [$target]<br>";
                //continue 2;
            }
        }
    
        $arr_url = parse_url($url);
        $domain_actual_url = $arr_url['host'];
        $url_arr[] = $arr_url['scheme']."://".$arr_url['host'];
        
        return($url_arr[0]);
    }
    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'POST' );
}

?>
