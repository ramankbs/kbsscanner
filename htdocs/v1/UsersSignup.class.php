<?php

class UsersSignup extends PagesBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        $this->_beginHandle();
        if('POST' == $this->method) {
            $this->handleSignup();
        } else if('GET' == $this->method) {
            if('verify' == $this->subject) {
                $this->handleVerification();
            }
        }
        $this->_endHandle();
    }

    private function handleSignup() {
        $referer = AdminUtils::getReferer();
        //$captcha = $_POST['captcha'];
        //if($_SESSION['phrase'] != $captcha) {
            //header("Location: /$referer/EBOTCHECK/");
           // return;
        //}
        $_POST['password'] = md5($_POST['password']);
        $stmt = DbHandler::select(Array('table' => 'users',
                                        'columns' => Array('id'),
                                        'where' => Array('login_id' => $_POST['email'])));
        $row = DbHandler::getRow($stmt);
        if(null == $row) { // this user does not exist yet
            // See if an anonymous user was created already who ran scans
            $user_id = null;
            if(null != ($anon = CookieManager::get(AppGlobals::$ANON_COOKIE_NAME))) {
                $stmt2 = DbHandler::select(Array('table' => 'users',
                                                'columns' => Array('id'),
                                                'where' => Array('auth' => $anon)));
                if(null != ($row2 = DbHandler::getRow($stmt2))) {
                    $user_id = $row2['id'];
                }
            }
            $now = date("Y-m-d H:i:s");

            if(null == $user_id) {
                // Create user
                DbHandler::insert(Array('table' => 'users',
                                        'columns' => Array('login_id' => $_POST['email'],
                                                       'first_name' => $_POST['first-name'],
                                                       'last_name' => $_POST['last-name'],
                                                       'auth' => $_POST['password'],
                                                       'company' => $_POST['company'],
                                                       'position' => $_POST['position'],
							'phonenumber' => $_POST['phonenumber'],
							'address' => $_POST['address'],
                                                       'created_on' => $now, 'modified_on' => $now)));
                $stmt2 = DbHandler::select(Array('table' => 'users',
                                            'columns' => Array('id'),
                                            'where' => Array('login_id' => $_POST['email'])));
                $row2 = DbHandler::getRow($stmt2);
                $user_id = $row2['id'];
            } else {
                // Convert anon user to signed-up
                DbHandler::update(Array('table' => 'users',
                                        'update' => Array('login_id' => $_POST['email'],
                                                       'first_name' => $_POST['first-name'],
                                                       'last_name' => $_POST['last-name'],
                                                       'auth' => $_POST['password'],
                                                       'company' => $_POST['company'],
                                                       'position' => $_POST['position'],
							'phonenumber' => $_POST['phonenumber'],
							'address' => $_POST['address'],
                                                       'created_on' => $now, 'modified_on' => $now),
                                        'where' => Array('id' => $user_id)));
                $site = AdminUtils::getSiteName();
                CookieManager::setDomainCookie(AppGlobals::$ANON_COOKIE_NAME, $anon, -(365 * 30 * 24 * 60 * 60), $site);
            }
            $time = time();
            $token = base64_encode($time . $_POST['email']);
            DbHandler::insert(Array('table' => 'verifications',
                                    'columns' => Array('user_id' => $user_id,
                                                       'timestamp' => $time,
                                                       'verify_token' => $token)));
            $domain = preg_replace("/^.*@/", "", $_POST['email']);
            DbHandler::insert(Array('table' => 'allowed_domains',
                                    'columns' => Array('user_id' => $user_id,
                                                       'domain' => $domain, 'is_verified' => true)));
            $first_name = $_POST['first-name'];
            $email = $_POST['email'];
            $htmlMessage = <<<EOH
<p>Hi $first_name,</p>
<p>Thanks for registering with us. Please click on the following link to verify your account with us.</p>
<p><a href="http://scan.zapts.com/v1/users/signup/verify/$email/$token"/>Verify Your Account</a></p>
<p>If the above link does not work, please copy and paste the following into your browser and enter</p>
<p>http://scan.zapts.com/v1/users/signup/verify/$email/$token</p>
<p>Thanks,</p>
<p>Security Scanner Team</p>
EOH;
            $textMessage = <<<EOH
Hi $first_name,

Thanks for registering with us.

Please copy and paste the following link into your browser and enter to verify your account

http://scan.zapts.com/v1/users/signup/verify/$email/$token

Thanks,
Security Scanner Team
EOH;
            AwsSesMail::sendMail(Array($_POST['email']), "Please verify your email with scan.zapts.com", $textMessage, $htmlMessage);
	    header ("Location: /$referer/success");
        }
        else {
            // Send 'user exists'
            header ("Location: /$referer/EUSEREXISTS/" . $_POST['email']);
        }
    }

    private function handleVerification() {
        $email = array_shift($this->args);
        $token = array_shift($this->args);
        $stmt = DbHandler::select(Array('table' => 'users',
                                        'columns' => Array('id', 'is_active'),
                                        'where' => Array('login_id' => $email)));
        $row = DbHandler::getRow($stmt);
        if(null == $row) {
            // Send 'no such user'
            header ("Location: /sign-up/EUSERNOTFO/$email");
            return;
        }
        $user_id = $row['id'];
        $is_active = $row['is_active'];
        if(true == $is_active) {
            // Send 'already verified'
            header ("Location: /sign-up/EVERIFALRDY/$email");
            return;
        }
        $stmt = DbHandler::select(Array('table' => 'verifications',
                                        'columns' => Array('timestamp'),
                                        'where' => Array('user_id' => $user_id, 'verify_token' => $token)));
        $row = DbHandler::getRow($stmt);
        if(null == $row) {
            // Send 'invalid verification'
            header ("Location: /sign-up/EVERIFINVAL/$email");
            return;
        }
        $time = $row['timestamp'];
        $gen_token = base64_encode($time . $email);
        if($token != $gen_token) {
            // Send 'invalid token'
            header ("Location: /sign-up/EVERIFCRED/$email");
            return;
        }
        DbHandler::update(Array('table' => 'users',
                                'update' => Array('is_active' => true),
                                'where' => Array('id' => $user_id)));
        DbHandler::deleteQuery(Array('table' => 'verifications',
                                     'where' => Array('user_id' => $user_id, 'verify_token' => $token)));
        header ("Location: /sign-up/verified");
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'POST', 'GET' );

    protected $allowNoJson = true;
}

?>
