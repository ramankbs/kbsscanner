<?php

class SignIn extends PagesBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if(!isset($this->object)) {
            $this->handleForm();
        } else {
            if(!isset($_SERVER['HTTP_REFERER']) or null == ($r = $_SERVER['HTTP_REFERER']) or '' == $r) {
                header("Location: /");
            }
            else {
                if('success' == $this->object) {
                    header("Location: /");
                } else {
                    $this->handleError();
                }
            }
        }
        $this->_endHandle();
    }

    private function handleForm() {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->page()
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function handleError() {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->error()
            . $this->page()
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function error() {
        $errCode = $this->object;
        $errArg = $this->subject;
        ResponseHandler::$OUTPUT = 'html';
        $message = ResponseHandler::errorResponse($errCode, 200, false, $errArg);
        $str = <<<EOH
   <div class="notice error">$message</div>
EOH;
        return($str);
    }

    private function page() {
        $str = <<<EOH
  <div class="container body-margin-top centered-content">
    <div class="form-container centered-content">
      <div class="log-form">
        <div class="form-row">
          <h4>Sign-in through Your Security Scanner account</h4>
        </div>
        <div class="form-row centered-content">
          <div style="color:#999;">Don't have an account with us? <a href="/sign-up" style="display:inline-block;">Sign-up</a></div>
        </div>
        <form id="user-login" action="/v1/users/login/" method="POST" name="users-login" onsubmit="return validateSigninForm();">
        <div class="form-row centered-content">
          <table align="center">
            <tr>
              <td style="color:#777;">Email</td>
              <td><input type="text" name="email" id="email" inittext="Your email"/></td>
            </tr>
            <tr>
              <td style="color:#777;">Password</td>
              <td><input type="password" name="password" id="password" /></td>
            </tr>
            <tr>
              <td style="color:#777;">Remember Me</td>
              <td><input type="checkbox" name="rememberme" id="rememberme"/></td>
            </tr>
          </table>
        </div>
        <div class="form-row">
          <div style="margin-bottom:10px;"><input class="type-1" type="submit" id="login" value="Sign In"/></div>
        </div>
        </form>
        <div class="form-row centered-content log-options">
          <div><a href="/forgot-password">Forgot Password?</a></div>
        </div>
      </div>
    </div>
  </div>
EOH;
        return($str);
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET' );
}

?>