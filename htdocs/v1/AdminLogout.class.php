<?php

class AdminLogout extends AdminBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->handlePost();
    }

    private function handlePost() {
        $site = AdminUtils::getSiteName();
        $val = CookieManager::get(AppGlobals::$ADMIN_COOKIE_NAME);
        CookieManager::setDomainCookie(AppGlobals::$ADMIN_COOKIE_NAME, $val, -365*24*10*60, $site);
        CookieManager::setDomainCookie(AppGlobals::$SUPERADMIN_COOKIE_NAME, $val, -365*24*10*60, $site);
        header ("Location: /v1/admin/login");
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'POST' );
}

?>
