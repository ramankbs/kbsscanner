<?php

spl_autoload_register(function ($class) {
    include "$class.class.php";
});

try {
    // check for cookie and redirect to login in case the cookie is not set
    if(null == CookieManager::get(AppGlobals::$USER_COOKIE_NAME)
        && !preg_match("/scans\/details/", $_REQUEST['request'])
        && !preg_match("/scans\/create/", $_REQUEST['request'])
        && !preg_match("/users\/emails\/.*\/verify/", $_REQUEST['request'])
        && !preg_match("/users\/login/", $_REQUEST['request'])
        && !preg_match("/users\/logout/", $_REQUEST['request'])
        && !preg_match("/users\/signup/", $_REQUEST['request'])
        && !preg_match("/users\/forgot/", $_REQUEST['request']))
    {
        header(302);
        header ("Location: /sign-in");
        return;
    }

    if(null != ($cookie = CookieManager::get(AppGlobals::$USER_COOKIE_NAME))
        && !preg_match("/users\/logout/", $_REQUEST['request']))
    {  
        $site = AdminUtils::getSiteName();
        CookieManager::setDomainCookie(AppGlobals::$USER_COOKIE_NAME, $cookie, 30*60, $site);
    }

    header("Content-Type: text/html; charset=utf-8");

    $handler = RestHandlerFactory::getHandler($_REQUEST['request']);
    if(null != $handler) {
        $handler->process();
    } else {
        ResponseHandler::$OUTPUT = 'html';
        ResponseHandler::errorResponse('ENOTFO', 404, true, 'endpoint', $_REQUEST['request']);
    }
}
catch(Exception $e) {
    echo $e->getMessage();
}

?>
