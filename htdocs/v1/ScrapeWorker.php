<?php

spl_autoload_register(function ($class) {
    include "$class.class.php";
});

require_once 'AwsSesMail.class.php';

// read gearman host, port from config
if(false == ($handle = fopen("conf/gearman.conf", "r"))) {
    echo "Can't open gearman configuration\n";
    exit;
}
$host = chop(fgets($handle));
$port = (int)(chop(fgets($handle)));
fclose($handle);

echo "Connecting with gearman server @$host:$port\n";
$gmworker= new GearmanWorker();
$gmworker->addServer($host, $port);
$gmworker->addFunction("scrape", "scrape_fn");
print "Waiting for job...\n";
while($gmworker->work())
{
    if ($gmworker->returnCode() != GEARMAN_SUCCESS) {
        echo "return_code: " . $gmworker->returnCode() . "\n";
        break;
    }
}

function scrape_fn($job)
{
    $handle = $job->handle();
    $workload= json_decode($job->workload(), true);
    sleep(1);

    DbHandler::init();

          $url = $workload['url'];
          $email = $workload['email'];
          $job_id = $workload['jobid'];
          $stmt = DbHandler::select(Array('table' => 'jobs', 'columns' => Array('user_id'), 'where' => Array('job_id' => $job_id)));
          $row = DbHandler::getRow($stmt);
          $user_id = $row['user_id'];
          $now = date("Y-m-d H:i:s");
          echo "$now: Starting scan job# $job_id, url - $url, recipient - $email\n";


    $sh = new ScrapeHandler($url, $job_id);
    $sh->scrapeAllModules();

    $now = date("Y-m-d H:i:s");
    DbHandler::update(Array('table' => 'jobs', 'update' => Array('is_completed' => true, 'completed_on' => $now), 'where' => Array('handle' => $handle)));

    DbHandler::close();

    if('anon' != $email) {
        echo "Job# $job_id completed. Sending email ...\n";
        if(false == sendConfirmationMail($job_id, $user_id, $url, $email)) {
            echo "\nFailed to send email to " , $email . "\n";
        } else {
            echo "\nSent confirmation email to " , $email . "\n";
        }
    }
    echo "$now: Completed job# $job_id ...\n";
}

function sendConfirmationMail($job_id, $user_id, $url, $to) {
    if(0 != $user_id) {
        $link = "http://scan.zapts.com/v1/scans/details/:$job_id";
    } else {
        $link = "http://scan.zapts.com/v1/admin/jobs/$job_id";
    }
    $htmlMessage = <<<EOH
<p>Hi</p>
<p>Your scraping job# $job_id for scanning $url has completed.</p>
<p>Here is the link to access the details</p>
<p><a href="$link"/>Job #$job_id</a></p>
<p>If the above link does not work, please copy and paste the following into your browser and enter</p>
<p>$link</p>
<p>Thanks,</p>
<p>Security Scanner Team</p>
EOH;
    $textMessage = <<<EOH
Hi,

Your scraping job# $job_id for scanning $url has completed.

Please copy and paste the following link into your browser and enter to see details of scanning.

$link

Thanks,
Security Scanner Team
EOH;
    AwsSesMail::sendMail(Array($to), "Your scanning job# $job_id has completed", $textMessage, $htmlMessage);
    return(true);
}

?>
