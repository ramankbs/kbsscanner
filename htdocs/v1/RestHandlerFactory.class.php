<?php

class RestHandlerFactory
{
    /* private constructor */
    private function __construct() {
    }

    /* Get a handler object: Allow for CORS, assemble and pre-process the data */
    static function getHandler($request, $modelOnly=false) {
        $args = explode('/', rtrim($request, '/'));
        $model = array_shift($args); // model base e.g. /users
        if(1 == preg_match("/-/", $model)) {
            $modelParts = explode("-", $model);
        } else {
            $modelParts = Array($model);
        }
        $modelUc = '';
        for($i = 0; $i < sizeof($modelParts); ++$i) {
            $modelUc .= ucfirst($modelParts[$i]);
        }
        $className = $modelUc;
        if (array_key_exists(0, $args)) {
            // data sub-model or action e.g. /users/login or /users/create
            $object = array_shift($args);
            $objectUc = ucfirst($object);
            if(false == $modelOnly) $className .= $objectUc;
        } else {
            $object = null;
        }
        if(true == class_exists($className)) {
            return new $className($model, $object, $args);
        } else {
            return null;
        }
    }
}

?>
