<?php

require_once 'AwsSesMail.class.php';

try {
    $message = 'Hi Reetesh, this is a test message for seeing how to send email using PHP SDK of AWS using SES PHP API. Thanks, IHR';
    AwsSesMail::sendMail(Array('reeteshranjan@yahoo.in'), 'Test of SES emailing', $message, $message);
}
catch(Exception $e) {
    /* Assume an HTTP header is set, and the exception has a JSON format
     * response encoded message. So at this point, we just echo it. */
    echo $e->getMessage();
}

?>
