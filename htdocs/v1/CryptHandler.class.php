<?php

class CryptHandler
{

    static function encrypt($data, $key) {
        $key = substr(hash('sha256', self::$SALT . $key . self::$SALT), 0, 32);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $data, MCRYPT_MODE_ECB, $iv));
        return $encrypted;
    }

    static function decrypt($data, $key) {
        $key = substr(hash('sha256', self::$SALT.$key.self::$SALT), 0, 32);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, base64_decode($data), MCRYPT_MODE_ECB, $iv);
        return $decrypted;
    }

    static $SALT = 'XJWjZRUkw3eEpmV1JVNXc5SEFcXpQMmM1BdzdTbUx6VjI1MmJUcKclRjZWN2RmVFem1FUTW1HN1A2c1hOVDdNTZLODVLSm5M';
}

?>
