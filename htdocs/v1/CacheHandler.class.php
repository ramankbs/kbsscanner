<?php

class CacheHandler
{
    /* Private constructor */
    private function __construct() {
    }

    /* Initialize */
    static function init() {
        // open Redis connection
        try {
            self::$REDIS = new Redis();
            self::$REDIS->connect('127.0.0.1');
        }
        catch(RedisException $e) {
            throw new Exception(ResponseHandler::errorResponse('ESYS04', 500, false, $e->getMessage()));
        }
    }

    /* Wrap up */
    static function close() {
        // commit the transaction started
        try {
            self::$REDIS->close();
        }
        catch(RedisException $e) {
        }
    }

    /* Set */
    static function set($key, $value) {
        try {
            $result = self::$REDIS->set($key, $value);
            if(false == $result) {
                throw new Exception(ResponseHandler::errorResponse('ESYS05', 500, false, $key, $value, $e->getMessage()));
            }
        }
        catch (RedisException $e) {
            self::$REDIS->close();
            throw new Exception(ResponseHandler::errorResponse('ESYS05', 500, false, $key, $value, $e->getMessage()));
        }
    }

    /* Get */
    static function get($key) {
        try {
            $result = self::$REDIS->get($key);
            if(false != $result) { return($result); }
            else { return(null); }
        }
        catch (RedisException $e) {
            self::$REDIS->close();
            throw new Exception(ResponseHandler::errorResponse('ESYS06', 500, false, $key, $e->getMessage()));
        }
    }

    /* ATTRIBUTES */
    static $REDIS = null;
}

?>
