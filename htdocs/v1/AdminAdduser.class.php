<?php

class AdminAdduser extends AdminBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if('GET' == $this->method) {
            if(!isset($this->subject) or null == $this->subject or '' == $this->subject) {
                $this->handleAddUsersForm();
            } else {
                $this->handleAck();
            }
        } else if('POST' == $this->method) {
            $this->handleAdduser();
        }
        $this->_endHandle();
    }

    private function handleAddUsersForm() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            . AdminPanelHelper::navBar(Array('selected' => 'adduser'))
            . $this->addusersForm()
            . AdminPanelHelper::htmlEnd());
    }

    private function handleAck() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            . AdminPanelHelper::navBar(Array('selected' => 'adduser'))
            . $this->ack_user()
            . $this->addusersForm()
            . AdminPanelHelper::htmlEnd());
    }

    private function ack() {
        $user_id = array_shift($this->args);
        $str = <<<EOH
  <div class="container centered-content row">
     <h3>User with id $user_id verified successfully</h3>
  </div>
EOH;
        return($str);
    }

    private function addusersForm() {
        $str = <<<EOH
  <div class="container body-margin-top centered-content">
    <div class="form-container centered-content signup-form">
      <div class="log-form">
        <div class="form-row">
          <h4>Add User</h4>
        </div>
        <form action="/v1/admin/Adduser" method="POST" name="admin-add-user" onsubmit="return validateSignupFormadmin();">
        <div class="form-row centered-content">
          <table align="center">
            <tr>
              <td style="color:#777;">First Name<em>*</em></td>
              <td><input type="text" name="first-name" inittext="First name"/></td>
            </tr>
            <tr>
              <td style="color:#777;">Last Name<em>*</em></td>
              <td><input type="text" name="last-name" inittext="Last name"/></td>
            </tr>
            <tr>
              <td style="color:#777;">Email<em>*</em></td>
              <td><input type="text" name="email" inittext="Your email"/></td>
            </tr>
            <tr>
              <td style="color:#777;">Password<em>*</em></td>
              <td><input type="password" name="password"/></td>
            </tr>
            <tr>
              <td style="color:#777;">Company<em>*</em></td>
              <td><input type="text" name="company" inittext="Your company"/></td>
            </tr>
            <tr>
              <td style="color:#777;">Position<em>*</em></td>
              <td><input type="text" name="position" inittext="Your position in your company"/></td>
            </tr>
            <tr>
              <td style="color:#777;">Phone Number</td>
              <td><input type="text" name="phonenumber" inittext="Phone Number"/></td>
            </tr> 
            <tr>
              <td style="color:#777;">Address</td>
              <td><input type="text" name="address" inittext="Address"/></td>
            </tr>  
	</table>
        </div>
        <div class="form-row" id="user-signup-button">
          <div style="margin-bottom:10px;">
		<input type="hidden" name="active" value="1" />
		<input class="type-1" type="submit" value="Add User"/>
	   </div>
        </div>
        </form>
      </div>
    </div>
  </div>
EOH;
        return($str);
    }

    private function handleAdduser() {
		$_POST['password'] = md5($_POST['password']);
		$email = $_POST['email'];
		$now = date("Y-m-d H:i:s");
        	$stmt = DbHandler::select(Array('table' => 'users',
                                        'columns' => Array('id'),
                                        'where' => Array('login_id' => $_POST['email'])));
        
		$row = DbHandler::getRow($stmt);

			if($row == null){

				DbHandler::insert(Array('table' => 'users',
				                        'columns' => Array('login_id' => $_POST['email'],
				                                       'first_name' => $_POST['first-name'],
				                                       'last_name' => $_POST['last-name'],
									'is_active' => $_POST['active'],
				                                       'auth' => $_POST['password'],
				                                       'company' => $_POST['company'],
				                                       'position' => $_POST['position'],
									'phonenumber' => $_POST['phonenumber'],
									'address' => $_POST['address'],
				                                       'created_on' => $now, 'modified_on' => $now)));
				header("Location: /v1/admin/adduser/success");

			}else{

				header("Location: /v1/admin/adduser/failed/$email");
			}


    }

   private function ack_user(){

if (strpos($_SERVER['REQUEST_URI'],'success') !== false) {
	$str = <<<EOH
  <div class="container centered-content row">
     <h3>User created successfully</h3>
  </div>
EOH;
				
}elseif (strpos($_SERVER['REQUEST_URI'],'failed') !== false) {

$get_email = explode('failed/', $_SERVER['REQUEST_URI']);
$final_email = $get_email[1];

	$str = <<<EOH
  <div class="container centered-content row">
     <h3>User with email - $final_email already exists</h3>
  </div>
EOH;
				
}

return($str);
	}

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET', 'POST' );
}

?>
