<?php

class UsersForgot extends PagesBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        $this->_beginHandle();
        if('POST' == $this->method) {
            if(isset($_POST['password'])) {
                $this->handleReset();
            } else {
                $this->handleForgot();
            }
        } else if('GET' == $this->method) {
            if('verify' == $this->subject) {
                $this->handleVerification();
            }
        }
        $this->_endHandle();
    }

    private function handleForgot() {
        $referer = AdminUtils::getReferer();
        $stmt = DbHandler::select(Array('table' => 'users',
                                        'columns' => Array('id', 'first_name'),
                                        'where' => Array('login_id' => $_POST['email'])));
        $row = DbHandler::getRow($stmt);
        if(null != $row) { // found the user
            $user_id = $row['id'];
            $name = $row['first_name'];
            $time = time();
            $token = base64_encode($time . $_POST['email']);
            DbHandler::insert(Array('table' => 'verifications',
                                    'columns' => Array('user_id' => $user_id,
                                                       'timestamp' => $time,
                                                       'verify_token' => $token)));
            $email = $_POST['email'];
            $htmlMessage = <<<EOH
<p>Hi $first_name,</p>
<p>Please click on the following link to verify your account and choosing a new password.</p>
<p><a href="http://scan.zapts.com/v1/users/forgot/verify/$email/$token"/>Choose New Password</a></p>
<p>If the above link does not work, please copy and paste the following into your browser and enter</p>
<p>http://scan.zapts.com/v1/users/forgot/verify/$email/$token</p>
<p>Thanks,</p>
<p>Security Scanner Team</p>
EOH;
            $textMessage = <<<EOH
Hi $name,

Thanks for registering with us.

Please copy and paste the following link into your browser and enter to verify your account and choose new password.

http://scan.zapts.com/v1/users/forgot/verify/$email/$token

Thanks,
Security Scanner Team
EOH;
            AwsSesMail::sendMail(Array($_POST['email']), "Instructions to reset password of your account on scan.zapts.com", $textMessage, $htmlMessage);
	    header ("Location: /$referer/success");
        }
        else {
            // User not found
            header ("Location: /$referer/EUSERNOTFO/" . $_POST['email']);
        }
    }
    private function handleReset() {
        $referer = AdminUtils::getReferer();
        $_POST['password'] = md5($_POST['password']);
        $stmt = DbHandler::select(Array('table' => 'users',
                                        'columns' => Array('id'),
                                        'where' => Array('login_id' => $_POST['email'])));
        $row = DbHandler::getRow($stmt);
        if(null != $row) { // found the user
            $now = date("Y-m-d H:i:s");
            $user_id = $row['id'];
            $email = $_POST['email'];
            DbHandler::update(Array('table' => 'users', 'update' => Array('auth' => $_POST['password'], 'modified_on' => $now), 'where' => Array('id' => $user_id)));
	    header ("Location: /$referer/changed");
        }
        else {
            // User not found
            header ("Location: /$referer/EUSERNOTFO/" . $_POST['email']);
        }
    }

    private function handleVerification() {
        $email = array_shift($this->args);
        $token = array_shift($this->args);
        $stmt = DbHandler::select(Array('table' => 'users',
                                        'columns' => Array('id', 'is_active'),
                                        'where' => Array('login_id' => $email)));
        $row = DbHandler::getRow($stmt);
        if(null == $row) {
            // Send 'no such user'
            header ("Location: /forgot-password/EUSERNOTFO/$email");
            return;
        }
        $user_id = $row['id'];
        $stmt = DbHandler::select(Array('table' => 'verifications',
                                        'columns' => Array('timestamp'),
                                        'where' => Array('user_id' => $user_id, 'verify_token' => $token)));
        $row = DbHandler::getRow($stmt);
        if(null == $row) {
            // Send 'invalid verification'
            header ("Location: /forgot-password/EVERIFINVAL/$email");
            return;
        }
        $time = $row['timestamp'];
        $gen_token = base64_encode($time . $email);
        if($token != $gen_token) {
            // Send 'invalid token'
            header ("Location: /forgot-password/EVERIFCRED/$email");
            return;
        }
        DbHandler::update(Array('table' => 'users',
                                'update' => Array('is_active' => true),
                                'where' => Array('id' => $user_id)));
        DbHandler::deleteQuery(Array('table' => 'verifications',
                                     'where' => Array('user_id' => $user_id, 'verify_token' => $token)));
        header ("Location: /forgot-password/verified/$email");
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'POST', 'GET' );

    protected $allowNoJson = true;
}

?>
