<?php
class AdminJobs extends AdminBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if('GET' == $this->method) {
            if(!isset($this->subject) or sizeof($this->subject) == 0) {
                $this->printJobs();
            } else {
                $this->printSelected(Array("'$this->subject'"));
            }
        } else {
            $this->searchJobs();
        }
        $this->_endHandle();
    }

    private function searchJobs() {
        $search = $_POST['url'];
        $stmt = DbHandler::selectUsingQuery("SELECT job_id FROM jobs WHERE url rlike '$search';");
        $jobs = Array();
        while(null != ($row = DbHandler::getRow($stmt))) {
            $jobs[] = "'" . $row['job_id'] . "'";
        }
        if(sizeof($jobs) > 0) {
            $this->printSelected($jobs);
        } else {
            $this->printNotFound();
        }
    }

    private function printNotFound() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            . AdminPanelHelper::navBar(Array('selected' => 'jobs'))
            . $this->notFound()
            . AdminPanelHelper::htmlEnd());
    }

    private function notFound() {
        $str = <<<EOH
   <div class="container centered-content row">
     <h3>No matching jobs were found</h3>
   </div>
   <form action="/v1/admin/jobs" method="POST">
   <div class="container">
     <div class="row centered-content">
       <h3>Enter a domain or url to search for</h3>
       <input type="text" name="url" class="url"/>
     </div>
     <div class="row centered-content">
       <input type="submit" value="Submit"/>
     </div>
   </div>
   </form>
EOH;
        return($str);
    }

    private function printJobs() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            . AdminPanelHelper::navBar(Array('selected' => 'jobs'))
            . $this->jobs()
            . AdminPanelHelper::htmlEnd());
    }

    private function printSelected($jobs) {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            . AdminPanelHelper::navBar(Array('selected' => 'jobs'))
            . $this->jobs($jobs)
            . AdminPanelHelper::htmlEnd());
    }

    private function jobs($jobs = null) {
        $stmt = null;
        if(null != $jobs) {
            $jobs_str = join(', ', $jobs);
            $stmt = DbHandler::selectUsingQuery("SELECT user_id, job_id, url, created_on, completed_on, is_completed FROM jobs WHERE job_id IN ( $jobs_str );");
        } else {
            $stmt =  DbHandler::select(Array('table' => 'jobs', 'columns' => Array('user_id', 'job_id', 'url', 'created_on', 'completed_on', 'is_completed', 'ip_address'), 'order' => Array('created_on DESC', 'completed_on DESC')));//, 'limit' => Array('offset' => 0, 'number' => 10)));
        }
        $str = <<<EOH
   <form action="/v1/admin/jobs" method="POST">
   <div class="container">
     <div class="row centered-content">
       <h3>Enter a domain or url to search for</h3>
       <input type="text" name="url" class="url"/>
     </div>
     <div class="row centered-content">
       <input type="submit" value="Submit"/>
     </div>
   </div>
   </form>
   <div class="container centered-content row">
    <div class="export-exceldata">
        <input onclick="location.href = '/v1/admin/scanreport';" type="button" name="exportdata" value="Generate Report as PDF" class="small green">
        <input onclick="location.href = '/v1/admin/scanreportxls';" type="button" name="exportdata" value="Generate Report as XLS" class="small green">
    </div>
     <table class="results">
       <tr><th>Job#</th><th>Base Url</th><th>Requester</th><th>Started On</th><th>Completed On</th><th>Vulnerabilties</th><th>IP Address</th><th>View</th></tr>
EOH;
        while($row = DbHandler::getRow($stmt)) {
            $user_id = $row['user_id'];
            $job_id = $row['job_id'];
            $url = $row['url'];
            $created_on = $row['created_on'];
            $completed_on = $row['completed_on'];
            $is_completed = $row['is_completed'];
            $ip_address = $row['ip_address'];
            if(0 != $user_id) {
                $stmt2 = DbHandler::select(Array('table' => 'users', 'columns' => Array('login_id', 'first_name', 'last_name', 'company', 'position', 'login_id'), 'where' => Array('id' => $user_id)));
                $row2 = DbHandler::getRow($stmt2);
                if('anon' != $row2['login_id']) {
                    $name = $row2['first_name'] . " " . $row2['last_name'];
                    $email = $row2['login_id'];
                    $work = $row2['position'] . ", " . $row2['company'];
                    $contact_str = "<p style='margin:0;'>$name</p><p style='margin:0;'>$email</p><p style='margin:0;'>$work</p>";
                } else {
                    $contact_str = 'Anonymous User';
                }
            } else {
                $contact_str = 'Admin';
            }
            $view_str = '';
            $vulnerabilities = '';
	    if($is_completed) {
                $view_str = '<input type=button class="small green" value=View onclick="window.location=\'/v1/admin/jobdetails/' . $job_id . '\';"/>';
                $stmt2 = DbHandler::selectUsingQuery("SELECT modules.name AS name FROM modules INNER JOIN scan_vulnerabilities ON modules.id = scan_vulnerabilities.module_id WHERE scan_vulnerabilities.job_id = '$job_id';");
                while(null != ($row2 = DbHandler::getRow($stmt2))) {
                    $vulnerabilities .= $row2['name'] . ", ";
                }
                $vulnerabilities = preg_replace("/, $/", "", $vulnerabilities);
            }
            $str .= <<<EOH
       <tr>
         <td>$job_id</td>
         <td>$url</td>
         <td>$contact_str</td>
         <td>$created_on</td>
         <td>$completed_on</td>
         <td>$vulnerabilities</td>
         <td>$ip_address</td>
         <td>$view_str</td>
       </tr>
EOH;
        }

        $str .= <<<EOH
     </table>
  </div>
EOH;

    if (strpos($_SERVER['REQUEST_URI'],'jobs?report') !== false) {
$dompdf = new DOMPDF();
$dompdf->load_html($str);
$dompdf->render();

$dompdf->stream("Report.pdf");
    }

        return($str);
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET', 'POST' );
}

?>
