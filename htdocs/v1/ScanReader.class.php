<?php

class ScanReader {

    private $url;
    private $job_id;

    public function __construct($job_id) {
        $this->job_id = $job_id;
    }

    public function printResults($complete_details = false) {
        if(!$complete_details) $column = 'summary_html';
        else $column = 'html';

        $stmt = DbHandler::select(
            Array('table' => 'scan_results',
                  'columns' => Array($column),
                  'where' => Array('job_id' => $this->job_id)));

        if(null != ($row = DbHandler::getRow($stmt))) {
            echo gzuncompress($row[$column]);
        }
    }
}

?>
