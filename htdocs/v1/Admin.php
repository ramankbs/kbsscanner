<?php

spl_autoload_register(function ($class) {
    include "$class.class.php";
});

try {
    // check for cookie and redirect to login in case the cookie is not set
    if(null == CookieManager::get(AppGlobals::$ADMIN_COOKIE_NAME)
        && !preg_match("/admin\/login/", $_REQUEST['request']))
    {
        header(302);
        header ("Location: /v1/admin/login");
        return;
    }

    if(!preg_match("/admin\/log/", $_REQUEST['request'])) {
        $site = AdminUtils::getSiteName();
        $ip = AdminUtils::getIpAddr();
        list($admin_key, $enc_admin_key) = AdminUtils::getKey($ip, $site);
        CookieManager::setDomainCookie(AppGlobals::$ADMIN_COOKIE_NAME, $enc_admin_key, 10*60, $site);
    }

    if(preg_match('/^admin$/', $_REQUEST['request']) or preg_match('/^admin\/$/', $_REQUEST['request'])) {
        header(302);
        header ("Location: /v1/admin/login");
        return;
    }

    header("Content-Type: text/html; charset=iso-8859-1");

    $handler = RestHandlerFactory::getHandler($_REQUEST['request']);
    if(null != $handler) {
        $handler->process();
    } else {
        ResponseHandler::$OUTPUT = 'html';
        ResponseHandler::errorResponse('ENOTFO', 404, true, 'endpoint', $_REQUEST['request']);
    }
}
catch(Exception $e) {
    echo $e->getMessage();
}

?>
