<?php

class AdminLive extends AdminBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if('GET' == $this->method) {
            $this->handlePrintForm();
        } else if('POST' == $this->method) {
            $this->handleDemo();
        }
        $this->_endHandle();
    }

    private function handlePrintForm() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            . AdminPanelHelper::navBar(Array('selected' => 'live'))
            . $this->printForm()
            . AdminPanelHelper::htmlEnd());
    }

    private function handleDemo() {
        $url = $_POST['url'];
        header ("Location: /v1/admin/liveresults/url/$url");
    }

    private function printForm() {
        $str = <<<EOH
   <form action="/v1/admin/live" method="POST">
   <div class="container">
     <div class="row centered-content">
       <h3>Enter URL to scan</h3>
       <input type="text" name="url" class="url"/>
     </div>
     <div class="row centered-content">
       <input type="submit" value="Submit"/>
     </div>
   </div>
   </form>
EOH;
        return($str);
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET', 'POST' );
}

?>
