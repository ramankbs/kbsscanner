<?php

abstract class AdminBase extends RestHandler
{

    /*
     * PUBLIC METHODS
     */

    public function __construct($_model, $_object, $_args) {
        parent::__construct($_model, $_object, $_args);
        $this->allowNoJson = true;
    }

    protected function handleGetRedirect($object) {
        $filter = '';
        header("Location: /v1/admin/$object/0-7$filter");
    }
}

?>
