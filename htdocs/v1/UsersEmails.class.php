<?php

class UsersEmails extends SessionPagesBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if('GET' == $this->method and sizeof($this->args) and 'verify' == $this->args[0]) {
            $action = array_shift($this->args);
            if(preg_match("/^E/", $this->args[0])) {
                if(false == ($user_id = $this->_checkSession())) {
                    $this->printUnsignedError();
                } else {
                    $this->printSignedError($user_id);
                }
            } else if(preg_match("/success/", $this->args[0])) {
                if(false == ($user_id = $this->_checkSession())) {
                    $this->printUnsignedAck("Your additional email has been verified");
                } else {
                    $this->printSignedAck($user_id, "Your additional email has been verified");
                }
            } else {
                $this->handleVerify();
            }
        }
        else if(false == ($user_id = $this->_checkSession())) {
            return;
        }
        else if(!isset($this->subject) or null == $this->subject or '' == $this->subject) {
            $this->printError("Please specify a user id to view and add emails");
        }
        else if(':' . $user_id != $this->subject) {
            $this->printError("You are not authorized to view emails of user" . $this->subject);
        }
        else {
            if('GET' == $this->method) {
                if(null != $this->args and sizeof($this->args)) {
                    if(preg_match("/^E/", $this->args[0])) {
                        $this->printSignedError($user_id);
                    } else {
                        $type = array_shift($this->args);
                        if('sent' == $type) {
                            $this->printSignedAck($user_id, "Please check your email for further instructions to verify your email.");
                        }
                    }
                } else {
                    $this->printEmails($user_id);
                }
            } else {
                $action = array_shift($this->args);
                if('add' == $action) {
                    $this->handleAdd($user_id);
                }
                else if('verify' == $action) {
                    $this->sendVerification($user_id, $this->args[0]);
                }
            }
        }
        $this->_endHandle();
    }

    private function handleVerify() {
        $primary = array_shift($this->args);
        $email = array_shift($this->args);
        $token = array_shift($this->args);
        $stmt = DbHandler::select(Array('table' => 'users',
                                        'columns' => Array('id'),
                                        'where' => Array('login_id' => $primary)));
        $row = DbHandler::getRow($stmt);
        if(null == $row) {
            // Send 'no such user'
            header ("Location: /v1/users/emails/:0/verify/EUSERNOTFO/$primary");
            return;
        }
        $user_id = $row['id'];
        $stmt = DbHandler::select(Array('table' => 'user_emails', 'columns' => Array('is_verified'), 'where' => Array('email' => $email, 'user_id' => $user_id)));
        $row = DbHandler::getRow($stmt);
        $is_verified = $row['is_verified'];
        if(true == $is_verified) {
            // Send 'already verified'
            header ("Location: /v1/users/emails/:$user_id/verify/EVERIFALRDY/$email");
            return;
        }
        $stmt = DbHandler::select(Array('table' => 'verifications',
                                        'columns' => Array('timestamp'),
                                        'where' => Array('user_id' => $user_id, 'verify_token' => $token)));
        $row = DbHandler::getRow($stmt);
        if(null == $row) {
            // Send 'invalid verification'
            header ("Location: /v1/users/emails/:$user_id/verify/EVERIFINVAL/$email");
            return;
        }
        $time = $row['timestamp'];
        $gen_token = base64_encode($time . $email);
        if($token != $gen_token) {
            // Send 'invalid token'
            header ("Location: /v1/users/emails/:$user_id/verify/EVERIFCRED/$email");
            return;
        }
        DbHandler::update(Array('table' => 'user_emails',
                                'update' => Array('is_verified' => true),
                                'where' => Array('user_id' => $user_id, 'email' => $email)));
        $domain = preg_replace("/^.*@/", "", $email);
        DbHandler::insert(Array('table' => 'allowed_domains', 'columns' => Array('user_id' => $user_id, 'domain' => $domain, 'is_verified' => true)));
        DbHandler::deleteQuery(Array('table' => 'verifications',
                                     'where' => Array('user_id' => $user_id, 'verify_token' => $token)));
        header ("Location: /v1/users/emails/:$user_id/verify/success");
    }

    private function handleAdd($user_id) {
        $email = $_POST['email'];
        $stmt = DbHandler::select(Array('table' => 'user_emails', 'columns' => Array('email'), 'where' => Array('email' => $email, 'user_id' => $user_id)));
        if(null != ($row = DbHandler::getRow($stmt))) {
            header("Location: /v1/users/emails/:$user_id/EEMAILEXIST/$email");
            return;
        }
        DbHandler::insert(Array('table' => 'user_emails', 'columns' => Array('email' => $email, 'user_id' => $user_id)));
        $this->sendVerification($user_id, $email);
    }

    private function printError($message) {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->error($message)
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function sendVerification($user_id, $email) {
        $stmt = DbHandler::select(Array('table' => 'users', 'columns' => Array('login_id'), 'where' => Array('id' => $user_id)));
        $row = DbHandler::getRow($stmt);
        $primary = $row['login_id'];
        $time = time();
        $token = base64_encode($time . $email);
        DbHandler::insert(Array('table' => 'verifications',
                                'columns' => Array('user_id' => $user_id,
                                                   'timestamp' => $time,
                                                   'verify_token' => $token)));
        $htmlMessage = <<<EOH
<p>Hi,</p>
<p>Please click on the following link to verify your additional email with us.</p>
<p><a href="http://scan.zapts.com/v1/users/emails/:$user_id/verify/$primary/$email/$token"/>Verify Your Email</a></p>
<p>If the above link does not work, please copy and paste the following into your browser and enter</p>
<p>http://scan.zapts.com/v1/users/emails/:$user_id/verify/$primary/$email/$token</p>
<p>Thanks,</p>
<p>Security Scanner Team</p>
EOH;
        $textMessage = <<<EOH
Hi,

Please copy and paste the following link into your browser and enter to verify your additional email

http://scan.zapts.com/v1/users/emails/:$user_id/verify/$primary/$email/$token

Thanks,
Security Scanner Team
EOH;
        AwsSesMail::sendMail(Array($email), "Please verify your additional email at scan.zapts.com", $textMessage, $htmlMessage);
	header ("Location: /v1/users/emails/:$user_id/sent");
    }

    private function error($message) {
        $str = <<<EOH
   <div class="notice error">$message</div>
EOH;
        return $str;
    }

    private function printUnsignedError() {
        $errCode = array_shift($this->args);
        $errArg = array_shift($this->args);
        ResponseHandler::$OUTPUT = 'html';
        $message = ResponseHandler::errorResponse($errCode, 200, false, $errArg);
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->error($message)
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function printUnsignedAck($message) {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->ack($message)
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function printSignedError($user_id) {
        $errCode = array_shift($this->args);
        $errArg = array_shift($this->args);
        ResponseHandler::$OUTPUT = 'html';
        $message = ResponseHandler::errorResponse($errCode, 200, false, $errArg);
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->error($message)
            . $this->emails($user_id)
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function printSignedAck($user_id, $message) {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->ack($message)
            . $this->emails($user_id)
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function ack($message) {
        $str = <<<EOH
   <div class="notice info">$message</div>
EOH;
        return $str;
    }

    private function printEmails($user_id) {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->emails($user_id)
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function emails($user_id) {
        $stmt =  DbHandler::select(Array('table' => 'user_emails', 'columns' => Array('email', 'is_verified', 'is_login_email'), 'where' => Array('user_id' => $user_id), 'order' => Array('is_verified ASC', 'is_login_email ASC')));
        $str = <<<EOH
   <div class="container centered-content scan-results body-margin-top">
     <div>
       <h3>Add Email</h3>
       <form action="/v1/users/emails/:$user_id/add/" method="POST">
         <div class="form-row">
           <input type="text" class="url user-domain-textfield" inittext="Your email to add" name="email"/>
           <input type="submit" name="submit" value="Add Email">           
         </div>
       </form>
     </div>
   </div>
   <div class="container centered-content scan-results">
     <div>
       <h3>Your Emails</h3>
       <table class="results users-emails">
         <tr><th>Email</th><th>Verified?</th><th>Action</th></tr>
EOH;
        while($row = DbHandler::getRow($stmt)) {
            $email = $row['email'];
            $is_verified = $row['is_verified'];
            $is_login_email = $row['is_login_email'];
            $action_str = '';
            if(!$is_verified) {
                $action_str = '<form method="POST" action="/v1/users/emails/:' . $user_id . '/verify/' . $email . '"><input type=submit class="type-1" value="Resend Verification"></form>';
            }
            $is_verified = ($is_verified ? 'Yes' : 'No');
            $style = '';
            if($is_login_email) $style = ' style="color: #ff8822"';
            $str .= <<<EOH
         <tr>
           <td$style>$email</td>
           <td$style>$is_verified</td>
           <td$style>$action_str</td>
         </tr>
EOH;
        }

        $str .= <<<EOH
      </table>
    </div>
  </div>
  <div class="body-margin-bottom"></div>
EOH;
        return($str);
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET', 'POST' );
}

?>
