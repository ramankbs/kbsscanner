<?php

class AdminPanelHelper
{
    /* Private constructor */
    private function __construct() {
    }

    static function htmlBegin() {
        $str = <<<EOH
<!DOCTYPE html>
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <title>Security Scanner Admin Panel</title>
   <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
   <link rel="stylesheet" type="text/css" href="/css/normalize.css">
   <link rel="stylesheet" type="text/css" href="/css/admin.css">
</head>
<body>
EOH;
        return($str);
    }

    static function htmlEnd() {
        $str = <<<EOH
<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/admin.js"></script>
<script type="text/javascript" src="/js/finre.js"></script>
</body>
</html>
EOH;
        return($str);
    }

    static function headerNotLoggedIn() {
        $str = <<<EOH
   <div class="container header centered-content">
<div class="main-container">
     	 <div class="logo2"><a href="http://scan.zapts.com/"><img align=left src="/img/logo.png"></a></div>
   </div>
</div>
   <div class="container header-back"></div>
EOH;
        return($str);
    }

    static function headerLoggedIn() {
        $str = <<<EOH
   <div class="container centered-content header">
<div class="main-container">
      
<div class="logo2"><a href="http://scan.zapts.com/v1/admin/live"><img align=left src="/img/logo.png"></a></div>
      <div class="controls">
         <div>
            <form name="logout" action="/v1/admin/logout" method="POST">
               <input type="submit" class="small" value="Logout"/>
            </form>
         </div>
      </div>
   </div>
</div>
   <div class="container header-back"></div>
EOH;
        return($str);
    }

    static function navBar($spec) {
        if(null != CookieManager::get(AppGlobals::$SUPERADMIN_COOKIE_NAME)) $ring = 1;
        else $ring = 0;
        $str = <<<EOH
   <div class="container centered-content row nav">
EOH;

        foreach(self::$NAV as $key => $details) {
            if($details['ring'] > $ring) continue;
            if($key != $spec['selected']) {
                $name = $details['name'];
                $link = $details['link'];
                $str .= <<<EOH
            <div><a href="$link">$name</a></div>
EOH;
            } else {
                $name = $details['name'];
                $str .= <<<EOH
            <div class="selected">$name</div>
EOH;
            }
        }

        $str .= <<<EOH
   </div>
EOH;
        return($str);
    }

    static function pagination($countQuery, $object, $offset, $limit) {
        $str = <<<EOH
   <div class="container centered-content absolute-children">
      <div class="pagination">
EOH;

        $stmt = DbHandler::selectUsingQuery($countQuery);
        $row = DbHandler::getRow($stmt);
        $count = $row['count'];
        if($offset != 0) {
            $o1 = $offset - $limit;
            $l1 = $o1 + $limit - 1;
            $str .= <<<EOH
            <div style="position:absolute;left:40px;"><a href="/v1/admin/$object/$o1-$l1"><h3 class="page">&#12296;</h3></a></div>
EOH;
        }
        if($count > ($offset + $limit)) {
            $o2 = $offset + $limit;
            $l2 = $o2 + $limit - 1;
            $str .= <<<EOH
            <div style="position:absolute;right:40px;"><a href="/v1/admin/$object/$o2-$l2"><h3 class="page">&#12297;</h3></a></div>
EOH;
        }

        $str .= <<<EOH
      </div>
   </div>
   <div class="container"><div class="row">&nbsp;</div></div>
EOH;
        return $str;
    }

    static function dateFilter($object, $from, $to) {
        if(null == $from) {
            $to_year = date("Y"); $to_month = date("m"); $to_date = date("d");
            $to_month_str = self::monthOptions($to_month);
            $from_year = $to_year - 1; $from_month = $to_month; $from_date = "01";
            $from_month_str = self::monthOptions($from_month);
        } else {
            $from_parts = explode("-", $from);
            $to_parts = explode("-", $to);
            $to_year = $to_parts[0]; $to_month_str = self::monthOptions($to_parts[1]); $to_date = $to_parts[2];
            $from_year = $from_parts[0]; $from_month_str = self::monthOptions($from_parts[1]); $from_date = $from_parts[2];
        }
        $str = <<<EOH
      <form method="GET" name="date-filter" action="/v1/admin/$object/">
   <div class="container centered-content row">
      <div class="date-filter">
        <div><input type="text" class="small" name="from-year" value="$from_year">
             <select class="month" name="from-month">$from_month_str</select>
             <input type="text" class="tiny" name="from-date" value="$from_date"></div>
        <div>-</div>
        <div><input type="text" class="small" name="to-year" value="$to_year">
             <select class="month" name="to-month">$to_month_str</select>
             <input type="text" class="tiny" name="to-date" value="$to_date"></div>
        <div><input type="submit" class="small" value="filter"></div>
      </div>
   </div>
      </form>
EOH;
        return($str);
    }

    static function monthOptions($selected) {
        $spec = Array('01' => 'Jan', '02' => 'Feb', '03' => 'Mar',
                      '04' => 'Apr', '05' => 'May', '06' => 'Jun',
                      '07' => 'Jul', '08' => 'Aug', '09' => 'Sep',
                      '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');
        $str = '';
        foreach($spec as $k => $v) {
            if($selected != $k) {
                $str .= <<<EOH
                <option value="$k">$v</option>
EOH;
            } else {
                $str .= <<<EOH
                <option value="$k" selected="selected">$v</option>
EOH;
            }
        }
        return($str);
    }


    static $NAV = Array ( 'live' => Array( 'name' => 'Live Scan', 'ring' => 0, 'link' => '/v1/admin/live' ),
                          'scan' => Array( 'name' => 'Background Scan', 'ring' => 0, 'link' => '/v1/admin/scan' ),
                          'jobs' => Array( 'name' => 'Scans', 'ring' => 0,  'link' => '/v1/admin/jobs' ),
                          'domains' => Array( 'name' => 'User Domains', 'ring' => 0, 'link' => '/v1/admin/domains' ),
                          'verify' => Array( 'name' => 'Verify User', 'ring' => 0, 'link' => '/v1/admin/verify' ),
			  'adduser' => Array( 'name' => 'Add User', 'ring' => 0, 'link' => '/v1/admin/adduser' ),
                          'modules' => Array( 'name' => 'Modules', 'ring' => 0, 'link' => '/v1/admin/modules' ) );
}

?>
