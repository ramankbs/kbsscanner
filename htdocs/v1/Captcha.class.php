<?php

require 'vendor/autoload.php';

use Gregwar\Captcha\CaptchaBuilder;

class Captcha
{
     private function __construct() {
     }

     static function inline() {
         $builder = new CaptchaBuilder;
         $builder->build();
         $_SESSION['phrase'] = $builder->getPhrase();
         return($builder->inline());
     }
}

?>
