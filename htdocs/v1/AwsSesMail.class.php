<?php

require 'vendor/autoload.php';

use Aws\Common\Aws;

class AwsSesMail
{
    private function __construct() {
    }

    static function sendMail($toArr, $subject, $text, $html) {
        $aws = Aws::factory('AwsConfig.php');
        $client = $aws->get('Ses');
        $result = $client->sendEmail(array(
            'Source' => 'scanner@scanmail.zapts.com',
            'Destination' => array('ToAddresses' => $toArr, 'CcAddresses' => array(), 'BccAddresses' => array()),
            'Message' => array(
                'Subject' => array('Data' => $subject, 'Charset' => 'UTF-8'),
                'Body' => array('Text' => array('Data' => $text, 'Charset' => 'UTF-8'),
                                'Html' => array('Data' => $html, 'Charset' => 'UTF-8')
                )
            ),
            'ReplyToAddresses' => array('scanner@scanmail.zapts.com'),
            'ReturnPath' => 'scanner@scanmail.zapts.com')
        );
        // print_r($result);
    }
}

?>
