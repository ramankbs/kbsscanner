<?php

class AdminDomains extends AdminBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if('GET' == $this->method) {
            if(!isset($this->subject) or null == $this->subject or '' == $this->subject) {
                $this->handleUsersForm();
            } else {
                $this->handleDomainsForm();
            }
        } else if('POST' == $this->method) {
            $this->handleAddDomain();
        }
        $this->_endHandle();
    }

    private function handleUsersForm() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            . AdminPanelHelper::navBar(Array('selected' => 'domains'))
            . $this->usersForm()
            . AdminPanelHelper::htmlEnd());
    }

    private function handleDomainsForm() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            . AdminPanelHelper::navBar(Array('selected' => 'domains'))
            . $this->domainsForm()
            . AdminPanelHelper::htmlEnd());
    }

    private function usersForm() {
        $stmt = DbHandler::select(Array('table' => 'users', 'columns' => Array('id', 'first_name', 'last_name', 'login_id', 'company', 'position'), 'where' => Array('is_active' => true)));
        $str = <<<EOH
       <div class="export-exceldata">
        <input onclick="location.href = '/v1/admin/domainsreportxls';" type="button" name="exportdata" value="Generate Report as XLS" class="small green">
    </div>
   <div class="container centered-content row">
     <table class="results">
       <tr><th>Email</th><th>Name</th><th>Company</th><th>Position</th><th>Select</th></tr>
EOH;

        while(null != ($row = DbHandler::getRow($stmt))) {
            $user_id = $row['id'];
            $email = $row['login_id'];
            $name = $row['first_name'] . " " . $row['last_name'];
            $company = $row['company'];
            $position = $row['position'];
            $str .= <<<EOH
       <tr><td>$email</td><td>$name</td><td>$company</td><td>$position</td><td><input type=button class="small green" value=Select onclick="window.location='/v1/admin/domains/$user_id';"/></td></tr>
EOH;
        }

        $str .= <<<EOH
     </table>
   </div>
EOH;
        return($str);
    }

    private function domainsForm() {
        $user_id = $this->subject;
        $stmt = DbHandler::select(Array('table' => 'allowed_domains', 'columns' => Array('domain', 'is_verified'), 'where' => Array('user_id' => $user_id), 'order' => Array('is_verified ASC')));
        $str = <<<EOH
   <form action="/v1/admin/domains/$user_id" method="POST">
   <div class="container">
     <div class="row centered-content">
       <h3>Enter domain to allow for user# $user_id</h3>
       <input type="text" name="domain" class="url"/>
     </div>
     <div class="row centered-content">
       <input type="submit" value="Submit"/>
     </div>
   </div>
   </form>
          <div class="export-exceldata">
        <input onclick="location.href = '/v1/admin/domainsallowedreportxls/$user_id';" type="button" name="exportdata" value="Generate Report as XLS" class="small green">
    </div>
   <div class="container centered-content row">
     <table class="results small">
       <tr><th>Domains Added/Allowed Already</th><th>Action</th></tr>
EOH;

        while(null != ($row = DbHandler::getRow($stmt))) {
            $domain = $row['domain'];
            $is_verified = $row['is_verified'];
            $action_str = '';
            if(!$is_verified) {
                $action_str = '<form action="/v1/admin/domains/' . $user_id . '" method="POST"><input type=hidden name=domain value="' . $domain . '"/><input type=submit class="type-1" value="Allow" style="margin-left:40px;"/></form>';
            }
            $str .= <<<EOH
       <tr><td>$domain</td><td>$action_str</td></tr>
EOH;
        }

        $str .= <<<EOH
     </table>
   </div>
EOH;
        return($str);
    }

    private function handleAddDomain() {
        $user_id = $this->subject;
        $domain = $_POST['domain'];
        $stmt = DbHandler::select(Array('table' => 'allowed_domains', 'columns' => Array('user_id'), 'where' => Array('user_id' => $user_id, 'domain' => $domain)));
        if(null == ($row = DbHandler::getRow($stmt))) {
            DbHandler::insert(Array('table' => 'allowed_domains', 'columns' => Array('is_verified' => true, 'user_id' => $user_id, 'domain' => $domain)));
        } else {
            DbHandler::update(Array('table' => 'allowed_domains', 'update' => Array('is_verified' => true), 'where' => Array('user_id' => $user_id, 'domain' => $domain)));
        }
        header("Location: /v1/admin/domains/$user_id");
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET', 'POST' );
}

?>
