<?php

class AdminLogin extends AdminBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        if('POST' == $this->method) {
            $this->handlePost();
        } else if('GET' == $this->method) {
            $this->handleGet();
        }
    }

    private function handlePost() {
        if($_POST['user'] == $this->SS_ADMIN and
           $_POST['password'] == $this->SS_ADMINPWD)
        {
            $this->doAdminLogin();
        } else {
            // read admins config
            if(false == ($handle = fopen("conf/admins.conf", "r"))) {
                header ("Location: /v1/admin/login");
                return;
            }
            $found = false;
            while(!$found and !feof($handle)) {
                $entry = chop(fgets($handle));
                $parts = explode(" ", $entry);
                $user = $parts[0];
                $passwd = $parts[1];
                if($_POST['user'] == $user and $_POST['password'] == $passwd) {
                    $found = true;
                    $this->doAdminLogin(false);
                }
            }
            fclose($handle);
            if(!$found) {
                header ("Location: /v1/admin/login");
            }
        }
    }

    private function doAdminLogin($is_super = true) {
        $site = AdminUtils::getSiteName();
        $ip = AdminUtils::getIpAddr();
        list($admin_key, $enc_admin_key) = AdminUtils::getKey($ip, $site);
        CookieManager::setDomainCookie(AppGlobals::$ADMIN_COOKIE_NAME, $enc_admin_key, 10*60, $site);
        if($is_super) {
            list($admin_key, $enc_admin_key) = AdminUtils::getKey($ip, $site);
            CookieManager::setDomainCookie(AppGlobals::$SUPERADMIN_COOKIE_NAME, $enc_admin_key, 10*60, $site);
        }
        header ("Location: /v1/admin/live");
    }

    private function handleGet() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerNotLoggedIn()
            . $this->loginForm()
            . AdminPanelHelper::htmlEnd());
    }

    private function loginForm() {
        $str = <<<EOH
   <div class="container centered-content row">
   </div>
   <div class="container centered-content row">
      <h2>Admin Login</h2>
   </div>
   <form name="login" action="/v1/admin/login" method="POST">
   <div class="container centered-content row">
      <table class="noborder login-form">
         <tr>
            <td width="25%">User Name</td>
            <td><input name="user" type="text"/></td>
         </tr>
         <tr>
            <td width="25%">Password</td>
            <td><input name="password" type="password"/></td>
         </tr>
      </table>
   </div>
   <div class="container centered-content row">
      <input type="submit" value="login"/>
   </div>
   </form>
EOH;
        return($str);
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'POST', 'GET' );

    protected $SS_ADMIN = 'admin';
    protected $SS_ADMINPWD = '$_ss@001#pd_';
}

?>
