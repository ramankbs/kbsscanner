<?php

header("Content-Disposition: attachment; filename=\"report.xls\"");
header("Content-Type: application/vnd.ms-excel;");
header("Pragma: no-cache");
header("Expires: 0");

class AdminDomainsreportxls extends AdminBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if('GET' == $this->method) {
            if(!isset($this->subject) or null == $this->subject or '' == $this->subject) {
                $this->handleUsersForm();
            } else {
                $this->handleDomainsForm();
            }
        } else if('POST' == $this->method) {
            $this->handleAddDomain();
        }
        $this->_endHandle();
    }

    private function handleUsersForm() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            //. AdminPanelHelper::navBar(Array('selected' => 'domains'))
            . $this->usersForm()
            . AdminPanelHelper::htmlEnd());
    }

    private function handleDomainsForm() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            //. AdminPanelHelper::navBar(Array('selected' => 'domains'))
            . $this->domainsForm()
            . AdminPanelHelper::htmlEnd());
    }

    private function usersForm() {
        $stmt = DbHandler::select(Array('table' => 'users', 'columns' => Array('id', 'first_name', 'last_name', 'login_id', 'company', 'position'), 'where' => Array('is_active' => true)));
        $str = <<<EOH
       <div class="export-exceldata">
        <input onclick="location.href = '';" type="button" name="exportdata" value="Generate Report as XLS" class="small green">
    </div>
   <div class="container centered-content row">
     <table class="results">
       <tr><th>Email</th><th>Name</th><th>Company</th><th>Position</th></tr>
EOH;

        while(null != ($row = DbHandler::getRow($stmt))) {
            $user_id = $row['id'];
            $email = $row['login_id'];
            $name = $row['first_name'] . " " . $row['last_name'];
            $company = $row['company'];
            $position = $row['position'];
            $str .= <<<EOH
       <tr><td>$email</td><td>$name</td><td>$company</td><td>$position</td></tr>
EOH;
        }

        $str .= <<<EOH
     </table>
   </div>
EOH;


$file="report.xls";
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=$file");
echo $str;

    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET', 'POST' );
}

?>
