<?php

class CookieManager
{
    /* private constructor */
    private function __construct() {
    }

    /* Get a cookie's value - null in case not set */
    static function get($name) {
        if(isset($_COOKIE[$name])) {
            return($_COOKIE[$name]);
        }
        return(null);
    }

    /* Set a cookie on root of a domain */
    static function setDomainCookie($name, $value, $seconds, $site) {
        setCookie($name, $value, time() + $seconds, "/", "$site.com", false, true);
    }
}

?>
