<?php

require_once "AppGlobals.class.php";

return array(
    // Bootstrap the configuration file with AWS specific features
    'includes' => array('_aws'),
    'services' => array(
        // All AWS clients extend from 'default_settings'. Here we are
        // overriding 'default_settings' with our default credentials and
        // providing a default region setting.
        'default_settings' => array(
            'params' => array(
                'key'    => AppGlobals::$AWS_ACCESS_KEY,
                'secret' => AppGlobals::$AWS_ACCESS_SECRET,
                'region' => 'us-west-2'
            )
        )
    )
);

?>
