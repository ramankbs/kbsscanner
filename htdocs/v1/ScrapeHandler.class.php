<?php

require_once "uws/http.php";
require_once "uws/web_browser.php";
require_once "uws/simple_html_dom.php";


class ScrapeHandler {

    private $proto;
    private $domain;
    private $conditions;
    private $webBrowser;
    private $dom;
    private $domain_parts;
    private $url;
    private $urls;
    private $job_id;
    private $usedUrls;
    private $html;
    private $summary_html;

    public function __construct($url, $job_id = null) {

        if(preg_match("/^http:/", $url)) {
            $this->proto = "http";
            $this->domain = preg_replace("/^http:\/\//", "", $url);
        } else if(preg_match("/^https:/", $url)) {
            $this->proto = "https";
            $this->domain = preg_replace("/^https:\/\//", "", $url);
        }
        $this->domain_parts = explode("/", $this->domain);
        $this->domain = array_shift($this->domain_parts);

        $this->conditions = Array( '4503_port_responds' => true );
        $this->url = $url;
        $this->job_id = $job_id;

        $this->html = '';
        $this->summary_html = '';

        $this->webBrowser = new WebBrowser();

        $this->findBaseUrls();
    }

    public function findBaseUrls() {

        $this->urls = Array();

        $httpRes = $this->webBrowser->Process($this->url);

        if($httpRes['success'] and '200' == $httpRes['response']['code']) {
            if($httpRes['url'] != $this->url) {
                $this->url = $httpRes['url'];
            }
            if(preg_match('/.html$/', $this->url)) {
                $this->urls[] = $this->url;
            }
            $this->dom = new simple_html_dom();
            $this->dom->load($httpRes['body']);
            $links = $this->dom->find("link[href]");
            foreach($links as $link) {
                if(preg_match('/\/etc\/designs\/.*\/.*\.css/', $link->href)) {
                    if(!preg_match('/^http/', $link->href)) {
                        $this->urls[] = $this->proto . '://' . $this->domain . $link->href;
                    } else {
                        $this->urls[] = $link->href;
                    }
                    break;
                }
            }
            $links = $this->dom->find("img[src]");
            foreach($links as $link) {
                if(preg_match('/\/content\/dam\/.*\/.*\.(jpg|png|gif|pdf|doc|mp4|psd)/', $link->src)) {
                    if(!preg_match('/^http/', $link->src)) {
                        $this->urls[] = $this->proto . '://' . $this->domain . $link->src;
                    } else {
                        $this->urls[] = $link->src;
                    }
                    break;
                }
            }
            if(!preg_match('/.html$/', $this->url)) {
                $links = $this->dom->find("a[href]");
                $found_html_link = false;
                foreach($links as $link) {
                    $url = $link->href;
                    if(preg_match('/^\//', $url) and (preg_match('/.*\/.*\.html/', $url) or preg_match('/\/.*\.html/', $url))) {
                        $this->urls[] = $this->proto . '://' . $this->domain . $url;
                        $found_html_link = true;
                        break;
                    }
                }
                if(false == $found_html_link and preg_match('/\/$/', $this->url)) {
                    $this->urls[] = preg_replace('/\/$/', '.html', $this->url);
                }
            }
        }
        if(sizeof($this->urls) == 0) {
            $this->urls[] = $this->url;
        }
        for($i = 0; $i < sizeof($this->urls); ++$i) {
            DbHandler::insert(Array('table' => 'base_urls', 'columns' => Array('url' => $this->url, 'related_url' => $this->urls[$i])));
        }
    }

    public function scrapeAllModules() {

        $this->usedUrls = Array();

        $stmt = DbHandler::select(
            Array('table' => 'modules',
                  'columns' => Array('id', 'type', 'name', 'timeout', 'is_data_scraped', 'stop_on_positive', 'follow_redirects', 'summary', 'positive_on_match', 'negative_on_match'),
                  'order' => Array('sequence_no ASC', 'id ASC')));

        while(null != ($row = DbHandler::getRow($stmt))) {
            $module_id = $row['id'];
            $type = $row['type'];
            $name = $row['name'];
            $timeout = $row['timeout'];
            $is_data_scraped = $row['is_data_scraped'];
            $stop_on_positive = $row['stop_on_positive'];
            $follow_redirects = $row['follow_redirects'];
            $positive_on_match = $row['positive_on_match'];
            $negative_on_match = $row['negative_on_match'];
            $summary = $row['summary'];
            //if('CQ5/AEM Version' == $name)
            $this->scrapeModule($module_id, $type, $name, $timeout, $is_data_scraped, $stop_on_positive, $follow_redirects, $positive_on_match, $negative_on_match, $summary);
        }

        if(null != $this->job_id) {
            if('' == $this->summary_html) {
                $this->summary_html = <<<EOH
  <div class="container centered-content row scan-results">
    <div>No vulnerabilities found</div>
  </div>
EOH;
            }
            DbHandler::insert(Array('table' => 'scan_results', 'columns' => Array('job_id' => $this->job_id, 'html' => gzcompress($this->html), 'summary_html' => gzcompress($this->summary_html))));
        }
    }

    public function scrapeSingleModule($name) {

        $this->usedUrls = Array();

        $stmt = DbHandler::select(
            Array('table' => 'modules',
                  'columns' => Array('id', 'type', 'name', 'timeout', 'is_data_scraped', 'stop_on_positive', 'follow_redirects', 'summary', 'positive_on_match', 'negative_on_match'),
                  'where' => Array('name' => $name)));

        if(null != ($row = DbHandler::getRow($stmt))) {
            $module_id = $row['id'];
            $type = $row['type'];
            $name = $row['name'];
            $timeout = $row['timeout'];
            $is_data_scraped = $row['is_data_scraped'];
            $stop_on_positive = $row['stop_on_positive'];
            $follow_redirects = $row['follow_redirects'];
            $positive_on_match = $row['positive_on_match'];
            $negative_on_match = $row['negative_on_match'];
            $summary = $row['summary'];
            $this->scrapeModule($module_id, $type, $name, $timeout, $is_data_scraped, $stop_on_positive, $follow_redirects, $positive_on_match, $negative_on_match, $summary);
        }

        if(null != $this->job_id) {
            if('' == $this->summary_html) {
                $this->summary_html = <<<EOH
  <div class="container centered-content row scan-results">
    <div>No vulnerabilities found</div>
  </div>
EOH;
            }
            DbHandler::insert(Array('table' => 'scan_results', 'columns' => Array('job_id' => $this->job_id, 'html' => gzcompress($this->html), 'summary_html' => gzcompress($this->summary_html))));
        }
    }

    public function scrapeModule($module_id, $type, $name, $timeout, $is_data_scraped, $stop_on_positive, $follow_redirects, $positive_on_match, $negative_on_match, $summary) {
        $str = <<<EOH
  <div class="container centered-content row scan-results">
    <div>
      <div class="test-header"><span class="module-name">$name</span><span>&nbsp;&nbsp;|&nbsp;&nbsp;</span><span class="module-type">$type</span></div>
      <table class="results scrape-results">
        <tr><th>Module URL</th><th>Result</th><th>Time</th><th>Vulnerable?</th></tr>
EOH;
        $this->html .= $str;
        if(null == $this->job_id) {
            echo $str;
            @ob_flush();
            flush();
        }

        $stop_scan = false;
        $is_vulnerable = false;
        $scraped_data = Array();

        for($i = 0; $i < sizeof($this->urls); ++$i)
        {
            $url = $this->urls[$i];

            $stmt2 = DbHandler::select(
                Array('table' => 'module_urls',
                      'columns' => Array('url_format', 'cond', 'response_type'),
                      'where' => Array('module_id' => $module_id)));

            while(null != ($row2 = DbHandler::getRow($stmt2))) {
                $uf = $row2['url_format'];
                if('4503' == $uf) {
                    $test = $this->proto . "://" . $this->domain . ":4503/" . join("/", $this->domain_parts);
                } else {
                    if(preg_match("/{DOMAIN}/", $uf)) {
                        $test = preg_replace("/{DOMAIN}/", $this->domain, $uf);
                        $test = $this->proto . "://$test";
                    } else
                    if(preg_match("/{URL}/", $uf)) {
                        $url_pfx = preg_replace("/\.html$/", "", $url);
                        $test = preg_replace("/{URL}/", $url_pfx, $uf);
                    } else
                    if(preg_match("/{AUTHOR}/", $uf) or preg_match("/{AUTHORSSL}/", $uf)) {
                        $author = $this->domain;
                        if(preg_match("/^www\./", $author)) {
                            $author = preg_replace("/^www\./", "author.", $author);
                        } else if(preg_match("/^[^\.]+\./", $author)) {
                            $author = "author$author";
                        }
                        if(preg_match("/{AUTHOR}/", $uf)) {
                            $test = preg_replace("/{AUTHOR}/", $author, $uf);
                            $test = "http://$test";
                        } else
                        if(preg_match("/{AUTHORSSL}/", $uf)) {
                            $test = preg_replace("/{AUTHORSSL}/", $author, $uf);
                            $test = "https://$test";
                        }
                    } else
                    if(preg_match("/{RANDOMURL}/", $uf)) {
                        $allowed_extensions = Array('css','jpg','jpeg','png','gif','pdf','doc','mp4','psd','html');
                        $url_extension = strrchr($url, ".");
                        if(in_array(substr($url_extension, 1), $allowed_extensions)){
                            $test = str_replace($url_extension, ".random" . $url_extension, $url);
                        }
                    } else {
                        continue;
                    }
                }
                if(!isset($this->usedUrls[$test])) {
                    $this->usedUrls[$test] = true;
                    $now = date("Y-m-d H:i:s");
                    $vulnerable = 'no';
                    $cond = $row2['cond'];
                    $result = 'Unavailable';
                    if(null == $cond or !isset($this->conditions[$cond]) or true == $this->conditions[$cond]) {
                        $options = Array('timeout' => $timeout);
                        if($name == 'Dispatcher Flush') {
                            $options['headers'] = Array("CQ-Action" => "Flush", "CQ-Handle" => "content", "CQ-Path" => "content", "Content-Length" => "0", "Content-Type" => "application/octet-stream");
                        }
                        $httpRes = $this->webBrowser->Process($test, 'auto', $options);
                        if(!$httpRes['success']) {
                            if('4503' == $uf) {
                                $this->conditions['4503_port_responds'] = false;
                            }
                            $result = $httpRes['error'];
                            if(preg_match("/^Unable to/", $result)) $result = "Unavailable";
                        } else {
                            $result = $httpRes['response']['code'];
                            if('200' == $result) {
                                $to_process = true;
                                if($httpRes['url'] != $test) { // redirects
                                    if(preg_match("/404/", $httpRes['url'])) {
                                        $result = '404 [200]';
                                        $to_process = false;
                                    }
                                }
                                if($to_process) {
                                    if('' != $negative_on_match) {
                                        if(preg_match("/$negative_on_match/", $httpRes['body'])) {
                                            $to_process = false;
                                            $result = '200 [Neg]';
                                        }
                                    } else
                                    if('' != $positive_on_match) {
                                        if(preg_match("/$positive_on_match/", $httpRes['body'])) {
                                            $result = '200 [Pos]';
                                            if($stop_on_positive) $stop_scan = true;
                                            $vulnerable = 'yes';
                                            $to_process = false;
                                        }
                                    }
                                }
                                if($to_process) {
                                    if($is_data_scraped) {
                                        $response_type = $row2['response_type'];
                                        $is_page_valid = true;
                                        $xml = false;
                                        if('JSON' == $response_type) {
                                            if(null == json_decode($httpRes['body'])) $is_page_valid = false;
                                        } else
                                        if('XML' == $response_type) {
                                            if(false == ($xml = simplexml_load_string($httpRes['body'])) and
                                               false == strpos($httpRes['body'], "version=") and
                                               false == strpos($httpRes['body'], "jcr:createdBy") and
                                               false == strpos($httpRes['body'], "jcr:lastModifiedBy"))
                                            {
                                                $is_page_valid = false;
                                            }
                                        }
                                        if($is_page_valid) {
                                            $stmt4 = DbHandler::select(Array('table' => 'scraped_data', 'columns' => Array('data_scraped'), 'where' => Array('module_id' => $module_id)));
                                            while(null != ($row4 = DbHandler::getRow($stmt4))) {
                                                $data_scraped = $row4['data_scraped'];
                                                if('CQ Version' == $data_scraped) {
                                                    $lines = explode("\n", $httpRes['body']);
                                                    if(preg_match('/version="[0-9\.]+"/', $lines[0])) {
                                                        $version = preg_replace('/^.*version="([0-9\.]+)".*$/', '${1}', $lines[0]);
                                                        $result = "200 [ CQ v$version ]";
                                                        $vulnerable = 'yes';
                                                        if(!in_array($version, $scraped_data)) {
                                                            $scraped_data[] = $version;
                                                        }
                                                        if($stop_on_positive) $stop_scan = true;
                                                        break;
                                                    } else {
                                                        $result = "200";
                                                    }
                                                }
                                                else if('User IDs' == $data_scraped) {
                                                    if(preg_match("/\.json/", $test)) {
                                                        $body_parts = explode("\"jcr:createdBy\":\"", $httpRes['body']);
                                                        $out = Array();
                                                        for($i = 0; $i < sizeof($body_parts); ++$i) {
                                                            $res = preg_replace('/^([^"]+).*$/', '${1}', $body_parts[$i]);
                                                            if('{' != $res and !in_array($res, $out))
                                                                $out[] = $res;
                                                        }
                                                        $body_parts = explode("\"jcr:lastModifiedBy\":\"", $httpRes['body']);
                                                        for($i = 0; $i < sizeof($body_parts); ++$i) {
                                                            $res = preg_replace('/^([^"]+).*$/', '${1}', $body_parts[$i]);
                                                            if('{' != $res and !in_array($res, $out))
                                                                $out[] = $res;
                                                        }
                                                        if(sizeof($out) > 0) {
                                                            $result = "200 [ Authors " . join(', ', $out) . " ]";
                                                            $vulnerable = 'yes';
                                                            for($k = 0; $k < sizeof($out); ++$k) {
                                                                if(!in_array($out[$k], $scraped_data)) {
                                                                    $scraped_data[] = $out[$k];
                                                                }
                                                            }
                                                            if($stop_on_positive) $stop_scan = true;
                                                            break;
                                                        } else {
                                                            $result = '200';
                                                        }
                                                    } else if(preg_match("/\.xml/", $test)) {
                                                        $out = Array();
                                                        if(false != $xml) {
                                                            $res = $xml->xpath('//@jcr:createdBy');
                                                            while(list(, $node) = each($res)) {
                                                                foreach($node as $k => $v) {
                                                                    $v = (string)$v;
                                                                    if(!in_array($v, $out)) {
                                                                        $out[] = $v;
                                                                    }
                                                                }
                                                            }
                                                            $res = $xml->xpath('//@jcr:lastModifiedBy');
                                                            while(list(, $node) = each($res)) {
                                                                foreach($node as $k => $v) {
                                                                    $v = (string)$v;
                                                                    if(!in_array($v, $out)) {
                                                                        $out[] = $v;
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            $find_strs = Array('jcr:createdBy', 'jcr:lastModifiedBy');
                                                            for($k = 0; $k < sizeof($find_strs); ++$k) {
                                                                $find = $find_strs[$k];
                                                                $pos = 0;
                                                                $start = strpos($httpRes['body'], $find, $pos);
                                                                while(false != $start) {
                                                                    $quote1 = strpos($httpRes['body'], '"', $start);
                                                                    if(false != $quote1) {
                                                                        ++$quote1;
                                                                        $quote2 = strpos($httpRes['body'], '"', $quote1);
                                                                        if(false != $quote2) {
                                                                            $length = $quote2 - $quote1;
                                                                            $val = substr($httpRes['body'], $quote1, $length);
                                                                            if(!in_array($val, $out)) $out[] = $val;
                                                                            $pos = $quote2 + 1;
                                                                            $start = strpos($httpRes['body'], $find, $pos);
                                                                        } else {
                                                                            break;
                                                                        }
                                                                    } else {
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if(sizeof($out) > 0) {
                                                            $result = "200 [ Authors " . join(', ', $out) . " ]";
                                                            $vulnerable = 'yes';
                                                            for($k = 0; $k < sizeof($out); ++$k) {
                                                                if(!in_array($out[$k], $scraped_data)) {
                                                                    $scraped_data[] = $out[$k];
                                                                }
                                                            }
                                                            if($stop_on_positive) $stop_scan = true;
                                                            break;
                                                        } else {
                                                            $result = '200';
                                                        }
                                                    } else {
                                                        $vulnerable = 'yes';
                                                        if($stop_on_positive) $stop_scan = true;
                                                        $result = '200';
                                                    }
                                                }
                                                else if('Name' == $data_scraped) {
                                                    $this->dom->load($httpRes['body']);
                                                    $results = $this->dom->find('div.license-info div.license-body div.row div.label');
                                                    if(null != $results) {
                                                        $result = '200 [ ';
                                                        $count = 0;
                                                        foreach($results as $row) {
                                                            if(0 == $count or 3 == $count) {
                                                                $v = $row->next_sibling();
                                                                $result .= (string)$v->innertext;
                                                                if(!in_array($v->innertext, $scraped_data)) {
                                                                    $scraped_data[] = $v->innertext;
                                                                }
                                                                if(0 == $count) $result .= ' -- ';
                                                            }
                                                            ++$count;
                                                        }
                                                        $result .= ' ]';
                                                        $vulnerable = 'yes';
                                                        if($stop_on_positive) $stop_scan = true;
                                                        break;
                                                    } else {
                                                        $result = "200";
                                                    }
                                                }
                                                else {
                                                    $result = "200";
                                                }
                                            }
                                        } else {
                                            $result = "200";
                                        }
                                    } else {
                                        if(!preg_match("/QUICKSTART_HOMEPAGE/", $httpRes['body'])) {
                                            $vulnerable = 'yes';
                                            $result = '200';
                                            if($stop_on_positive) $stop_scan = true;
                                        } else {
                                            $result = '200 [ JS redirect ]';
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $str = <<<EOH
        <tr><td>$test</td><td>$result</td><td>$now</td><td class="$vulnerable">$vulnerable</td></tr>
EOH;
                    $this->html .= $str;
                    if(null == $this->job_id) {
                        echo $str;
                        @ob_flush();
                        flush();
                    }
                    if('yes' == $vulnerable) $is_vulnerable = true;
                }

                if($stop_scan) break;
            }

            if($stop_scan) break;
        }

        $str = <<<EOH
      </table>
    </div>
  </div>
EOH;
        $this->html .= $str;
        if(null == $this->job_id) {
            echo $str;
        }

        if($is_vulnerable and null != $this->job_id) {
            DbHandler::insert(Array('table' => 'scan_vulnerabilities', 'columns' => Array('job_id' => $this->job_id, 'module_id' => $module_id)));
            if($is_data_scraped and sizeof($scraped_data) > 0 and preg_match('/{CONTENT}/', $summary)) {
                $content = join(", ", $scraped_data);
                $summary = preg_replace('/{CONTENT}/', $content, $summary);
            }
            $str = <<<EOH
  <div class="container centered-content row scan-results">
    <div>
      <div class="test-header"><span class="module-name">$name</span><span>&nbsp;&nbsp;|&nbsp;&nbsp;</span><span class="module-type">$type</span></div>
      <div style="text-align:left;">$summary</div>
    </div>
  </div>
EOH;
            $this->summary_html .= $str;
        }
    }
}

?>
