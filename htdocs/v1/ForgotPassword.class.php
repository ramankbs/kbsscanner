<?php

class ForgotPassword extends PagesBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if(!isset($this->object)) {
            $this->handleForm();
        } else {
            /*if(!isset($_SERVER['HTTP_REFERER']) or null == ($r = $_SERVER['HTTP_REFERER']) or '' == $r) {
                header("Location: /");
            }
            else {*/
                if('success' == $this->object) {
                    $this->handleAck();
                } else
                if('changed' == $this->object) {
                    $this->handleChanged();
                } else
                if('verified' == $this->object) {
                    $this->handleVerified();
                } else {
                    $this->handleError();
                }
            /*}*/
        }
        $this->_endHandle();
    }

    private function handleForm() {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->page()
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function handleAck() {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->ack()
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function handleChanged() {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->changed()
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function handleVerified() {
        $email = $this->subject;
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->resetForm($email)
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function handleError() {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->error()
            . $this->page()
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function ack() {
        $str = <<<EOH
   <div class="notice info">Please check your email for instructions on resetting your password.</div>
EOH;
        return($str);
    }

    private function changed() {
        $str = <<<EOH
   <div class="notice info">Your password was successfully reset</div>
EOH;
        return($str);
    }

    private function resetForm($email) {
        $str = <<<EOH
  <div class="container body-margin-top centered-content">
    <div class="form-container centered-content forgot-form">
      <div class="log-form">
        <div class="form-row">
          <h4>Choose a new password</h4>
        </div>
        <form action="/v1/users/forgot/" method="POST" name="forgot-password" onsubmit="return validateForgotPasswordForm();">
        <div class="form-row centered-content">
          <table align="center">
            <tr>
              <td style="color:#777;">Email</td>
              <td><input type="text" name="email" inittext="Your email" value="$email"/></td>
            </tr>
            <tr>
              <td style="color:#777;">New Password</td>
              <td><input type="password" name="password"/></td>
            </tr>
            <tr>
              <td style="color:#777;">Confirm</td>
              <td><input type="password" name="confirm"/></td>
            </tr>
          </table>
        </div>
        <div class="form-row">
          <div style="margin-bottom:10px;"><input class="type-1" type="submit" value="Submit"/></div>
        </div>
        </form>
      </div>
    </div>
  </div>
EOH;
        return($str);
    }

    private function error() {
        $errCode = $this->object;
        $errArg = $this->subject;
        ResponseHandler::$OUTPUT = 'html';
        $message = ResponseHandler::errorResponse($errCode, 200, false, $errArg);
        $str = <<<EOH
   <div class="notice error">$message</div>
EOH;
        return($str);
    }

    private function page() {
        $str = <<<EOH
  <div class="container body-margin-top centered-content">
    <div class="form-container centered-content forgot-form">
      <div class="log-form">
        <div class="form-row">
          <h4>Enter your email for receiving instructions to reset your password</h4>
        </div>
        <form action="/v1/users/forgot/" method="POST" name="forgot-password" onsubmit="return validateForgotPasswordForm();">
        <div class="form-row centered-content">
          <table align="center">
            <tr>
              <td style="color:#777;">Email</td>
              <td><input type="text" name="email" inittext="Your email"/></td>
            </tr>
          </table>
        </div>
        <div class="form-row">
          <div style="margin-bottom:10px;"><input class="type-1" type="submit" value="Submit"/></div>
        </div>
        </form>
      </div>
    </div>
  </div>
EOH;
        return($str);
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET' );
}

?>
