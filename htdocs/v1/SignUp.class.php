<?php

class SignUp extends PagesBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if(!isset($this->object)) {
            $this->handleForm();
        } else {
            /*if(!isset($_SERVER['HTTP_REFERER']) or null == ($r = $_SERVER['HTTP_REFERER']) or '' == $r) {
                header("Location: /");
            }
            else {*/
                if('success' == $this->object) {
                    $this->handleAck();
                } else
                if('verified' == $this->object) {
                    $this->handleVerified();
                } else {
                    $this->handleError();
                }
            /*}*/
        }
        $this->_endHandle();
    }

    private function handleForm() {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->page()
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function handleAck() {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->ack()
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function handleVerified() {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->verified()
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function handleError() {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->error()
            . $this->page()
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function ack() {
        $str = <<<EOH
   <div class="notice info">Your sign-up is pending verification. Please check your email for further instructions.</div>
EOH;
        return($str);
    }

    private function verified() {
        $str = <<<EOH
   <div class="notice info">Your account has been successfully verified. Please log in with your email and password.</div>
EOH;
        return($str);
    }

    private function error() {
        $errCode = $this->object;
        $errArg = $this->subject;
        ResponseHandler::$OUTPUT = 'html';
        $message = ResponseHandler::errorResponse($errCode, 200, false, $errArg);
        $str = <<<EOH
   <div class="notice error">$message</div>
EOH;
        return($str);
    }

    private function page() {
        //$captcha = Captcha::inline();
        $str = <<<EOH
  <div class="container body-margin-top centered-content">
    <div class="form-container centered-content signup-form">
      <div class="log-form">
        <div class="form-row">
          <h4>Sign-up for a Security Scanner account</h4>
        </div>
        <div class="form-row centered-content">
          <div>Already have an account with us? <a href="/sign-in" style="display:inline-block;">Sign-in</a></div>
        </div>
        <form action="/v1/users/signup/" method="POST" name="users-signup" onsubmit="return validateSignupForm();">
        <div class="form-row centered-content">
          <table align="center">
            <tr>
              <td style="color:#777;">First Name<em>*</em></td>
              <td><input type="text" name="first-name" inittext="First name"/></td>
            </tr>
            <tr>
              <td style="color:#777;">Last Name<em>*</em></td>
              <td><input type="text" name="last-name" inittext="Last name"/></td>
            </tr>
            <tr>
              <td style="color:#777;">Email<em>*</em></td>
              <td><input type="text" name="email" inittext="Your email"/></td>
            </tr>
            <tr>
              <td style="color:#777;">Password<em>*</em></td>
              <td><input type="password" name="password"/></td>
            </tr>
            <tr>
              <td style="color:#777;">Confirm<em>*</em></td>
              <td><input type="password" name="confirm"/></td>
            </tr>
            <tr>
              <td style="color:#777;">Company<em>*</em></td>
              <td><input type="text" name="company" inittext="Your company"/></td>
            </tr>
            <tr>
              <td style="color:#777;">Position<em>*</em></td>
              <td><input type="text" name="position" inittext="Your position in your company"/></td>
            </tr>
            <tr>
              <td style="color:#777;">Phone Number</td>
              <td><input type="text" name="phonenumber" inittext="Phone Number"/></td>
            </tr> 
            <tr>
              <td style="color:#777;">Address</td>
              <td><input type="text" name="address" inittext="Address"/></td>
            </tr>  
	</table>
<div class="g-recaptcha" data-sitekey="6LdLJQ4TAAAAAPRnK2h09FyixAaslaEF-18SOwf8"></div>
        </div>

        <div class="form-row">
          <div><label class="checkbox"><input type="checkbox" name="terms" value="yes"/>I have read and agree to the <a href="/terms-of-service" style="display:inline-block;" target="_blank">Terms of Use</a></label></div>
        </div>
        <div class="form-row" id="user-signup-button">
          <div style="margin-bottom:10px;"><input class="type-1" type="submit" value="Sign Up"/></div>
        </div>
        </form>
      </div>
    </div>
  </div>
EOH;
        return($str);
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET' );
}

?>

