<?php

class Scan extends PagesBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if(!isset($this->object) or null == $this->object or '' == $this->object)
            $this->handleForm();
        else
            $this->handleError();
        $this->_endHandle();
    }

    private function handleForm() {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->page()
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function handleError() {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->error()
            . $this->page()
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function error() {
        $errCode = $this->object;
        $errArg = $this->subject;
        ResponseHandler::$OUTPUT = 'html';
        $message = ResponseHandler::errorResponse($errCode, 200, false, $errArg);
        $str = <<<EOH
   <div class="notice error">$message</div>
EOH;
        return($str);
    }

    private function page() {
        if(null == CookieManager::get(AppGlobals::$USER_COOKIE_NAME)) $show_terms = true;
        else $show_terms = false;
        //$captcha = Captcha::inline();
        $user_remote_address = $_SERVER['REMOTE_ADDR'];
        $str = <<<EOH
  <div class="container body-margin-top centered-content">
    <div class="form-container centered-content">
      <div class="log-form">
        <div class="form-row">
          <h4>Please enter a URL to scan</h4>
        </div>
        <div class="form-row">
          <p style="font-size:14px;color:#aaa;text-align:left">You'll be redirected to a page where you can view status and results of the scan. Please note that you'll be able to view complete details of scans of the domains you successfully verify.</p>
        </div>
        <div class="form-row"></div>
        <form action="/v1/scans/create/" method="POST" name="scans-create" onsubmit="return validateScanForm();">
        <div class="form-row centered-content">
          <input type="text" name="url" class="url" inittext="URL to scan"/>
          <p style="margin-bottom:0;font-size:12px;color:#777;">Please enter a URL including http or https e.g. http://www.xyz.com/ or https://www.xyz.com/a/b/c.html</p>
        </div>
        <div class="form-row"></div>
        <div class="form-row centered-content">
          <div class="g-recaptcha" data-sitekey="6LdLJQ4TAAAAAPRnK2h09FyixAaslaEF-18SOwf8"></div>
        </div>
EOH;
        if(true == $show_terms) {
            $str .= <<<EOH
        <div class="form-row">
          <div style="color:#aaa;"><label class="checkbox"><input type="checkbox" name="terms" value="yes"/>I have read and agree to the <a href="/terms-of-service" style="display:inline-block;" target="_blank">Terms of Use</a></label></div>
        </div>
EOH;
        }

        $str .= <<<EOH
        <div class="form-row">
          <div style="margin-bottom:10px;">
            <input type="hidden" name="user_ip_address" value="$user_remote_address">
            <input class="type-1" type="submit" value="Submit"/>
          </div>
        </div>
        <div class="form-row"></div>
        </form>
      </div>
    </div>
  </div>
EOH;
        return($str);
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET' );
}

?>
