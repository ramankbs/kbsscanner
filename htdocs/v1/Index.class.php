<?php

class Index extends PagesBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        $this->handleGet();
        $this->_endHandle();
    }

    private function handleGet() {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->home()
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function home() {
        $str = <<<EOH
  <div class="content body-margin-top home-page">
    <div>
      <div class="col">
        <h3>How secure is your website?</h3>
        <p>You may be inadvertently exposing sensitive data such as:</p>
        <ul><li>User Names</li><li>Custom Code</li><li>Secure Content</li><li>CQ Packages</li><li>Able to query your content repository</li></ul>
        <p>Additionally, hackers may be able to bypass your cache and overload your AEM/CQ5 publish servers.</p>
      </div>
      <div class="col">
        <h3>Scan your website!</h3>
        <p>We've designed a web-based website security scanner specifically for sites running on AEM/CQ5. The scan was designed to avoid putting much load on your website and will be the equivalent of 4 to 6 concurrent users hitting your AEM/CQ5 publish servers. When the scan finishes you'll have a report of the types of vulnerabilities on your website and how they could be exploited by hackers.</p>
        <p>It's everyone's responsibility to keep their organization's website safe.</p>
      </div>
      <div style="clear:both;"></div>
      <div class="home-page-buttons centered-content">
        <div><input type=button class="type-2" value="Run a Scan  &#12297;" onclick="window.location='/scan';"/></div>
        <!--div><input type=button class="type-2" value="More Details  &#12297;" onclick="window.location='/scanner-details';"/></div-->
      </div>
    </div>
  </div>
  <div class="body-margin-bottom"></div>
EOH;
        return($str);
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET' );
}

?>
