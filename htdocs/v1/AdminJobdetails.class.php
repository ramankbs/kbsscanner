<?php



class AdminJobdetails extends AdminBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if(!isset($this->subject) or sizeof($this->subject) == 0) {
            $this->printError();
        } else {
            $this->printJob();
        }
        $this->_endHandle();
    }

    private function printError() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            . AdminPanelHelper::navBar(Array('selected' => 'none'))
            . $this->error()
            . AdminPanelHelper::htmlEnd());
    }

    private function error() {
        $str = <<<EOH
   <div class="container centered-content row">
     <h2 class="error">Please specify a job handle to get details of</h2>
   </div>
EOH;
        return $str;
    }

    private function printJob() {
        $job_id = $this->subject;
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            . AdminPanelHelper::navBar(Array('selected' => 'none')));
        $this->job($job_id);
        echo AdminPanelHelper::htmlEnd();
    }

    private function job($job_id) {
        $stmt = DbHandler::select(Array('table' => 'jobs', 'columns' => Array('url', 'completed_on', 'is_completed'), 'where' => Array('job_id' => $job_id)));
        if(null == ($row = DbHandler::getRow($stmt))) {
            $this->error("No job# $job_id not found");
            return;
        }
        if(false == $row['is_completed']) {
            $this->error("The job# $job_id is not complete yet");
            return;
        }
        $url = $row['url'];
        $completed_on = $row['completed_on'];
        $str = <<<EOH
   <div class="container centered-content">
     <h3>Results of scraping $url, completed on $completed_on</h3>
   </div>
    <div class="export-exceldata">
        <input onclick="location.href = '/v1/admin/scanjobdetailsreportxls/$job_id';" type="button" name="exportdata" value="Generate Report as XLS" class="small green">
    </div>
EOH;
        echo $str;

        $sr = new ScanReader($job_id);
        $sr->printResults(true);
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET' );
}

?>
