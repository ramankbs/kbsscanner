<?php

class ScannerDetails extends PagesBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        $this->handleGet();
        $this->_endHandle();
    }

    private function handleGet() {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->details()
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function details() {
        $str = <<<EOH
  <div class="content body-margin-top">
    <div>
       <h3 style="text-transform:uppercase;">Zap Technology Solutions Security Shield for Adobe Experience Manager and CQ5</h3>
    </div>
    <div>
       <p>A highly configurable security software designed to protect your assets and limit your exposure to security vulnerabilities that are abundant in a default installation of Adobe Experience Manager (AEM) and CQ5. This software plugs into AEM/CQ5 using CRX/Apache Felix OSGi. The Zap Technology Solutions Security Shield will help prevent access to your sensitive information which lives on your AEM/CQ5 publish servers such as:</p>
       <ul>
         <li>User ID’s</li>
         <li>Passwords</li>
         <li>Code customizations</li>
         <li>Website file trees</li>
         <li>Server names</li>
         <li>Administration applications</li>
       </ul>
       <p>Even if you’ve taken steps to protect your website from Internet incursions, there is still a high likelihood that internal/corporate users could have access to all of the above information. Our content management and security experts can install and configure the Security Shield in about a week, in most web environments. Register to scan your site and receive a detailed report. We are also available to schedule more in-depth evaluations of your AEM/CQ5 instances and provide solutions to maximize your website and content management security.</p>
    </div>
  </div>
  <div class="body-margin-bottom"></div>
EOH;
        return($str);
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET' );
}

?>
