<?php

class DomainVerify extends PagesBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        $this->handleGet();
        $this->_endHandle();
    }

    private function handleGet() {
        
        $headerFuncName = $this->headerFunctionName();
        $user_id = $this->object;
        $domain = $this->subject;
         //ResponseHandler::response("<html><head><title>Verifier - scan.zapts.com</title></head><body><div class=\"user\" verifier=\"scan.zapts.com\">$user_id</div><div class=\"domain\" verifier=\"scan.zapts.com\">$domain</div></body></html>");
        ResponseHandler::response("<html><head><title>scan-zapts-com-verifier.html</title></head><body><div class=\"user\" verifier=\"scan.zapts.com\">$user_id</div><div class=\"domain\" verifier=\"scan.zapts.com\">$domain</div></body></html>");
        
            //// there is contention over if this mime-type is right, but just use it for now
            //header('Content-type: text/javascript');
            //header('Content-Disposition: attachment; filename="scan-zapts-com-verifier.html"');
            //readfile('scan-zapts-com-verifier.html'); // echo the file
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET' );
}

?>
