<?php

class UsersScans extends SessionPagesBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if(false == ($user_id = $this->_checkSession())) {
            return;
        }
        else if(!isset($this->subject) or null == $this->subject or '' == $this->subject) {
            $this->printError("Please specify a user id to view scans");
        }
        else if(':' . $user_id != $this->subject) {
            $this->printError("You are not authorized to view scans of user" . $this->subject);
        }
        else {
            $this->printJobs($user_id);
        }
        $this->_endHandle();
    }

    private function printError($message) {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->error($message)
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function error($message) {
        $str = <<<EOH
   <div class="notice error">$message</div>
EOH;
        return $str;
    }

    private function printJobs($user_id) {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->jobs($user_id)
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function jobs($user_id) {
        $stmt = DbHandler::select(Array('table' => 'allowed_domains', 'columns' => Array('domain'), 'where' => Array('user_id' => $user_id, 'is_verified' => true)));
        $domains = Array();
        while(null != ($row = DbHandler::getRow($stmt))) {
            $domains[] = $row['domain'];
        }
        $stmt =  DbHandler::select(Array('table' => 'jobs', 'columns' => Array('job_id', 'url', 'domain', 'created_on', 'completed_on', 'is_completed'), 'where' => Array('user_id' => $user_id), 'order' => Array('created_on DESC', 'completed_on DESC')));
        $str = <<<EOH
   <div class="container centered-content scan-results body-margin-top">
     <div>
       <h3>Your Scans</h3>
       <table class="results users-scans">
         <tr><th>Job#</th><th>Base Url</th><th>Started On</th><th>Completed On</th><th>Vulnerabilties</th><th>View</th></tr>
EOH;
        while($row = DbHandler::getRow($stmt)) {
            $job_id = $row['job_id'];
            $url = $row['url'];
            $domain = $row['domain'];
            $created_on = $row['created_on'];
            $completed_on = $row['completed_on'];
            $is_completed = $row['is_completed'];
            $view_str = '';
            $vulnerabilities = '';
	    if($is_completed) {
                $stmt2 = DbHandler::selectUsingQuery("SELECT modules.name AS name FROM modules INNER JOIN scan_vulnerabilities ON modules.id = scan_vulnerabilities.module_id WHERE scan_vulnerabilities.job_id = '$job_id';");
                while(null != ($row2 = DbHandler::getRow($stmt2))) {
                    $vulnerabilities .= $row2['name'] . ", ";
                }
                $vulnerabilities = preg_replace("/, $/", "", $vulnerabilities);
                $is_allowed = false;
                for($k = 0; $k < sizeof($domains); ++$k) {
                    $allowed = $domains[$k];
                    if(preg_match("/$allowed/", $domain) or preg_match("/$domain/", $allowed)) {
                        $is_allowed = true;
                        break;
                    }
                }
                if($is_allowed) {
                    $view_str = '<input type=button class="type-1" value=View onclick="window.location=\'/v1/scans/details/:' . $job_id . '\';"/>';
                } else {
                    $view_str = 'Domain not verified';
                }
            }
            $str .= <<<EOH
         <tr>
           <td>$job_id</td>
           <td>$url</td>
           <td>$created_on</td>
           <td>$completed_on</td>
           <td class="vulnerabilities">$vulnerabilities</td>
           <td>$view_str</td>
         </tr>
EOH;
        }

        $str .= <<<EOH
      </table>
    </div>
  </div>
  <div class="body-margin-bottom"></div>
EOH;
        return($str);
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET' );
}

?>
