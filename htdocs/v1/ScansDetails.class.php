<?php

class ScansDetails extends SessionPagesBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if(null != CookieManager::get(AppGlobals::$USER_COOKIE_NAME)) {
            if(false == ($user_id = $this->_checkSession())) return;
        } else
        if(null != ($anon = CookieManager::get(AppGlobals::$ANON_COOKIE_NAME))) {
            if(false == ($user_id = $this->_getAnonSession())) {
                header('Location: /');
                $this->_endHandle();
                return;
            }
        } else {
            header('Location: /');
            $this->_endHandle();
            return;
        }
        if(!isset($this->subject) or sizeof($this->subject) == 0) {
            $this->printError("No scan# specified");
        } else {
            $job_id = preg_replace("/^:/", "", $this->subject);
            $stmt = DbHandler::select(Array('table' => 'jobs', 'columns' => Array('user_id', 'url', 'domain', 'created_on', 'completed_on', 'is_completed'), 'where' => Array('job_id' => $job_id)));
            if(null == ($row = DbHandler::getRow($stmt))) {
                $this->printError("The scan# $job_id does not exist");
            } else if($user_id != $row['user_id']) {
                $this->printError("You are not authorized to view scan# $job_id. You may need to login into your account first.");
            } else {
                // check if the user is authorized to view this domain's scan
                $domain = $row['domain'];
                $stmt2 = DbHandler::select(Array('table' => 'users', 'columns' => Array('login_id'), 'where' => Array('id' => $user_id)));
                $row2 = DbHandler::getRow($stmt2);
                if('anon' != $row2['login_id'] or false == $row['is_completed']) {
                    if('anon' != $row2['login_id']) {
                        $stmt3 = DbHandler::select(Array('table' => 'allowed_domains', 'columns' => Array('domain'), 'where' => Array('user_id' => $user_id, 'is_verified' => true)));
                        $is_allowed = false;
                        while(null != ($row3 = DbHandler::getRow($stmt3))) {
                            $allowed = $row3['domain'];
                            if(preg_match("/$allowed/", $domain) or preg_match("/$domain/", $allowed)) {
                                $is_allowed = true;
                                break;
                            }
                        }
                        if(false == $is_allowed) {
                            $this->printError("The scan# $job_id is on a domain which you are not authorized to view/run scans for");
                        } else {
                            $this->printJob($job_id, $row['url'], $row['is_completed'], $row['completed_on']);
                        }
                    } else {
                        $this->printJob($job_id, $row['url'], $row['is_completed'], $row['completed_on']);
                    }
                } else {
                    $this->printAnonJobSummary($job_id, $row['url'], $row['created_on'], $row['completed_on']);
                }
            }
        }
        $this->_endHandle();
    }

    private function printError($message) {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->error($message)
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function error($message) {
        $str = <<<EOH
   <div class="notice error">$message</div>
EOH;
        return $str;
    }

    private function printJob($job_id, $url, $is_completed, $completed_on) {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName());

        $this->job($job_id, $url, $is_completed, $completed_on);

        echo PagesHelper::footer()
            . PagesHelper::htmlEnd();
    }

    private function job($job_id, $url, $is_completed, $completed_on) {
        if(false == $is_completed) {
            $str = <<<EOH
  <div class="container centered-content scan-results body-margin-top">
    <div>
      <h4>Scan# $job_id is in progress. Please refresh the page in few seconds.</h4>
	  <span>The Security Scan could take more than 30 minutes to complete.  If you would like to be<br>notified by email when your scan is complete, please <a href="/sign-up" style="display:inline-block;">Register</a>.</span><br><br>
      <input type=button class="type-2" value="Refresh  &#12297;" onclick="window.location.reload(1);"/>
    </div>
  </div>
EOH;
            echo $str;
            return;
        }
        $str = <<<EOH
   <div class="container centered-content scan-results body-margin-top">
    <div>
      <h5 style="color:#bbb;margin: 0 0 10px 0;">Results of scanning $url</h5>
      <span style="color:#777;">Completed on $completed_on</span>
      <p style="font-size:12px; margin:10px 0px 0px; color:#777;">* Please note that if your web server is misconfigured to respond with an HTTP 200 response for error pages, it could cause the scanner to return false positives.</p>
    </div>
   </div>
EOH;
        echo $str;

        $sr = new ScanReader($job_id);
        $sr->printResults();

        $str = <<<EOH
   <div class="body-margin-bottom"></div>
EOH;
        echo $str;
    }

    private function printAnonJobSummary($job_id, $url, $created_on, $completed_on) {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName());

        $vulnerabilities = '';
        $signup_str = '<input type=button class="type-1" value="Register" onclick="window.location=\'/sign-up\';"/>';
        $stmt = DbHandler::selectUsingQuery("SELECT modules.name AS name FROM modules INNER JOIN scan_vulnerabilities ON modules.id = scan_vulnerabilities.module_id WHERE scan_vulnerabilities.job_id = '$job_id';");
        while(null != ($row = DbHandler::getRow($stmt))) {
            $vulnerabilities .= $row['name'] . ", ";
        }
        $vulnerabilities = preg_replace("/, $/", "", $vulnerabilities);
        $str = <<<EOH
   <div class="container centered-content scan-results body-margin-top">
     <div>
       <h3>Here is a summary of your scan. Anonymous scans and results are abbreviated. Please <a href="/sign-up" style="display:inline-block;">Register</a> to start a full scan and view more scan details.</h3>
       <table class="results">
         <tr><th>Job#</th><th>Base Url</th><th>Started On</th><th>Completed On</th><th>Vulnerabilties</th><th>Register</th></tr>
         <tr>
           <td>$job_id</td>
           <td>$url</td>
           <td>$created_on</td>
           <td>$completed_on</td>
           <td>$vulnerabilities</td>
           <td>$signup_str</td>
         </tr>
       </table>
     </div>
   </div>
EOH;
        echo $str;

        echo PagesHelper::footer()
            . PagesHelper::htmlEnd();
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET' );
}

?>
