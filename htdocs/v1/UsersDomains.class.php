<?php

require_once "uws/http.php";
require_once "uws/web_browser.php";

class UsersDomains extends SessionPagesBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if(false == ($user_id = $this->_checkSession())) {
            return;
        }
        else if(!isset($this->subject) or null == $this->subject or '' == $this->subject) {
            $this->printError("Please specify a user id to view and add domains");
        }
        else if(':' . $user_id != $this->subject) {
            $this->printError("You are not authorized to view domains of user" . $this->subject);
        }
        else {
            if('GET' == $this->method) {
                if(null != $this->args and sizeof($this->args)) {
                    if(preg_match("/^E/", $this->args[0])) {
                        $this->printSignedError($user_id);
                    } else {
                        $type = array_shift($this->args);
                        if('ack' == $type) {
                            $domain = array_shift($this->args);
                            $page = "/domain-verify/$user_id/$domain/scan-zapts-com-verifier.html";
                            $this->printSignedAck($user_id, "Please download verifier for $domain from the table below, upload to specified directory, and click verify.");
                        } else
                        if('success' == $type) {
                            $domain = array_shift($this->args);
                            $this->printSignedAck($user_id, "Your ownership of $domain is verified");
                        }
                    }
                } else {
                    $this->printDomains($user_id);
                }
            } else {
                $action = array_shift($this->args);
                if('add' == $action) {
                    $this->handleAdd($user_id);
                }
                else if('verify' == $action) {
                    $this->handleVerify($user_id, $this->args[0]);
                }
            }
        }
        $this->_endHandle();
    }

    private function handleVerify($user_id, $domain) {
        $stmt = DbHandler::select(Array('table' => 'allowed_domains', 'columns' => Array('verify_root'), 'where' => Array('domain' => $domain, 'user_id' => $user_id)));
        $row = DbHandler::getRow($stmt);
		
        $uniqueFname = $_POST['uni_filename'];
		

		if(!empty($_POST['domain_directory_check'])){
			
				 $verify_root = $row['verify_root'].$_POST['domain_directory_check'].$uniqueFname;
		}else{		
				$verify_root = $row['verify_root'].$uniqueFname;
			
		}
		
       
        //$w = new WebBrowser();
        //$r = $w->Process("http://$verify_root");

		
			$ch = curl_init($verify_root);

			curl_setopt($ch, CURLOPT_NOBODY, true);
			curl_exec($ch);
			$retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			// $retcode >= 400 -> not found, $retcode = 200, found.
			//curl_close($ch);

			if($retcode == '200'){
				$file = "http://$verify_root";
				$aa = basename($file);
				
					if (strpos($aa,'.html') !== false) {
						
					DbHandler::update(Array('table' => 'allowed_domains', 'update' => Array('is_verified' => true), 'where' => Array('user_id' => $user_id, 'domain' => $domain)));
					header("Location: /v1/users/domains/:$user_id/success/$domain");
					
						}else{
							
							header("Location: /v1/users/domains/:$user_id/EDOMVERIFY/$domain");
							return;
						
						}
			}else {
				header("Location: /v1/users/domains/:$user_id/EDOMVERIFY/$domain");
			}
			
		
		
		
        //if($r['success'] and '200' == $r['response']['code']) {
        //
        //
        //
        //    $got_user = null;
        //    if(preg_match('/<div class="user" verifier="scan.zapts.com">/', $r['body'])) {
        //        $got_user = preg_replace('/^.*<div class="user" verifier="scan.zapts.com">([^<]+).*$/', '${1}', $r['body']);
        //        $got_user = trim(rtrim($got_user));
        //    }
        //    $got_domain = null;
        //    if(preg_match('/<div class="domain" verifier="scan.zapts.com">/', $r['body'])) {
        //        $got_domain = preg_replace('/^.*<div class="domain" verifier="scan.zapts.com">([^<]+).*$/', '${1}', $r['body']);
        //        $got_domain = trim(rtrim($got_domain));
        //    }
        //
        //    if(null != $got_user and null != $got_domain) {
        //        if($user_id != $got_user) {
        //            header("Location: /v1/users/domains/:$user_id/EDOMVERIFY/$domain");
        //            return;
        //        }
        //        if($domain != $got_domain) {
        //            header("Location: /v1/users/domains/:$user_id/EDOMVERIFY/$domain");
        //            return;
        //        }
        //        DbHandler::update(Array('table' => 'allowed_domains', 'update' => Array('is_verified' => true), 'where' => Array('user_id' => $user_id, 'domain' => $domain)));
        //        header("Location: /v1/users/domains/:$user_id/success/$domain");
        //    } else {
        //        if(null == $got_user) {
        //            header("Location: /v1/users/domains/:$user_id/EDOMVERIFY/$domain");
        //            return;
        //        }
        //        if(null == $got_domain) {
        //            header("Location: /v1/users/domains/:$user_id/EDOMVERIFY/$domain");
        //            return;
        //        }
        //    }
        //} else {
        //    header("Location: /v1/users/domains/:$user_id/EDOMVERIFY/$domain");
        //}
    }

    private function handleAdd($user_id) {
        $domain = $_POST['domain'];
		$uniquefilename = $_POST['uni_filename'];
        $stmt = DbHandler::select(Array('table' => 'allowed_domains', 'columns' => Array('domain'), 'where' => Array('domain' => $domain, 'user_id' => $user_id)));
        if(null != ($row = DbHandler::getRow($stmt))) {
            header("Location: /v1/users/domains/:$user_id/EDOMEXIST/$domain");
            return;
        }
        // find the root
        $w = new WebBrowser();
        $r = $w->Process("http://$domain/");
        if(isset($r['url'])) {
            $path = $r['url'];
            $path = preg_replace('/^.*:\/\//', "", $path);
            $pos = strrpos($path, "/");
            $path = substr($path, 0, $pos + 1);
            DbHandler::insert(Array('table' => 'allowed_domains', 'columns' => Array('domain' => $domain, 'verify_root' => $path, 'user_id' => $user_id, 'is_verified' => false, 'uniquefilename' => $uniquefilename)));
            header("Location: /v1/users/domains/:$user_id/ack/$domain/");
        } else {
            header("Location: /v1/users/domains/:$user_id/EERROR/Failed to access the domain, please try again/");
        }
    }

    private function printError($message) {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->error($message)
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function error($message) {
        $str = <<<EOH
   <div class="notice error">$message</div>
EOH;
        return $str;
    }

    private function printSignedError($user_id) {
        $errCode = array_shift($this->args);
        $errArg = array_shift($this->args);
        ResponseHandler::$OUTPUT = 'html';
        $message = ResponseHandler::errorResponse($errCode, 200, false, $errArg);
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->error($message)
            . $this->domains($user_id)
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function printSignedAck($user_id, $message) {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->ack($message)
            . $this->domains($user_id)
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

    private function ack($message) {
        $str = <<<EOH
   <div class="notice info">$message</div>
EOH;
        return $str;
    }

    private function printDomains($user_id) {
        $headerFuncName = $this->headerFunctionName();
        ResponseHandler::response(
            PagesHelper::htmlBegin()
            . PagesHelper::$headerFuncName()
            . $this->domains($user_id)
            . PagesHelper::footer()
            . PagesHelper::htmlEnd());
    }

	private function random_gen($length)
{
  $str= "";
  srand((double)microtime()*1000000);
  //$char_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  $char_list .= "abcdefghijklmnopqrstuvwxyz";
  $char_list .= "1234567890-";
  // Add the special characters to $char_list if needed

  for($i = 0; $i < $length; $i++)
  {
    $str .= substr($char_list,(rand()%(strlen($char_list))), 1);
  }
  return $str;
}

    private function domains($user_id) {
		$scanfile = $this->random_gen(8).".html";
        $stmt =  DbHandler::select(Array('table' => 'allowed_domains', 'columns' => Array('domain', 'verify_root', 'is_verified', 'uniquefilename'), 'where' => Array('user_id' => $user_id), 'order' => Array('is_verified ASC')));
        $str = <<<EOH
		
   <div class="container centered-content scan-results body-margin-top">
     <div>
       <h3>Add Domain</h3>
       <form action="/v1/users/domains/:$user_id/add/" method="POST">
         <div class="form-row user-domain-list">
           <i>http://</i><span><input type="text" class="url user-domain-textfield" inittext="Your domain to add" name="domain"/></span>
           <input type='hidden' name='uni_filename' value=$scanfile>
		   <span><input type="submit" name="submit" value="Add Domain"></span>
         </div>
       </form>
     </div>
   </div>
   <div class="container centered-content scan-results">
     <div>
       <h3>Your Domains</h3>
       <table class="results users-domains">
         <tr><th>Domain</th><th>Verified?</th><th>Verify Path</th><th>Action</th></tr>
		 
EOH;
        while($row = DbHandler::getRow($stmt)) {
            $domain = $row['domain'];
            $is_verified = $row['is_verified'];
            $action_str = '';
            $html_str = '';
            $path_str = '';
			$uniquefilename = "/" . $row['uniquefilename'];
			

			$str .= <<<EOH
			<form method="POST" action="/v1/users/domains/:$user_id/verify/$domain">
EOH;
			
            if(!$is_verified) {
                $action_str = '<input type=submit class="type-1" value="Verify"></form>';
                //$html_str = "<a target='_blank' href=\"/domain-verify/$user_id/$domain/scan-zapts-com-verifier.html\">Verifier</a>";
                $path_str = $row['verify_root'];
            }
            $is_verified = ($is_verified ? 'Yes' : 'No');
			
			if($is_verified == "No"){
				$asdf = $path_str."<input type='text' placeholder='Path to html file' name='domain_directory_check'><input type='hidden' name='uni_filename' value=$uniquefilename>  $uniquefilename";
				
			}else{
				
				$asdf = "";
				
			}
			
            $str .= <<<EOH
         <tr>
			<td>$domain</td>
			<td>$is_verified</td>
			<td>$asdf</td>
			<td>$action_str</td>
           <!--<td>$path_str</td>-->
         </tr>
EOH;
        }

        $str .= <<<EOH
      </table>
    </div>
  </div>
  <div class="body-margin-bottom"></div>
EOH;
        return($str);
    }
	

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET', 'POST' );
}

?>
