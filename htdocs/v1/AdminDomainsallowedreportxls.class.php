<?php
header("Content-Disposition: attachment; filename=\"report.xls\"");
header("Content-Type: application/vnd.ms-excel;");
header("Pragma: no-cache");
header("Expires: 0");
class AdminDomainsallowedreportxls extends AdminBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if('GET' == $this->method) {
            if(!isset($this->subject) or null == $this->subject or '' == $this->subject) {
                $this->handleUsersForm();
            } else {
                $this->handleDomainsForm();
            }
        } else if('POST' == $this->method) {
            $this->handleAddDomain();
        }
        $this->_endHandle();
    }

    private function handleUsersForm() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
           // . AdminPanelHelper::navBar(Array('selected' => 'domains'))
            . $this->usersForm()
            . AdminPanelHelper::htmlEnd());
    }

    private function handleDomainsForm() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            //. AdminPanelHelper::navBar(Array('selected' => 'domains'))
            . $this->domainsForm()
            . AdminPanelHelper::htmlEnd());
    }


    private function domainsForm() {
        $user_id = $this->subject;
        $stmt = DbHandler::select(Array('table' => 'allowed_domains', 'columns' => Array('domain', 'is_verified'), 'where' => Array('user_id' => $user_id), 'order' => Array('is_verified ASC')));
        $str = <<<EOH
       <h3>Domains allowed for user# $user_id</h3>
   <div class="container centered-content row">
     <table class="results small">
       <tr><th>Domains Added/Allowed Already</th></tr>
EOH;

        while(null != ($row = DbHandler::getRow($stmt))) {
            $domain = $row['domain'];
            $is_verified = $row['is_verified'];
            $str .= <<<EOH
       <tr><td>$domain</td></tr>
EOH;
        }

        $str .= <<<EOH
     </table>
   </div>
EOH;
$file="report.xls";
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=$file");
echo $str;
    }


    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET', 'POST' );
}

?>
