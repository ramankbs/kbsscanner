<?php

class AdminVerify extends AdminBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if('GET' == $this->method) {
            if(!isset($this->subject) or null == $this->subject or '' == $this->subject) {
                $this->handleUsersForm();
            } else {
                $this->handleAck();
            }
        } else if('POST' == $this->method) {
            $this->handleVerify();
        }
        $this->_endHandle();
    }

    private function handleUsersForm() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            . AdminPanelHelper::navBar(Array('selected' => 'verify'))
            . $this->usersForm()
            . AdminPanelHelper::htmlEnd());
    }

    private function handleAck() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            . AdminPanelHelper::navBar(Array('selected' => 'verify'))
            . $this->ack()
            . $this->usersForm()
            . AdminPanelHelper::htmlEnd());
    }

    private function ack() {
        $user_id = array_shift($this->args);
        $str = <<<EOH
  <div class="container centered-content row">
     <h3>User with id $user_id verified successfully</h3>
  </div>
EOH;
        return($str);
    }

    private function usersForm() {
        $stmt = DbHandler::selectUsingQuery("SELECT id, first_name, last_name, login_id, company, position FROM users WHERE login_id != 'anon' AND is_active = false;");
        $str = <<<EOH
   <form action="/v1/admin/verify" method="POST" name=verify>
      <input type=hidden name=user />
   </form>
   <div class="container centered-content row">
     <table class="results">
       <tr><th>Email</th><th>Name</th><th>Company</th><th>Position</th><th>Verify</th></tr>
EOH;

        while(null != ($row = DbHandler::getRow($stmt))) {
            $user_id = $row['id'];
            $email = $row['login_id'];
            $name = $row['first_name'] . " " . $row['last_name'];
            $company = $row['company'];
            $position = $row['position'];
            $str .= <<<EOH
       <tr><td>$email</td><td>$name</td><td>$company</td><td>$position</td><td><input type=button class="small green" value=Verify onclick="$('form[name=verify] input[name=user]:first').val('$user_id');$('form[name=verify]:first').submit();"/></td></tr>
EOH;
        }

        $str .= <<<EOH
     </table>
   </div>
EOH;
        return($str);
    }

    private function handleVerify() {
        $user_id = $_POST['user'];
        DbHandler::update(Array('table' => 'users', 'update' => Array('is_active' => true), 'where' => Array('id' => $user_id)));
        header("Location: /v1/admin/verify/success/$user_id");
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET', 'POST' );
}

?>
