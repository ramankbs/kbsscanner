<?php

class AdminModules extends AdminBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if('GET' == $this->method) {
            $this->printModules();
        } else {
            $this->changeModule();
        }
        $this->_endHandle();
    }

    private function changeModule() {
        $where['id'] = $_POST['module'];
        $update['timeout'] = $_POST['timeout'];
        $update['stop_on_positive'] = false;
        if(isset($_POST['stop_on_positive'])) $update['stop_on_positive'] = true;
        $update['follow_redirects'] = false;
        if(isset($_POST['follow_redirects'])) $update['follow_redirects'] = true;
        $update['positive_on_match'] = $_POST['positive_on_match'];
        $update['negative_on_match'] = $_POST['negative_on_match'];
        DbHandler::update(Array('table' => 'modules', 'update' => $update, 'where' => $where));
        header("Location: /v1/admin/modules");
    }

    private function printModules() {
        ResponseHandler::response(
            AdminPanelHelper::htmlBegin()
            . AdminPanelHelper::headerLoggedIn()
            . AdminPanelHelper::navBar(Array('selected' => 'modules'))
            . $this->modules()
            . AdminPanelHelper::htmlEnd());
    }

    private function modules() {
        $stmt = DbHandler::selectUsingQuery("SELECT id, name, timeout, stop_on_positive, follow_redirects, positive_on_match, negative_on_match FROM modules;");
        $str = <<<EOH
   <div class="container centered-content row">
     <table class="results">
       <tr><th>Module</th><th>Timeout</th><th>Stop on 1st Positive?</th><th>Follow redirects?</th><th>Positive on Match</th><th>Negative On Match</th><th>Change</th></tr>
EOH;
        while($row = DbHandler::getRow($stmt)) {
            $module_id = $row['id'];
            $name = $row['name'];
            $timeout = $row['timeout'];
            $stop_on_positive = ($row['stop_on_positive'] ? ' checked=checked' : '');
            $follow_redirects = ($row['follow_redirects'] ? ' checked=checked' : '');
            $positive_on_match = $row['positive_on_match'];
            $negative_on_match = $row['negative_on_match'];
            $str .= <<<EOH
       <form action="/v1/admin/modules/" method="POST">
         <input type=hidden name=module value="$module_id"/>
       <tr>
         <td>$name</td>
         <td><input type=text class=small name=timeout value="$timeout"/></td>
         <td><input type=checkbox name=stop_on_positive value=1 $stop_on_positive/></td>
         <td><input type=checkbox name=follow_redirects value=1 $follow_redirects/></td>
         <td><input type=text name=positive_on_match value="$positive_on_match"></td>
         <td><input type=text name=negative_on_match value="$negative_on_match"></td>
         <td><input type=submit class="small green" value="change"/></td>
       </tr>
       </form>
EOH;
        }

        $str .= <<<EOH
     </table>
  </div>
EOH;
        return($str);
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET', 'POST' );
}

?>
