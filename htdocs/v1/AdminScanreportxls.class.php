<?php
header("Content-Disposition: attachment; filename=\"report.xls\"");
header("Content-Type: application/vnd.ms-excel;");
header("Pragma: no-cache");
header("Expires: 0");

class AdminScanreportxls extends AdminBase
{
    /*
     * PRIVATE METHODS
     */

    protected function _handle() {
        ResponseHandler::$OUTPUT = 'html';
        $this->_beginHandle();
        if('GET' == $this->method) {
            if(!isset($this->subject) or null == $this->subject or '' == $this->subject) {
                $this->handleScanreportxls();
            } else {
                $this->handleAck();
            }
        } else if('POST' == $this->method) {
            $this->handleScanreport();
        }
        $this->_endHandle();
    }

    private function handleScanreportxls() {

        $stmt = null;
        if(null != $jobs) {
            $jobs_str = join(', ', $jobs);
            $stmt = DbHandler::selectUsingQuery("SELECT user_id, job_id, url, created_on, completed_on, is_completed FROM jobs WHERE job_id IN ( $jobs_str );");
        } else {
            $stmt =  DbHandler::select(Array('table' => 'jobs', 'columns' => Array('user_id', 'job_id', 'url', 'created_on', 'completed_on', 'is_completed', 'ip_address'), 'order' => Array('created_on DESC', 'completed_on DESC')));//, 'limit' => Array('offset' => 0, 'number' => 10)));
        }
        $str = <<<EOH
     <table border="1">
       <tr><th>Job#</th><th>Base Url</th><th>Requester</th><th>Started On</th><th>Completed On</th><th>IP Address</th><th>Vulnerabilties</th></tr>
EOH;
        while($row = DbHandler::getRow($stmt)) {
            $user_id = $row['user_id'];
            $job_id = $row['job_id'];
            $url = $row['url'];
            $created_on = $row['created_on'];
            $completed_on = $row['completed_on'];
            $is_completed = $row['is_completed'];
            $ip_address = $row['ip_address'];
            if(0 != $user_id) {
                $stmt2 = DbHandler::select(Array('table' => 'users', 'columns' => Array('login_id', 'first_name', 'last_name', 'company', 'position', 'login_id'), 'where' => Array('id' => $user_id)));
                $row2 = DbHandler::getRow($stmt2);
                if('anon' != $row2['login_id']) {
                    $name = $row2['first_name'] . " " . $row2['last_name'];
                    $email = $row2['login_id'];
                    $work = $row2['position'] . ", " . $row2['company'];
                    $contact_str = "<p style='margin:0;'>$name</p><p style='margin:0;'>$email</p><p style='margin:0;'>$work</p>";
					$contact_str = $contact_str;
			    } else {
                    $contact_str = 'Anonymous User';
                }
            } else {
                $contact_str = 'Admin';
            }
            $view_str = '';
            $vulnerabilities = '';
	    if($is_completed) {
                $view_str = '<input type=button class="small green" value=View onclick="window.location=\'/v1/admin/jobdetails/' . $job_id . '\';"/>';
                $stmt2 = DbHandler::selectUsingQuery("SELECT modules.name AS name FROM modules INNER JOIN scan_vulnerabilities ON modules.id = scan_vulnerabilities.module_id WHERE scan_vulnerabilities.job_id = '$job_id';");
                while(null != ($row2 = DbHandler::getRow($stmt2))) {
                    $vulnerabilities .= $row2['name'] . ", ";
                }
                $vulnerabilities = preg_replace("/, $/", "", $vulnerabilities);
            }
            $str .= <<<EOH
       <tr>
         <td>$job_id</td>
         <td>$url</td>
         <td>$contact_str</td>
         <td>$created_on</td>
         <td>$completed_on</td>
         <td>$ip_address</td>
         <td>$vulnerabilities</td>
       </tr>
EOH;
        }

        $str .= <<<EOH
     </table>
EOH;


$file="report.xls";
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=$file");
echo $str;
    }

    /*
     * ATTRIBUTES
     */

    /* parameters and their format expected in input data per method */
    protected $POST_DataSpec = null;
    protected $GET_DataSpec = null;
    protected $PUT_DataSpec = null;
    protected $DELETE_DataSpec = null;

    /* methods expected to be handled */
    protected $methodsExpected = Array( 'GET', 'POST' );
}

?>